
#define _CRT_SECURE_NO_DEPRECATE // 1
#define _CRT_NONSTDC_NO_DEPRECATE // 1

//Eugen Mircea Anitas "Small-angle scattering (neutrons, X-rays, Light) from complex systems. Fractal and multifractal models for interpretation of experimental data".
//Springer briefs in physics, 2018
#include "Multifractal_SelectedFeas_function.cpp"

int main()
{
	int doMultifractalSpectrumOf_ColorImage(

		const Image& image_in,        

		//const PARAMETERS_REMOVAL_OF_LABELS *sPARAMETERS_REMOVAL_OF_LABELSf,
		COLOR_IMAGE *sColor_Image,

		float fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[]);

	int doOneFea_OfMultifractal(
		const int nNumOfOneFeaf,

		//const Image& image_in,

		const COLOR_IMAGE *sColor_Image,

		//	float fOneDim_Multifractal_Arrf[]) //[nNumOfMultifractalFeasFor_OneDimTot]
		float &fOneFeaf);

	int
		nRes;

	float
		fOneSelectedFeaf,
		fOneDim_Multifractal_Arr[nNumOfMultifractalFeasFor_OneDimTot]; //[nNumOfMultifractalFeasFor_OneDimTot]

		////////////////////////////////////////////////////////////////////////////////////////////////
/*
	fout_lr = fopen("wMain_RemovingLabels.txt", "w");

	if (fout_lr == NULL)
	{
		printf("\n\n fout_lr == NULL");
		getchar(); exit(1);
	} //if (fout_lr == NULL)
*/
	Image image_in;

	image_in.read("output_ForMultifractal.png");
	//image_in.read("Row A Day 1.png");
	//image_in.read("Row A Day 3.png");

	//image_in.read("Row B Day 1.png");
	//image_in.read("Row B Day 3.png");

	//image_in.read("Row C Day 1.png");
	//image_in.read("Row C Day 3.png");

	//Image image_out;
	COLOR_IMAGE sColor_Image; //

	nRes = doMultifractalSpectrumOf_ColorImage(
				image_in, // Image& image_in,

		&sColor_Image, //COLOR_IMAGE *sColor_Image,
		fOneDim_Multifractal_Arr); // 

	if (nRes == UNSUCCESSFUL_RETURN) // -1
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n The image has not been processed properly.");

		fprintf(fout_lr,"\n\n The image has not been processed properly.");

		fflush(fout_lr); //debugging;
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n Press any key to exit");	getchar(); 
		return UNSUCCESSFUL_RETURN; //
	} // if (nRes == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
	
	fflush(fout_lr); //debugging;
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

//////////////////////////////////////////////
	if (nNumOfSelectedFea < 0 || nNumOfSelectedFea >= nNumOfMultifractalFeasFor_OneDimTot)
	{
		printf("\n\n Please specify the feature number correctly: nNumOfSelectedFea = %d, nNumOfMultifractalFeasFor_OneDimTot = %d", 
			nNumOfSelectedFea, nNumOfMultifractalFeasFor_OneDimTot);

		printf("\n\n Press any key to exit");	getchar();
		return UNSUCCESSFUL_RETURN; //
	} //if (nNumOfSelectedFea < 0 || nNumOfSelectedFea >= nNumOfMultifractalFeasFor_OneDimTot)

	nNumOfSpectrumFea_Glob = -1;
	printf("\n\n Before 'doOneFea_OfMultifractal': please press any key"); getchar();

	nRes = doOneFea_OfMultifractal(
		nNumOfSelectedFea, //const int nNumOfOneFeaf,

		//image_in, //const Image& image_in,

		&sColor_Image, //const COLOR_IMAGE *sColor_Image,

		fOneSelectedFeaf); // float &fOneFeaf);

	if (nNumOfSelectedFea > 0 && nNumOfSelectedFea < nNumOfMultifractalFeasFor_OneDimTot - 1)
	{
		printf("\n\n nNumOfSelectedFea = %d, fOneSelectedFeaf = %E, fOneDim_Multifractal_Arr[%d] = %E, fOneDim_Multifractal_Arr[%d] = %E, fOneDim_Multifractal_Arr[%d] = %E", 
			nNumOfSelectedFea, fOneSelectedFeaf, nNumOfSelectedFea - 1, fOneDim_Multifractal_Arr[nNumOfSelectedFea - 1], 
			nNumOfSelectedFea, fOneDim_Multifractal_Arr[nNumOfSelectedFea],
			nNumOfSelectedFea + 1, fOneDim_Multifractal_Arr[nNumOfSelectedFea + 1]);

#ifndef COMMENT_OUT_ALL_PRINTS

		fprintf(fout_lr, "\n\n nNumOfSelectedFea = %d, fOneSelectedFeaf = %E, fOneDim_Multifractal_Arr[%d] = %E, fOneDim_Multifractal_Arr[%d] = %E, fOneDim_Multifractal_Arr[%d] = %E",
			nNumOfSelectedFea, fOneSelectedFeaf, nNumOfSelectedFea - 1, fOneDim_Multifractal_Arr[nNumOfSelectedFea - 1],
			nNumOfSelectedFea, fOneDim_Multifractal_Arr[nNumOfSelectedFea],
			nNumOfSelectedFea + 1, fOneDim_Multifractal_Arr[nNumOfSelectedFea + 1]);

		//fflush(fout_lr); //debugging;
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	} //if (nNumOfSelectedFea > 0 && nNumOfSelectedFea < nNumOfMultifractalFeasFor_OneDimTot - 1)
////////////////////////
	//image_out.write("01-144_LCC_mask_NoLabels.png");

//#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n The endof 'doMultifractalSpectrumOf_ColorImage': please press any key to exit"); fflush(fout_lr); getchar(); //exit(1);
//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

#ifndef COMMENT_OUT_ALL_PRINTS
	fclose(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	return SUCCESSFUL_RETURN;
} //int main()

//printf("\n\n Please press any key:"); getchar();