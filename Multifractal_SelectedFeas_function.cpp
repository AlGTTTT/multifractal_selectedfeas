
//Eugen Mircea Anitas "Small-angle scattering (neutrons, X-rays, Light) from complex systems. Fractal and multifractal models for interpretation of experimental data".
//Springer briefs in physics, 2018
//#include "Multifractal_SelectedFeas_2.h"
#include "Multifractal_And_Lacunarity_2.h"

using namespace imago;

FILE *fout_lr;

int
	nNumOfOneFea_Spectrum_AdjustedGlob = -1,
		nNumOfSpectrumFea_Glob = -1,
	iIntensity_Interval_1_Glob,
	iIntensity_Interval_2_Glob;

int doMultifractalSpectrumOf_ColorImage(
	const Image& image_in,

	//const PARAMETERS_REMOVAL_OF_LABELS *sParameters_Removal_Of_Labelsf,
		COLOR_IMAGE *sColor_Image,

	float fOneDim_Multifractal_Arrf[]) //[nNumOfMultifractalFeasFor_OneDimTot]
{

	int Initializing_Color_To_CurSize(
		const int nImageWidthf,
		const int nImageLengthf,

		COLOR_IMAGE *sColor_Imagef);
	   
	int GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges(

		//const int nDim_2pNf,
		const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

		const COLOR_IMAGE *sColor_ImageInitf,

		float fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[], //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]
		float fSpectrumOf_All_Orders_And_All_IntensityRanges_Arrf[], //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]
		float fCrowdingIndex_Of_All_Orders_And_All_IntensityRanges_Arrf[]); //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	int
		nRes,
		i,
		j,

		iFeaf,
		nSizeOfImage, // = nImageWidth*nImageHeight,

		nIndexOfPixelCur,

		iLen,
		iWid,

		nRed,
		nGreen,
		nBlue,

		nIntensity_Read_Test_ImageMax = -nLarge,
		nImageWidth,
		nImageHeight;

	float
		fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[nNumOfFeasForMultifractalTot], //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]
		fSpectrumOf_All_Orders_And_All_IntensityRanges_Arrf[nNumOfFeasForMultifractalTot], //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]
		fCrowdingIndex_Of_All_Orders_And_All_IntensityRanges_Arrf[nNumOfFeasForMultifractalTot]; //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]

	////////////////////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
	fout_lr = fopen("wMain_Multifractal.txt", "w");

	if (fout_lr == NULL)
	{
		printf("\n\n fout_lr == NULL");
		//getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (fout_lr == NULL)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	  // size of image
	nImageWidth = image_in.width();
	nImageHeight = image_in.height();

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n nImageWidth = %d, nImageHeight = %d, nNumOfMultifractalFeasFor_OneDimTot = %d", nImageWidth, nImageHeight, nNumOfMultifractalFeasFor_OneDimTot);
	fprintf(fout_lr, "\n\n nImageWidth = %d, nImageHeight = %d, nNumOfMultifractalFeasFor_OneDimTot = %d", nImageWidth, nImageHeight, nNumOfMultifractalFeasFor_OneDimTot);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (nImageWidth > nLenMax || nImageWidth < nLenMin)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in reading the image width: nImageWidth = %d", nImageWidth);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n An error in reading the image width: nImageWidth = %d", nImageWidth);
		getchar(); exit(1);

//#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} // if (nImageWidth > nLenMax || nImageWidth < nLenMin)

	if (nImageHeight > nWidMax || nImageHeight < nWidMin)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in reading the image height: nImageHeight = %d", nImageHeight);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n An error in reading the image height: nImageHeight = %d", nImageHeight);
		getchar(); exit(1);
		return UNSUCCESSFUL_RETURN;
	} // if (nImageHeight > nWidMax || nImageHeight < nWidMin)

	int bytesOfWidth = image_in.pitchInBytes();

	Image imageToSave(nImageWidth, nImageHeight, bytesOfWidth);

	///////////////////////////////////////////////////////
	//COLOR_IMAGE sColor_Image; //

	nRes = Initializing_Color_To_CurSize(
		nImageHeight, //const int nImageWidthf,
		nImageWidth, //const int nImageLengthf,

		//&sColor_Image); // COLOR_IMAGE *sColor_Imagef);
		sColor_Image); // COLOR_IMAGE *sColor_Imagef);

	if (nRes == UNSUCCESSFUL_RETURN)
	{
		//if (sColor_Imagef->nLenObjectBoundary_Arr == nullptr || sColor_Imagef->nRed_Arr == nullptr || sColor_Imagef->nGreen_Arr == nullptr || sColor_Imagef->nBlue_Arr == nullptr ||
			//sColor_Imagef->nIsAPixelBackground_Arr == nullptr)
/*
		delete[] sColor_Image.nLenObjectBoundary_Arr;
		delete[] sColor_Image.nRed_Arr;
		delete[] sColor_Image.nGreen_Arr;
		delete[] sColor_Image.nBlue_Arr;
		delete[] sColor_Image.nIsAPixelBackground_Arr;
*/
#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n An error in'Initializing_Color_To_CurSize':");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n An error in'Initializing_Color_To_CurSize':");
		getchar(); exit(1);

		delete[] sColor_Image->nLenObjectBoundary_Arr;
		delete[] sColor_Image->nRed_Arr;
		delete[] sColor_Image->nGreen_Arr;
		delete[] sColor_Image->nBlue_Arr;
		delete[] sColor_Image->nIsAPixelBackground_Arr;

		return UNSUCCESSFUL_RETURN;
	} //if (nRes == UNSUCCESSFUL_RETURN)

	nSizeOfImage = nImageWidth * nImageHeight;

	//finding 'nIntensity_Read_Test_ImageMax' to decide if the intensity rescaling to (0,255) is needed
	for (j = 0; j < nImageHeight; j++)
	{
		for (i = 0; i < nImageWidth; i++)
		{
			nIndexOfPixelCur = i + j * nImageWidth;

			nRed = image_in(j, i, R);
			nGreen = image_in(j, i, G);
			nBlue = image_in(j, i, B);

			if (nRed < 0 || nRed > nIntensityStatForReadMax || nGreen < 0 || nGreen > nIntensityStatForReadMax || nBlue < 0 || nBlue > nIntensityStatForReadMax)
			{
#ifndef COMMENT_OUT_ALL_PRINTS

				fprintf(fout_lr, "\n\n An error in reading the image: nRed = %d, nGreen = %d, nBlue = %d, i = %d, j = %d", nRed, nGreen, nBlue, i, j);
				printf("\n\n An error in reading the image: nRed = %d, nGreen = %d, nBlue = %d, i = %d, j = %d", nRed, nGreen, nBlue, i, j);
				//printf("\n\n Please press any key to exit");
				getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

				return UNSUCCESSFUL_RETURN;
			} // if (nRed < 0 || nRed > nIntensityStatForReadMax || nGreen < 0 || nGreen > nIntensityStatForReadMax || nBlue < 0 || nBlue > nIntensityStatForReadMax)

			if (nRed > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nRed;

			if (nGreen > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nGreen;

			if (nBlue > nIntensity_Read_Test_ImageMax)
				nIntensity_Read_Test_ImageMax = nBlue;

		} // for (int i = 0; i < nImageWidth; i++)

	}//for (int j = 0; j < nImageHeight; j++)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);
	fprintf(fout_lr, "\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
	////////////////////////////////////////////////////////////////////////////////////////////

	for (j = 0; j < nImageHeight; j++)
	{
		for (i = 0; i < nImageWidth; i++)
		{
			nIndexOfPixelCur = i + j * nImageWidth;

			nRed = image_in(j, i, R);
			nGreen = image_in(j, i, G);
			nBlue = image_in(j, i, B);

			iLen = i;
			iWid = j;

			if (nIntensity_Read_Test_ImageMax > nIntensityStatMax)
			{
				//rescaling
				sColor_Image->nRed_Arr[nIndexOfPixelCur] = nRed / 256;

				sColor_Image->nGreen_Arr[nIndexOfPixelCur] = nGreen / 256;
				sColor_Image->nBlue_Arr[nIndexOfPixelCur] = nBlue / 256;

				//////////////////////////////////////////////////////////////////////////////////////////////////////
				sColor_Image->nRed_Arr[nIndexOfPixelCur] = nRed;
				sColor_Image->nGreen_Arr[nIndexOfPixelCur] = nGreen;
				sColor_Image->nBlue_Arr[nIndexOfPixelCur] = nBlue;

			} //if (nIntensity_Read_Test_ImageMax > nIntensityStatMax)
			else
			{
				// no rescaling
				sColor_Image->nRed_Arr[nIndexOfPixelCur] = nRed;
				sColor_Image->nGreen_Arr[nIndexOfPixelCur] = nGreen;
				sColor_Image->nBlue_Arr[nIndexOfPixelCur] = nBlue;
			} //else

		} // for (int i = 0; i < nImageWidth; i++)
	}//for (int j = 0; j < nImageHeight; j++)
///////////////////////////////////////////////////////////////////////////
	WEIGHTES_OF_RGB_COLORS sWeightsOfColorsf;

	sWeightsOfColorsf.fWeightOfRed = fWeightOfRed_InStr;
	sWeightsOfColorsf.fWeightOfGreen = fWeightOfGreen_InStr;
	sWeightsOfColorsf.fWeightOfBlue = fWeightOfBlue_InStr;

	//printf("\n 4"); getchar();

	///////////////////////////////////////////////////////////////////////
	
	nRes = GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges(

		&sWeightsOfColorsf, //const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

		//&sColor_Image, //const COLOR_IMAGE *sColor_ImageInitf,
		sColor_Image, //const COLOR_IMAGE *sColor_ImageInitf,

		fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf, // float fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[]) //[nNumOfFeasForMultifractalTot]
		fSpectrumOf_All_Orders_And_All_IntensityRanges_Arrf, //float fSpectrumOf_All_Orders_And_All_IntensityRanges_Arrf[], //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]
		fCrowdingIndex_Of_All_Orders_And_All_IntensityRanges_Arrf); // float fCrowdingIndex_Of_All_Orders_And_All_IntensityRanges_Arrf[]); //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		if (nRes == UNSUCCESSFUL_RETURN)
		{
			delete[] sColor_Image->nRed_Arr;
			delete[] sColor_Image->nGreen_Arr;
			delete[] sColor_Image->nBlue_Arr;

			delete[] sColor_Image->nLenObjectBoundary_Arr;

			delete[] sColor_Image->nIsAPixelBackground_Arr;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n An error in'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges':");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

			printf("\n\n An error in'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges':");
			getchar(); exit(1);

			return UNSUCCESSFUL_RETURN;
		}// if (nRes == UNSUCCESSFUL_RETURN)

		for (iFeaf = 0; iFeaf < nNumOfFeasForMultifractalTot; iFeaf++)
		{
			fOneDim_Multifractal_Arrf[iFeaf] = fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[iFeaf];
		}//for (iFeaf = 0; iFeaf < nNumOfFeasForMultifractalTot; iFeaf++)

		for (iFeaf = nNumOfFeasForMultifractalTot; iFeaf < 2*nNumOfFeasForMultifractalTot; iFeaf++)
		{
			fOneDim_Multifractal_Arrf[iFeaf] = fSpectrumOf_All_Orders_And_All_IntensityRanges_Arrf[iFeaf - nNumOfFeasForMultifractalTot];
		}//for (iFeaf = nNumOfFeasForMultifractalTot; iFeaf < 2*nNumOfFeasForMultifractalTot; iFeaf++)

		for (iFeaf = 2*nNumOfFeasForMultifractalTot; iFeaf < 3 * nNumOfFeasForMultifractalTot; iFeaf++)
		{
			fOneDim_Multifractal_Arrf[iFeaf] = fCrowdingIndex_Of_All_Orders_And_All_IntensityRanges_Arrf[iFeaf - (2 * nNumOfFeasForMultifractalTot)];
		}//for (iFeaf = 2*nNumOfFeasForMultifractalTot; iFeaf < 3*nNumOfFeasForMultifractalTot; iFeaf++)

		//nNumOfMultifractalFeasFor_OneDimTot

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n All OneDim multifractal feas:");
		fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

		for (iFeaf = 0; iFeaf < nNumOfMultifractalFeasFor_OneDimTot; iFeaf++)
		{
			fprintf(fout_lr, "\n fOneDim_Multifractal_Arrf[%d] = %E", iFeaf, fOneDim_Multifractal_Arrf[iFeaf]);
		}//for (iFeaf = 0; iFeaf < nNumOfMultifractalFeasFor_OneDimTot; iFeaf++)

		fprintf(fout_lr, "\n\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

		fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

//////////////////////////////////////////////////////////
/*
	delete[] sColor_Image->nRed_Arr;
	delete[] sColor_Image->nGreen_Arr;
	delete[] sColor_Image->nBlue_Arr;

	delete[] sColor_Image->nLenObjectBoundary_Arr;
	delete[] sColor_Image->nIsAPixelBackground_Arr;

*/
	/////////////////////////////////////////////////
	return SUCCESSFUL_RETURN;
} //int doMultifractalSpectrumOf_ColorImage(...
////////////////////////////////////////////////////////////////////

int doOneFea_OfMultifractal(
	const int nNumOfOneFeaf,

	//const Image& image_in,
	const COLOR_IMAGE *sColor_Image,

//	float fOneDim_Multifractal_Arrf[]) //[nNumOfMultifractalFeasFor_OneDimTot]
	float &fOneFeaf)
{

	int OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges(
		const int nNumOfOneFea_Adjustedf,
		const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

		const COLOR_IMAGE *sColor_ImageInitf,

		float &fOneFea_FrGenerDimf);

	int OneFea_Spectrum_Of_All_Orders_And_All_IntensityRanges(
		const int nNumOfOneFea_Adjustedf,
		const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

		const COLOR_IMAGE *sColor_ImageInitf,

		float &fOneFea_FrGenerDimf);

	int OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges(
		const int nNumOfOneFea_Adjustedf,
		const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

		const COLOR_IMAGE *sColor_ImageInitf,

		float &fOneFea_FrCrowdingIndexf);

	int
		nNumOfOneFea_Adjustedf,
		nRes;


	float
		fOneFea_FrGenerDimf,
		fOneFea_FrSpectrumf,
		fOneFea_FrCrowdingIndexf;

////////////////////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS
/*
	fout_lr = fopen("wMain_OneFea_OfMultifractal.txt", "w");

	if (fout_lr == NULL)
	{
		printf("\n\n fout_lr == NULL");
		//getchar(); exit(1);

		return UNSUCCESSFUL_RETURN;
	} //if (fout_lr == NULL)
*/
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	printf("\n\n 'doOneFea_OfMultifractal'");
	if (nNumOfOneFeaf < 0 || nNumOfOneFeaf >= 3 * nNumOfFeasForMultifractalTot) // == nNumOfMultifractalFeasFor_OneDimTot
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n The feature number %d is out of the range of 0 and %d", nNumOfOneFeaf, 3 * nNumOfFeasForMultifractalTot);
		fprintf(fout_lr,"\n\n The feature number %d is out of the range of 0 and %d", nNumOfOneFeaf, 3 * nNumOfFeasForMultifractalTot);

		fflush(fout_lr);  getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	}//if (nNumOfOneFeaf < 0 || nNumOfOneFeaf >= 3 * nNumOfFeasForMultifractalTot)

///////////////////////////////////////////////////////////////////////////
	
WEIGHTES_OF_RGB_COLORS sWeightsOfColorsf;

sWeightsOfColorsf.fWeightOfRed = fWeightOfRed_InStr;
sWeightsOfColorsf.fWeightOfGreen = fWeightOfGreen_InStr;
sWeightsOfColorsf.fWeightOfBlue = fWeightOfBlue_InStr;

	///////////////////////////////////////////////////////////////////////
	if (nNumOfOneFeaf < nNumOfFeasForMultifractalTot)
	{
		nNumOfOneFea_Adjustedf = nNumOfOneFeaf;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'doOneFea_OfMultifractal' (generalized dim): nNumOfOneFeaf = %d, nNumOfOneFea_Adjustedf = %d, nNumOfFeasForMultifractalTot = %d",
			nNumOfOneFeaf, nNumOfOneFea_Adjustedf, nNumOfFeasForMultifractalTot);
		fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n 'doOneFea_OfMultifractal' (generalized dim): nNumOfOneFeaf = %d, nNumOfOneFea_Adjustedf = %d, nNumOfFeasForMultifractalTot = %d",
			nNumOfOneFeaf, nNumOfOneFea_Adjustedf, nNumOfFeasForMultifractalTot);

		nRes = OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges(
			nNumOfOneFea_Adjustedf, // const int nNumOfOneFea_Adjustedf,
			&sWeightsOfColorsf, //const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

			//&sColor_Image, //const COLOR_IMAGE *sColor_ImageInitf,
			sColor_Image, //const COLOR_IMAGE *sColor_ImageInitf,

			fOneFea_FrGenerDimf); // float &fOneFea_FrGenerDimf) 

		fOneFeaf = fOneFea_FrGenerDimf;
	}//if (nNumOfOneFeaf < nNumOfFeasForMultifractalTot)
	else if (nNumOfOneFeaf >= nNumOfFeasForMultifractalTot  && nNumOfOneFeaf < 2*nNumOfFeasForMultifractalTot)
	{
		nNumOfOneFea_Adjustedf = nNumOfOneFeaf - nNumOfFeasForMultifractalTot;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'doOneFea_OfMultifractal' (spectrum): nNumOfOneFeaf = %d, nNumOfOneFea_Adjustedf = %d, nNumOfFeasForMultifractalTot = %d",
			nNumOfOneFeaf, nNumOfOneFea_Adjustedf, nNumOfFeasForMultifractalTot);
		fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf( "\n\n 'doOneFea_OfMultifractal' (spectrum): nNumOfOneFeaf = %d, nNumOfOneFea_Adjustedf = %d, nNumOfFeasForMultifractalTot = %d",
			nNumOfOneFeaf, nNumOfOneFea_Adjustedf, nNumOfFeasForMultifractalTot);

		nRes = OneFea_Spectrum_Of_All_Orders_And_All_IntensityRanges(
			nNumOfOneFea_Adjustedf, // const int nNumOfOneFea_Adjustedf,
			&sWeightsOfColorsf, //const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

			//&sColor_Image, //const COLOR_IMAGE *sColor_ImageInitf,
			sColor_Image, //const COLOR_IMAGE *sColor_ImageInitf,

			fOneFea_FrSpectrumf); // float &fOneFea_FrSpectrumf) 

		fOneFeaf = fOneFea_FrSpectrumf;

	}//else if (nNumOfOneFeaf >= nNumOfFeasForMultifractalTot  && nNumOfOneFeaf < 2*nNumOfFeasForMultifractalTot)
	else if (nNumOfOneFeaf >= 2 * nNumOfFeasForMultifractalTot && nNumOfOneFeaf < 3 * nNumOfFeasForMultifractalTot)
	{
		nNumOfOneFea_Adjustedf = nNumOfOneFeaf - (2 * nNumOfFeasForMultifractalTot);

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'doOneFea_OfMultifractal' (crowding index): nNumOfOneFeaf = %d, nNumOfOneFea_Adjustedf = %d, nNumOfFeasForMultifractalTot = %d",
			nNumOfOneFeaf, nNumOfOneFea_Adjustedf, nNumOfFeasForMultifractalTot);
		fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		printf("\n\n 'doOneFea_OfMultifractal' (crowding index): nNumOfOneFeaf = %d, nNumOfOneFea_Adjustedf = %d, nNumOfFeasForMultifractalTot = %d",
			nNumOfOneFeaf, nNumOfOneFea_Adjustedf, nNumOfFeasForMultifractalTot);

		nRes = OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges(
			nNumOfOneFea_Adjustedf, // const int nNumOfOneFea_Adjustedf,
			&sWeightsOfColorsf, //const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

			//&sColor_Image, //const COLOR_IMAGE *sColor_ImageInitf,
			sColor_Image, //const COLOR_IMAGE *sColor_ImageInitf,

			fOneFea_FrCrowdingIndexf); // float &fOneFea_FrCrowdingIndexf);

		fOneFeaf = fOneFea_FrCrowdingIndexf;
	}//else if (nNumOfOneFeaf >= 2*nNumOfFeasForMultifractalTot && nNumOfOneFeaf < 3 * nNumOfFeasForMultifractalTot)


#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n The end of 'doOneFea_OfMultifractal': nNumOfOneFeaf = %d, fOneFeaf = %E", nNumOfOneFeaf, fOneFeaf);
	fprintf(fout_lr, "\n nNumOfOneFea_Adjustedf = %d, nNumOfFeasForMultifractalTot = %d", nNumOfOneFea_Adjustedf, nNumOfFeasForMultifractalTot);

	fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	//////////////////////////////////////////////////////////
	delete[] sColor_Image->nRed_Arr;
	delete[] sColor_Image->nGreen_Arr;
	delete[] sColor_Image->nBlue_Arr;

	delete[] sColor_Image->nLenObjectBoundary_Arr;
	delete[] sColor_Image->nIsAPixelBackground_Arr;

	/////////////////////////////////////////////////
	return SUCCESSFUL_RETURN;
} //int doOneFea_OfMultifractal(...
////////////////////////////////////////////////////////////////////////////////////////////////

int Initializing_Color_To_CurSize(
	const int nImageWidthf,
	const int nImageLengthf,

	COLOR_IMAGE *sColor_Imagef) //[]
{
	int
		nIndexOfPixelCurf,

		nImageSizeCurf = nImageWidthf * nImageLengthf,
		iWidf,
		iLenf;

	sColor_Imagef->nSideOfObjectLocation = 0; // neither left nor right
	sColor_Imagef->nIntensityOfBackground_Red = -1;
	sColor_Imagef->nIntensityOfBackground_Green = -1;
	sColor_Imagef->nIntensityOfBackground_Blue = -1;

	sColor_Imagef->nWidth = nImageWidthf;
	sColor_Imagef->nLength = nImageLengthf;

	//sGrayscale_Imagef->nPixel_ValidOrNotArr = new int[nImageSizeCurf];

	sColor_Imagef->nLenObjectBoundary_Arr = new int[nImageWidthf];

	sColor_Imagef->nRed_Arr = new int[nImageSizeCurf];
	sColor_Imagef->nGreen_Arr = new int[nImageSizeCurf];
	sColor_Imagef->nBlue_Arr = new int[nImageSizeCurf];

	sColor_Imagef->nIsAPixelBackground_Arr = new int[nImageSizeCurf];

	if (sColor_Imagef->nLenObjectBoundary_Arr == nullptr || sColor_Imagef->nRed_Arr == nullptr || sColor_Imagef->nGreen_Arr == nullptr || sColor_Imagef->nBlue_Arr == nullptr ||
		sColor_Imagef->nIsAPixelBackground_Arr == nullptr)
	{
		return UNSUCCESSFUL_RETURN;
	} //if (sColor_Imagef->nLenObjectBoundary_Arr == nullptr || sColor_Imagef->nRed_Arr == nullptr || ...

	for (iWidf = 0; iWidf < nImageWidthf; iWidf++)
	{
		sColor_Imagef->nLenObjectBoundary_Arr[iWidf] = -1;

		for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
		{
			nIndexOfPixelCurf = iLenf + (iWidf*nImageLengthf);

			//sGrayscale_Imagef->nPixel_ValidOrNotArr[nIndexOfPixelCurf] = 1; //valid for no restrictions
			sColor_Imagef->nRed_Arr[nIndexOfPixelCurf] = -1;
			sColor_Imagef->nGreen_Arr[nIndexOfPixelCurf] = -1;
			sColor_Imagef->nBlue_Arr[nIndexOfPixelCurf] = -1;

			sColor_Imagef->nIsAPixelBackground_Arr[nIndexOfPixelCurf] = 0; //all pixels belong to the object
		} //for (iLenf = 0; iLenf < nImageLengthf; iLenf++)
	} // for (iWidf = 0; iWidf < nImageWidthf; iWidf++)

	return SUCCESSFUL_RETURN;
} //int Initializing_Color_To_CurSize(...
/////////////////////////////////////////////////////////////////////////////

int Initializing_Embedded_Image(
	const int nDim_2pNf,

	EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef) // //[nDim_2pNf*nDim_2pNf]
{
	int
		nIndexOfPixelInEmbeddedImagef,
		nImageSizeCurf = nDim_2pNf * nDim_2pNf,

		iWidf,
		iLenf;

	sImageEmbeddedf_BlackWhitef->nWidth = nDim_2pNf;
	sImageEmbeddedf_BlackWhitef->nLength = nDim_2pNf;

	sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr = new int[nImageSizeCurf];

	if (sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr == nullptr)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Initializing_Embedded_Image': sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr == nullptr");
		fprintf(fout_lr, "\n\n An error in 'Initializing_Embedded_Image': sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr == nullptr");
		 getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		return UNSUCCESSFUL_RETURN;
	} //if (sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr == nullptr)

	for (iWidf = 0; iWidf < nDim_2pNf; iWidf++)
	{

		for (iLenf = 0; iLenf < nDim_2pNf; iLenf++)
		{
			nIndexOfPixelInEmbeddedImagef = iLenf + (iWidf*nDim_2pNf);

			sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] = -1; //invalid

		} //for (iLenf = 0; iLenf < nDim_2pNf; iLenf++)
	} // for (iWidf = 0; iWidf < nDim_2pNf; iWidf++)

	return SUCCESSFUL_RETURN;
} //int Initializing_Embedded_Image(...
  ///////////////////////////////////////////////////////////////////////

int Dim_2powerN(
	const int nLengthf,
	const int nWidthf,

	int &nScalef,
	int &nDim_2pNf)
{
	int
		i,
		nTempf,
		nLargerDimInitf;

	if (nLengthf >= nWidthf)
		nLargerDimInitf = nLengthf;
	else
		nLargerDimInitf = nWidthf;

	nTempf = 2;
	for (i = 2; i < nNumOfIters_ForDim_2powerN_Max; i++)
	{
		nTempf = nTempf * 2;
		if (nTempf >= nLargerDimInitf)
		{
			nDim_2pNf = nTempf;
			break;
		}//if (nTempf >= nLargerDimInitf)
	}//for (i = 2; i < nNumOfIters_ForDim_2powerN_Max; i++)
/////////////////////////////////

	if (i == 3 || i >= nNumOfIters_ForDim_2powerN_Max - 1)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf( "\n\n An error in 'Dim_2powerN': i = %d, nNumOfIters_ForDim_2powerN_Max - 1 = %dE", i, nNumOfIters_ForDim_2powerN_Max - 1);
		fprintf(fout_lr, "\n\n An error in 'Dim_2powerN': i = %d, nNumOfIters_ForDim_2powerN_Max - 1 = %dE", i, nNumOfIters_ForDim_2powerN_Max - 1);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		return UNSUCCESSFUL_RETURN;
	}
	else
	{
		nScalef = i;
		return SUCCESSFUL_RETURN;
	} //
}//int Dim_2powerN(...
/////////////////////////////////////////////////////////////////////////////

int Embedding_Image_Into_2powerN_ForHausdorff(
	const int nDim_2pNf,

	const int nThresholdForIntensitiesMinf,
	const int nThresholdForIntensitiesMaxf,

	const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

	const COLOR_IMAGE *sColor_ImageInitf,
	EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef) //[nDim_2pNf*nDim_2pNf]

{
	int
		nIndexOfPixelCurf,
		nIndexOfPixelInEmbeddedImagef,

		iWidf,
		iLenf,

		nNumOfWhitePixelsTotf = 0,
		nNumOfBlackPixelsTotf = 0,

		nRedInitf,
		nGreenInitf,
		nBlueInitf,

		nDivisorf = 3,

		nRedCurf,
		nGreenCurf,
		nBlueCurf,
		nIntensityAverf;

	float
		fRatiof;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'Embedding_Image_Into_2powerN_ForHausdorff'");
	fprintf(fout_lr, "\n nDim_2pNf = %d, nThresholdForIntensitiesMinf = %d, nThresholdForIntensitiesMaxf = %d", nDim_2pNf, nThresholdForIntensitiesMinf, nThresholdForIntensitiesMaxf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	{
		nDivisorf = 3;
	} // if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
	{
		nDivisorf = 2;
	} //else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)

	else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	{
		nDivisorf = 2;
	} //else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	{
		nDivisorf = 2;
	} //else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)

	else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
	{
		nDivisorf = 1;
	} //else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
	else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
	{
		nDivisorf = 1;
	} //else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
	else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	{
		nDivisorf = 1;
	} //else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
	else
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'Embedding_Image_Into_2powerN_ForHausdorff': the color weights are wrong");
		printf( "\n\n sWeightsOfColorsf->fWeightOfRed = %E, sWeightsOfColorsf->fWeightOfGreen = %E, sWeightsOfColorsf->fWeightOfBlue = %E",
			sWeightsOfColorsf->fWeightOfRed, sWeightsOfColorsf->fWeightOfGreen, sWeightsOfColorsf->fWeightOfBlue);

		printf( "\n nDim_2pNf = %d, nThresholdForIntensitiesMinf = %d, nThresholdForIntensitiesMaxf = %d", nDim_2pNf, nThresholdForIntensitiesMinf, nThresholdForIntensitiesMaxf);

		fprintf(fout_lr, "\n\n An error in 'Embedding_Image_Into_2powerN_ForHausdorff': the color weights are wrong");

		fprintf(fout_lr, "\n\n sWeightsOfColorsf->fWeightOfRed = %E, sWeightsOfColorsf->fWeightOfGreen = %E, sWeightsOfColorsf->fWeightOfBlue = %E",
			sWeightsOfColorsf->fWeightOfRed, sWeightsOfColorsf->fWeightOfGreen, sWeightsOfColorsf->fWeightOfBlue);

		fprintf(fout_lr, "\n nDim_2pNf = %d, nThresholdForIntensitiesMinf = %d, nThresholdForIntensitiesMaxf = %d", nDim_2pNf, nThresholdForIntensitiesMinf, nThresholdForIntensitiesMaxf);

		printf("\n\n Please press any key:"); fflush(fout_lr); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		return UNSUCCESSFUL_RETURN;
	}//else

	for (iWidf = 0; iWidf < nDim_2pNf; iWidf++)
	{
		for (iLenf = 0; iLenf < nDim_2pNf; iLenf++)
		{
			nIndexOfPixelInEmbeddedImagef = iLenf + (iWidf*nDim_2pNf);

			if (iWidf < sColor_ImageInitf->nWidth && iLenf < sColor_ImageInitf->nLength)
			{
				nIndexOfPixelCurf = iLenf + iWidf * sColor_ImageInitf->nLength;

				nRedInitf = sColor_ImageInitf->nRed_Arr[nIndexOfPixelCurf];
				nGreenInitf = sColor_ImageInitf->nGreen_Arr[nIndexOfPixelCurf];
				nBlueInitf = sColor_ImageInitf->nBlue_Arr[nIndexOfPixelCurf];

				nRedCurf = (int)((float)(nRedInitf)* sWeightsOfColorsf->fWeightOfRed);
				nBlueCurf = (int)((float)(nGreenInitf)* sWeightsOfColorsf->fWeightOfGreen);
				nGreenCurf = (int)((float)(nBlueInitf)* sWeightsOfColorsf->fWeightOfBlue);

				nIntensityAverf = (nRedCurf + nBlueCurf + nGreenCurf) / nDivisorf;

				if (nIntensityAverf > nIntensityStatMax)
					nIntensityAverf = nIntensityStatMax;

				if (nIntensityAverf >= nThresholdForIntensitiesMinf && nIntensityAverf <= nThresholdForIntensitiesMaxf)
				{
					sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] = 1; //
					nNumOfWhitePixelsTotf += 1;
				} //if (nIntensityAverf >= nThresholdForIntensitiesMinf && nIntensityAverf <= nThresholdForIntensitiesMaxf)
				else
				{
					sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] = 0;
					nNumOfBlackPixelsTotf += 1;
				}//else if (nIntensityAverf <= nThresholdForIntensitiesf)

			} // if (iWidf < sColor_ImageInitf->nWidth && iLenf < sColor_ImageInitf->nLength)
			else
			{
				sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] = 0; // no nonzero pixels
				nNumOfBlackPixelsTotf += 1;
			}//else

		} //for (iLenf = 0; iLenf < nDim_2pNf; iLenf++)
	} // for (iWidf = 0; iWidf < nDim_2pNf; iWidf++)

	fRatiof = (float)(nNumOfWhitePixelsTotf) / (float)(nDim_2pNf*nDim_2pNf);

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n The end of 'Embedding_Image_Into_2powerN_ForHausdorff': nNumOfWhitePixelsTotf = %d, nNumOfBlackPixelsTotf = %d, fRatiof = %E", nNumOfWhitePixelsTotf, nNumOfBlackPixelsTotf, fRatiof);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	return SUCCESSFUL_RETURN;
} //int Embedding_Image_Into_2powerN_ForHausdorff(...

//////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

int NumOfObjectSquaresAndLogPoint_WithResolution(
	const int nDim_2pNf,

	const int nLenOfSquaref,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	int &nNumOfObjectSquaresTotf,
	float &fLogPointf)

{
	int
		nIndexOfPixelInEmbeddedImagef,

		nIndexOfPixelMaxf = nDim_2pNf* nDim_2pNf,

		iWidf,
		iLenf,

		nNumOfSquaresInImageSidef = nDim_2pNf / nLenOfSquaref,

		nNumOfSquaresInImageTotf, // = nNumOfSquaresInImageSidef* nNumOfSquaresInImageSidef,

		nWidOfSquareMinf,
		nWidOfSquareMaxf,

		nLenOfSquareMinf,
		nLenOfSquareMaxf,

		iWidSquaresf,
		iLenSquaresf;

	float
		fRatioOfSquaresf;
	////////////////////////////////////////////////
	nNumOfSquaresInImageTotf = nNumOfSquaresInImageSidef * nNumOfSquaresInImageSidef;

	nNumOfObjectSquaresTotf = 0;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'NumOfObjectSquaresAndLogPoint_WithResolution'\n");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iWidSquaresf = 0; iWidSquaresf < nNumOfSquaresInImageSidef; iWidSquaresf++)
	{
		nWidOfSquareMinf = iWidSquaresf * nLenOfSquaref;

		if (nWidOfSquareMinf > sColor_ImageInitf->nWidth)
		{
			continue;
		}//if (nWidOfSquareMinf > sColor_ImageInitf->nWidth)

		nWidOfSquareMaxf = (iWidSquaresf + 1)* nLenOfSquaref;

		for (iLenSquaresf = 0; iLenSquaresf < nNumOfSquaresInImageSidef; iLenSquaresf++)
		{
			nLenOfSquareMinf = iLenSquaresf * nLenOfSquaref;
			nLenOfSquareMaxf = (iLenSquaresf + 1)* nLenOfSquaref;

			if (nLenOfSquareMinf > sColor_ImageInitf->nLength)
			{
				continue;
			}//if (nLenOfSquareMinf > sColor_ImageInitf->nLength)

			for (iWidf = nWidOfSquareMinf; iWidf < nWidOfSquareMaxf; iWidf++)
			{
				for (iLenf = nWidOfSquareMinf; iLenf < nLenOfSquareMaxf; iLenf++)
				{
					nIndexOfPixelInEmbeddedImagef = iLenf + (iWidf*nDim_2pNf);

					if (nIndexOfPixelInEmbeddedImagef >= nIndexOfPixelMaxf)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						printf("\n\n An error in 'NumOfObjectSquaresAndLogPoint_WithResolution': nIndexOfPixelInEmbeddedImagef = %d >= nIndexOfPixelMaxf = %d", nIndexOfPixelInEmbeddedImagef,nIndexOfPixelMaxf);
						fprintf(fout_lr, "\n\n An error in 'NumOfObjectSquaresAndLogPoint_WithResolution': nIndexOfPixelInEmbeddedImagef = %d >= nIndexOfPixelMaxf = %d", nIndexOfPixelInEmbeddedImagef, nIndexOfPixelMaxf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

						return UNSUCCESSFUL_RETURN;
					}//if (nIndexOfPixelInEmbeddedImagef >= nIndexOfPixelMaxf)

					if (sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] == 1)
					{
						nNumOfObjectSquaresTotf += 1;

						goto MarkContinueForiLenSquares;
					} //if (sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] == 1)

				} //for (iLenf = nWidOfSquareMinf; iLenf < nLenOfSquareMaxf; iLenf++)
			} // for (iWidf = nWidOfSquareMinf; iWidf < nWidOfSquareMaxf; iWidf++)

		MarkContinueForiLenSquares: continue;
		} //for (iLenSquaresf = 0; iLenSquaresf < nNumOfSquaresInImageSidef; iLenSquaresf++)

	} // for (iWidSquaresf = 0; iWidSquaresf < nNumOfSquaresInImageSidef; iWidSquaresf++)

	fRatioOfSquaresf = (float)(nNumOfObjectSquaresTotf) / (float)(nNumOfSquaresInImageTotf);

	if (nNumOfObjectSquaresTotf > 1)
	{
		//fLogPointf = log(nNumOfObjectSquaresTotf) / (-log(nLenOfSquaref));
		fLogPointf = (float)(log(nNumOfObjectSquaresTotf));
	}
	else
		fLogPointf = 0.0;

#ifndef COMMENT_OUT_ALL_PRINTS
	printf( "\n\n The end of 'NumOfObjectSquaresAndLogPoint_WithResolution': nNumOfObjectSquaresTotf = %d, nNumOfSquaresInImageTotf = %d, fLogPointf = %E, fRatioOfSquaresf = %E",
		nNumOfObjectSquaresTotf, nNumOfSquaresInImageTotf, fLogPointf, fRatioOfSquaresf);
	printf("\n nLenOfSquaref = %d, nNumOfSquaresInImageSidef = %d", nLenOfSquaref, nNumOfSquaresInImageSidef);

	fprintf(fout_lr, "\n\n The end of 'NumOfObjectSquaresAndLogPoint_WithResolution': nNumOfObjectSquaresTotf = %d, nNumOfSquaresInImageTotf = %d, fLogPointf = %E, fRatioOfSquaresf = %E",
		nNumOfObjectSquaresTotf, nNumOfSquaresInImageTotf, fLogPointf, fRatioOfSquaresf);
	fprintf(fout_lr, "\n nLenOfSquaref = %d, nNumOfSquaresInImageSidef = %d", nLenOfSquaref, nNumOfSquaresInImageSidef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	return SUCCESSFUL_RETURN;
} //int NumOfObjectSquaresAndLogPoint_WithResolution(...
////////////////////////////////////////////////////////////////////////////////

//Tug-of-war lacunarity�A novel approach for estimating lacunarity --https://aip.scitation.org/doi/full/10.1063/1.4966539
int MassesOfAll_ObjectSquares_AtFixedLenOfSquare(
	const int nDim_2pNf,

	const int nLenOfSquaref,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	 int &nNumOfSquaresInImageTotf,

	int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf
	int &nMassOfImageTotf, //<= 

	int nMassesOfSquaresArrf[]) //[nNumOfSquaresTotf]


{
	int
		nIndexOfSquareCurf,

		nIndexOfPixelInEmbeddedImagef,

		nIndexOfPixelMaxf = nDim_2pNf * nDim_2pNf,

		iWidf,
		iLenf,

		nNumOfSquaresInImageSidef = nDim_2pNf / nLenOfSquaref,

		nMassOfASquaref, 
		nMassOfASquareMaxf = nLenOfSquaref * nLenOfSquaref,

		nWidOfSquareMinf,
		nWidOfSquareMaxf,

		nLenOfSquareMinf,
		nLenOfSquareMaxf,

		iWidSquaresf,
		iLenSquaresf;

////////////////////////////////////////////////
	nNumOfSquaresInImageTotf = nNumOfSquaresInImageSidef * nNumOfSquaresInImageSidef;

	nNumOfNonZero_ObjectSquaresTotf = 0;
	nMassOfImageTotf = 0;

#ifndef COMMENT_OUT_ALL_PRINTS
	//nMassOfASquareMaxf = nLenOfSquaref * nLenOfSquaref,
		fprintf(fout_lr, "\n\n 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare' nDim_2pNf = %d, nNumOfSquaresInImageSidef = %d, nMassOfASquareMaxf = %d", 
			nDim_2pNf, nNumOfSquaresInImageSidef, nMassOfASquareMaxf);
	fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

	if (nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea)
	{
		fprintf(fout_lr, "\n 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea = %d", nNumOfSelected_SpectrumFea);
	}//if (nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iWidSquaresf = 0; iWidSquaresf < nNumOfSquaresInImageSidef; iWidSquaresf++)
	{
		nWidOfSquareMinf = iWidSquaresf * nLenOfSquaref;
		nWidOfSquareMaxf = (iWidSquaresf + 1)* nLenOfSquaref;

		for (iLenSquaresf = 0; iLenSquaresf < nNumOfSquaresInImageSidef; iLenSquaresf++)
		{
			nIndexOfSquareCurf = iLenSquaresf + iWidSquaresf*nNumOfSquaresInImageSidef;
			if (nIndexOfSquareCurf >= nNumOfSquaresInImageTotf)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nIndexOfSquareCurf = %d >= nNumOfSquaresInImageTotf = %d", nIndexOfSquareCurf, nNumOfSquaresInImageTotf);
				fprintf(fout_lr, "\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nIndexOfSquareCurf = %d >= nNumOfSquaresInImageTotf = %d", nIndexOfSquareCurf, nNumOfSquaresInImageTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				return UNSUCCESSFUL_RETURN;
			}//if (nIndexOfSquareCurf >= nNumOfSquaresInImageTotf)

			nLenOfSquareMinf = iLenSquaresf * nLenOfSquaref;
			nLenOfSquareMaxf = (iLenSquaresf + 1)* nLenOfSquaref;

			if (nLenOfSquareMinf > sColor_ImageInitf->nLength || nWidOfSquareMinf > sColor_ImageInitf->nWidth)
			{
				nMassOfASquaref = 0;
				goto MarkContinueForiLenSquares;
			}//if (nLenOfSquareMinf > sColor_ImageInitf->nLength || nWidOfSquareMinf > sColor_ImageInitf->nWidth)

			nMassOfASquaref = 0;
			for (iWidf = nWidOfSquareMinf; iWidf < nWidOfSquareMaxf; iWidf++)
			{
				for (iLenf = nLenOfSquareMinf; iLenf < nLenOfSquareMaxf; iLenf++)
				{
					nIndexOfPixelInEmbeddedImagef = iLenf + (iWidf*nDim_2pNf);

					if (nIndexOfPixelInEmbeddedImagef >= nIndexOfPixelMaxf)
					{
#ifndef COMMENT_OUT_ALL_PRINTS
						printf("\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nIndexOfPixelInEmbeddedImagef = %d >= nIndexOfPixelMaxf = %d", nIndexOfPixelInEmbeddedImagef, nIndexOfPixelMaxf);
						fprintf(fout_lr, "\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nIndexOfPixelInEmbeddedImagef = %d >= nIndexOfPixelMaxf = %d", nIndexOfPixelInEmbeddedImagef, nIndexOfPixelMaxf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

						return UNSUCCESSFUL_RETURN;
					}//if (nIndexOfPixelInEmbeddedImagef >= nIndexOfPixelMaxf)

					if (sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] == 1)
					{
						nMassOfASquaref += 1;
					} //if (sImageEmbeddedf_BlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] == 1)

				} //for (iLenf = nLenOfSquareMinf
				
			} // for (iWidf = nWidOfSquareMinf; iWidf < nWidOfSquareMaxf; iWidf++)

			nMassOfImageTotf += nMassOfASquaref;

		MarkContinueForiLenSquares: nMassesOfSquaresArrf[nIndexOfSquareCurf] = nMassOfASquaref;
			if (nMassOfASquaref > 0)
			{
				nNumOfNonZero_ObjectSquaresTotf += 1;
			}//if (nMassOfASquaref > 0)

			if (nMassOfASquaref > nMassOfASquareMaxf)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nMassOfASquaref = %d > nMassOfASquareMaxf = %d", nMassOfASquaref, nMassOfASquareMaxf);
				printf( "\n iLenSquaresf = %d, iWidSquaresf = %d, nNumOfSquaresInImageSidef = %d", iLenSquaresf, iWidSquaresf, nNumOfSquaresInImageSidef);

				fprintf(fout_lr, "\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nMassOfASquaref = %d > nMassOfASquareMaxf = %d", nMassOfASquaref, nMassOfASquareMaxf);

				fprintf(fout_lr, "\n iLenSquaresf = %d, iWidSquaresf = %d, nNumOfSquaresInImageSidef = %d", iLenSquaresf, iWidSquaresf, nNumOfSquaresInImageSidef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				return UNSUCCESSFUL_RETURN;
			}//if (nMassOfASquaref > nMassOfASquareMaxf)
		} //for (iLenSquaresf = 0; iLenSquaresf < nNumOfSquaresInImageSidef; iLenSquaresf++)

	} // for (iWidSquaresf = 0; iWidSquaresf < nNumOfSquaresInImageSidef; iWidSquaresf++)

	//if (nMassOfImageTotf < 0)
		if (nMassOfImageTotf <= 0)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nMassOfImageTotf = %d <= 0", nMassOfImageTotf);

		fprintf(fout_lr, "\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare':  nMassOfImageTotf = %d <= 0", nMassOfImageTotf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		return UNSUCCESSFUL_RETURN;
	} // if (nMassOfImageTotf <= 0)

#ifndef COMMENT_OUT_ALL_PRINTS
	//printf("\n\n The end of 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nNumOfNonZero_ObjectSquaresTotf = %d, nNumOfSquaresInImageTotf = %d",
		//nNumOfNonZero_ObjectSquaresTotf, nNumOfSquaresInImageTotf);

	//printf("\n nLenOfSquaref = %d, nNumOfSquaresInImageSidef = %d, nMassOfImageTotf = %d", nLenOfSquaref, nNumOfSquaresInImageSidef, nMassOfImageTotf);

	fprintf(fout_lr, "\n\n The end of 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare':  nNumOfNonZero_ObjectSquaresTotf = %d, nNumOfSquaresInImageTotf = %d",
		nNumOfNonZero_ObjectSquaresTotf, nNumOfSquaresInImageTotf);

	fprintf(fout_lr, "\n nLenOfSquaref = %d, nNumOfSquaresInImageSidef = %d, nMassOfImageTotf = %d", nLenOfSquaref, nNumOfSquaresInImageSidef, nMassOfImageTotf);
	fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

	if (nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea)
	{
		fprintf(fout_lr, "\n 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea = %d", nNumOfSelected_SpectrumFea);
	}//if (nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea)

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	return SUCCESSFUL_RETURN;
} //int MassesOfAll_ObjectSquares_AtFixedLenOfSquare(...
/////////////////////////////////////////////////////////////////////////////////////////

//https://www.varsitytutors.com/hotmath/hotmath_help/topics/line-of-best-fit
void SlopeOfAStraightLine(
	const int nDimf,

	const float fX_Arrf[],
	const float fY_Arrf[],

	float &fSlopef)
{
	int
		i;
	float
		fX_Diff,
		fX_Averf = 0.0,
		fY_Averf = 0.0,

		fSumForNumeratorf = 0.0,
		fSumForDenominatorf = 0.0;

	for (i = 0; i < nDimf; i++)
	{
		fX_Averf += fX_Arrf[i];
		fY_Averf += fY_Arrf[i];

#ifndef COMMENT_OUT_ALL_PRINTS
		//printf("\n\n 'SlopeOfAStraightLine' 1: i = %d, fX_Averf = %E, fY_Averf = %E, fY_Arrf[i] = %E", i, fX_Averf, fY_Averf, fY_Arrf[i]);
		fprintf(fout_lr, "\n\n 'SlopeOfAStraightLine'   1: i = %d, fX_Averf = %E, fY_Averf = %E, fY_Arrf[i] = %E", i, fX_Averf, fY_Averf, fY_Arrf[i]);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	}//for (i = 0; i < nDimf; i++)

	fX_Averf = fX_Averf / nDimf;
	fY_Averf = fY_Averf / nDimf;

#ifndef COMMENT_OUT_ALL_PRINTS
//	printf("\n\n 'SlopeOfAStraightLine': nDimf = %d, fX_Averf = %E, fY_Averf = %E", nDimf, fX_Averf, fY_Averf);
	fprintf(fout_lr, "\n\n 'SlopeOfAStraightLine': nDimf = %d, fX_Averf = %E, fY_Averf = %E", nDimf, fX_Averf, fY_Averf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
	for (i = 0; i < nDimf; i++)
	{
		fX_Diff = fX_Arrf[i] - fX_Averf;

		fSumForNumeratorf += fX_Diff * (fY_Arrf[i] - fY_Averf);

		fSumForDenominatorf += fX_Diff * fX_Diff;
#ifndef COMMENT_OUT_ALL_PRINTS
		//printf("\n\n 'SlopeOfAStraightLine': i = %d, fSumForNumeratorf = %E, fSumForDenominatorf = %E", i, fSumForNumeratorf, fSumForDenominatorf);
		fprintf(fout_lr, "\n\n 'SlopeOfAStraightLine': i = %d, fSumForNumeratorf = %E, fSumForDenominatorf = %E", i, fSumForNumeratorf, fSumForDenominatorf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
	}//for (i = 0; i < nDimf; i++)

	if (fSumForDenominatorf > 0.0)
	{
		fSlopef = fSumForNumeratorf / fSumForDenominatorf;
	}//if (fSumForDenominatorf > 0.0)
	else
		fSlopef = fLarge;

}//void SlopeOfAStraightLine(...
  ////////////////////////////////////////////////////////////////////////

int MomentOf_Q_order_AtFixedLenOfSquareMultiFrac(

	const int nDim_2pNf,

	const int nLenOfSquaref,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	const float fQf,
	const int nIntOrFloatf, //1 -- int, 0 -- float
///////////////////////////////////////
	int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

	float &fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf,

	float &fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf,
	float &fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf)
{
	int MassesOfAll_ObjectSquares_AtFixedLenOfSquare(
		const int nDim_2pNf,

		const int nLenOfSquaref,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		int &nNumOfSquaresInImageTotf,

		int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

		int &nMassOfImageTotf,
		int nMassesOfSquaresArrf[]); //[nNumOfSquaresInImageTotf]

	float PowerOfAFloatNumber(
		const int nIntOrFloatf, //1 -- int, 0 -- float
		const int nPowerf,
		const float fPowerf,

		const float fFloatInitf); //fFloatInitf != 0.0
	int
		iSquaref,

		nPowerf,

		nNumOfSquaresInImageSidef = nDim_2pNf / nLenOfSquaref,

		nNumOfSquaresInImagef, // = nNumOfSquaresInImageSidef* nNumOfSquaresInImageSidef,

		nNumOfSquaresInImageTotf,
		//nNumOfNonZero_ObjectSquaresTotf,

		nNumOfNonZero_ObjectSquaresCurf,
		nMassOfASquaref,

		nMassOfImageTotf,
		nMassOfASquareMaxf = nLenOfSquaref * nLenOfSquaref,

		nResf;

	float
		fMassProportionForASquaref,
		fPowerf,

		fSumOfMomentsf = 0.0, 

		fMomentOf_Q_OrderAtFixedLenCurf;
	/////////////////////////////

	if (nIntOrFloatf == 1)
	{
		nPowerf = (int)(fQf);
		fPowerf = -fLarge;
	} //if (nIntOrFloatf == 1)
	else if (nIntOrFloatf == 0)
	{
		nPowerf = -nLarge;
		fPowerf = fQf;
	}//else if (nIntOrFloatf == 0)
	else
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nIntOrFloatf = %d", nIntOrFloatf);
		fprintf(fout_lr, "\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nIntOrFloatf = %d", nIntOrFloatf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		return UNSUCCESSFUL_RETURN;
	}//else

	nNumOfSquaresInImagef = nNumOfSquaresInImageSidef * nNumOfSquaresInImageSidef;

	if (nNumOfSquaresInImagef <= 1)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nNumOfSquaresInImagef = %d <= 1", nNumOfSquaresInImagef);
		fprintf(fout_lr, "\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nNumOfSquaresInImagef = %d <= 1", nNumOfSquaresInImagef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		return UNSUCCESSFUL_RETURN;
	}//if (nNumOfSquaresInImagef <= 1)
//////////////////////////////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
	//printf("\n\n 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nLenOfSquaref = %d, nDim_2pNf = %d, fQf = %E, nIntOrFloatf = %d", nLenOfSquaref, nDim_2pNf, fQf,nIntOrFloatf);
	fprintf(fout_lr, "\n\n 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nLenOfSquaref = %d, nDim_2pNf = %d, fQf = %E, nIntOrFloatf = %d", nLenOfSquaref, nDim_2pNf, fQf, nIntOrFloatf);

	fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

	if (nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea)
	{
		fprintf(fout_lr, "\n 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea = %d", nNumOfSelected_SpectrumFea);
	}//if (nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
////////////////////////////////////////////////////////////////
	int *nMassesOfSquaresArrf;
	nMassesOfSquaresArrf = new int[nNumOfSquaresInImagef];

	if (nMassesOfSquaresArrf == nullptr)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in dynamic memory allocation for 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac'");
		fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac'");
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		return UNSUCCESSFUL_RETURN;
	} //if (nMassesOfSquaresArrf == nullptr)

	nResf = MassesOfAll_ObjectSquares_AtFixedLenOfSquare(
		nDim_2pNf, //const int nDim_2pNf,

		nLenOfSquaref, //const int nLenOfSquaref,

		sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

		sImageEmbeddedf_BlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		nNumOfSquaresInImageTotf, //int &nNumOfSquaresInImageTotf,

		nNumOfNonZero_ObjectSquaresTotf, //int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

		nMassOfImageTotf, //int &nMassOfImageTotf,

		nMassesOfSquaresArrf); // int nMassesOfSquaresArrf[]); //[nNumOfSquaresTotf]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		delete[] nMassesOfSquaresArrf;
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

	if (nNumOfNonZero_ObjectSquaresTotf <= nNumOfNonZero_ObjectSquaresTotMin) //nNumOfNonZero_ObjectSquaresTotMin == 1)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n Too few object squares in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nNumOfNonZero_ObjectSquaresTotf = %d <= 1",nNumOfNonZero_ObjectSquaresTotf);

		fprintf(fout_lr, "\n\n Too few object squares in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nNumOfNonZero_ObjectSquaresTotf = %d <= 1",	nNumOfNonZero_ObjectSquaresTotf);
		//printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf = 0.0;
		fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf = 0.0;
		fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf = 0.0;

		delete[] nMassesOfSquaresArrf;
		return (-2);
	}//if (nNumOfNonZero_ObjectSquaresTotf <= nNumOfNonZero_ObjectSquaresTotMin) //nNumOfSquareOccurrence_Intervalsf)
//////////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
printf("\n\n 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nLenOfSquaref = %d,  nNumOfSquaresInImagef = %d, nNumOfSquaresInImageTotf = %d,  nNumOfNonZero_ObjectSquaresTotf = %d",
	nLenOfSquaref, nNumOfSquaresInImagef, nNumOfSquaresInImageTotf, nNumOfNonZero_ObjectSquaresTotf);
	printf("\n  nMassOfImageTotf = %d", nMassOfImageTotf);

	fprintf(fout_lr, "\n\n 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nLenOfSquaref = %d,  nNumOfSquaresInImagef = %d, nNumOfSquaresInImageTotf = %d,  nNumOfNonZero_ObjectSquaresTotf = %d",
		nLenOfSquaref, nNumOfSquaresInImagef, nNumOfSquaresInImageTotf, nNumOfNonZero_ObjectSquaresTotf);
	fprintf(fout_lr, "\n  nDim_2pNf = %d, nMassOfImageTotf = %d", nMassOfImageTotf, nDim_2pNf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	if (nNumOfSquaresInImagef != nNumOfSquaresInImageTotf)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nNumOfSquaresInImagef = %d != nNumOfSquaresInImageTotf = %d",
			nNumOfSquaresInImagef, nNumOfSquaresInImageTotf);
		fprintf(fout_lr, "\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nNumOfSquaresInImagef = %d != nNumOfSquaresInImageTotf = %d",
			nNumOfSquaresInImagef, nNumOfSquaresInImageTotf);

		printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		delete[] nMassesOfSquaresArrf;
		return UNSUCCESSFUL_RETURN;
	}//if (nNumOfSquaresInImagef != nNumOfSquaresInImageTotf)

	///////////////////////////////////////////////////////////////
	float *fMomentsOfSquaresArrf;
	fMomentsOfSquaresArrf = new float[nNumOfSquaresInImageTotf];

	if (fMomentsOfSquaresArrf == nullptr)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in dynamic memory allocation for 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac'");
		fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac'");
		printf("\n\n Please press any key:"); getchar();
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		delete[] nMassesOfSquaresArrf;

		return UNSUCCESSFUL_RETURN;
	} //if (fMomentsOfSquaresArrf == nullptr)

	float *fMomentsNormalizedOfSquaresArrf;
	fMomentsNormalizedOfSquaresArrf = new float[nNumOfSquaresInImageTotf];

	if (fMomentsNormalizedOfSquaresArrf == nullptr)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in dynamic memory allocation for 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac'");
		fprintf(fout_lr, "\n\n An error in dynamic memory allocation for 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac'");
		printf("\n\n Please press any key:"); getchar();
#endif // #ifndef COMMENT_OUT_ALL_PRINTS

		delete[] nMassesOfSquaresArrf;
		delete[] fMomentsOfSquaresArrf;

		return UNSUCCESSFUL_RETURN;
	} //if (fMomentsNormalizedOfSquaresArrf == nullptr)

/////////////////////////////////////////////////////////////	
	nNumOfNonZero_ObjectSquaresCurf = 0;
	fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf = 0.0;

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': before the loop for iSquaref, nIntOrFloatf = %d, nPowerf = %d, fPowerf = %E",
		nIntOrFloatf, nPowerf, fPowerf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
	
	for (iSquaref = 0; iSquaref < nNumOfSquaresInImageTotf; iSquaref++)
	{
		nMassOfASquaref = nMassesOfSquaresArrf[iSquaref];

		if (nMassOfASquaref > 0)
		{
			nNumOfNonZero_ObjectSquaresCurf += 1;

			fMassProportionForASquaref = (float)(nMassOfASquaref) / (float)(nMassOfImageTotf);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n iSquaref = %d, fMassProportionForASquaref = %E, nMassOfASquaref = %d, nMassOfImageTotf = %d",
				iSquaref, fMassProportionForASquaref, nMassOfASquaref, nMassOfImageTotf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			if (nMassOfASquaref < 0 || nMassOfASquaref > nMassOfImageTotf)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nMassOfASquaref < 0 || nMassOfASquaref > nMassOfImageTotf");
				fprintf(fout_lr, "\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nMassOfASquaref < 0 || nMassOfASquaref > nMassOfImageTotf");

				printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				delete[] nMassesOfSquaresArrf;

				delete[] fMomentsOfSquaresArrf;
				delete[] fMomentsNormalizedOfSquaresArrf;

				return UNSUCCESSFUL_RETURN;
			}//if (nMassOfASquaref < 0 || nMassOfASquaref > nMassOfImageTotf)

			fMomentOf_Q_OrderAtFixedLenCurf =  PowerOfAFloatNumber(
								nIntOrFloatf, //const int nIntOrFloatf, //1 -- int, 0 -- float
				nPowerf, //const int nPowerf,
				fPowerf, //const float fPowerf,

				fMassProportionForASquaref); // const float fFloatInitf); //fFloatInitf != 0.0

			fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf += fMomentOf_Q_OrderAtFixedLenCurf;

			fMomentsOfSquaresArrf[iSquaref] = fMomentOf_Q_OrderAtFixedLenCurf; //to be normalized later

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': iSquaref = %d, fMomentOf_Q_OrderAtFixedLenCurf = %E, fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf = %E",
				iSquaref, fMomentOf_Q_OrderAtFixedLenCurf, fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf);
			fprintf(fout_lr, "\n fMomentsOfSquaresArrf[%d] = %E, nPowerf = %d, fPowerf = %E", 
				iSquaref, fMomentsOfSquaresArrf[iSquaref], nPowerf, fPowerf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			//if (fMomentsOfSquaresArrf[iSquaref] <= 0.0)
			if (fMomentsOfSquaresArrf[iSquaref] < 0.0)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': fMomentsOfSquaresArrf[%d] = %E", iSquaref, fMomentsOfSquaresArrf[iSquaref]);
				fprintf(fout_lr, "\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': fMomentsOfSquaresArrf[%d] = %E", iSquaref, fMomentsOfSquaresArrf[iSquaref]);

				//printf("\n\n Please press any key:"); getchar();
				printf("\n\n Please press any key to exit:"); fflush(fout_lr);  getchar();  exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				delete[] nMassesOfSquaresArrf;
				delete[] fMomentsOfSquaresArrf;

				delete[] fMomentsNormalizedOfSquaresArrf;
				return UNSUCCESSFUL_RETURN;
			}//if (fMomentsOfSquaresArrf[iSquaref] < 0.0)

		} //if (nMassOfASquaref > 0)

	}//for (iSquaref = 0; iSquaref < nNumOfSquaresInImageTotf; iSquaref++)

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac' (before normalization): nPowerf = %d, fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf = %E, nNumOfSquaresInImageTotf = %d",
		nPowerf, fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf, nNumOfSquaresInImageTotf);

	//printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	if (fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf <= fCloseToZeroLimitForSumOfMomentsOfSquares)
	{
/*
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nPowerf = %d, fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf = %E",
			nPowerf, fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf);
		fprintf(fout_lr, "\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': nPowerf = %d, fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf = %E",
			nPowerf, fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf);

		printf("\n\n nLenOfSquaref = %d,  nNumOfSquaresInImagef = %d, nNumOfSquaresInImageTotf = %d,  nNumOfNonZero_ObjectSquaresTotf = %d",
			nLenOfSquaref, nNumOfSquaresInImagef, nNumOfSquaresInImageTotf, nNumOfNonZero_ObjectSquaresTotf);
		fprintf(fout_lr, "\n  nDim_2pNf = %d, nMassOfImageTotf = %d", nMassOfImageTotf, nDim_2pNf);

		fprintf(fout_lr, "\n\n nLenOfSquaref = %d,  nNumOfSquaresInImagef = %d, nNumOfSquaresInImageTotf = %d,  nNumOfNonZero_ObjectSquaresTotf = %d",
			nLenOfSquaref, nNumOfSquaresInImagef, nNumOfSquaresInImageTotf, nNumOfNonZero_ObjectSquaresTotf);
		fprintf(fout_lr, "\n  nDim_2pNf = %d, nMassOfImageTotf = %d", nMassOfImageTotf, nDim_2pNf);
		printf("\n\n Please press any key to exit:"); getchar(); exit(1);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		delete[] nMassesOfSquaresArrf;
		delete[] fMomentsOfSquaresArrf;

		delete[] fMomentsNormalizedOfSquaresArrf;
		return UNSUCCESSFUL_RETURN;
*/

#ifndef COMMENT_OUT_ALL_PRINTS
		//printf("\n\n Exit in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf = %E < fCloseToZeroLimitForSumOfMomentsOfSquares = %E",
			//fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf, fCloseToZeroLimitForSumOfMomentsOfSquares);

		fprintf(fout_lr, "\n\n  Exit in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf = %E < fCloseToZeroLimitForSumOfMomentsOfSquares = %E",
			fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf, fCloseToZeroLimitForSumOfMomentsOfSquares);
		//printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf = 0.0;
		fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf = 0.0;
		fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf = 0.0;

		delete[] nMassesOfSquaresArrf;
		delete[] fMomentsOfSquaresArrf;

		delete[] fMomentsNormalizedOfSquaresArrf;

		return (-2);
	}//if (fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf <= fCloseToZeroLimitForSumOfMomentsOfSquares)

///////////////////////////////////////////////////////////////
//for spectrum and crowding index
	
	nNumOfNonZero_ObjectSquaresCurf = 0;
	fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf = 0.0;
	fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf = 0.0;

	for (iSquaref = 0; iSquaref < nNumOfSquaresInImageTotf; iSquaref++)
	{
		nMassOfASquaref = nMassesOfSquaresArrf[iSquaref];

		if (nMassOfASquaref > 0)
		{
			nNumOfNonZero_ObjectSquaresCurf += 1;

			if (fMomentsOfSquaresArrf[iSquaref] < fCloseToZeroLimitForMomentsOfSquares)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n Continue for iSquaref = %d, fMomentsOfSquaresArrf[iSquaref] = %E < fCloseToZeroLimitForMomentsOfSquares = %E",
					iSquaref, fMomentsOfSquaresArrf[iSquaref], fCloseToZeroLimitForMomentsOfSquares);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
				continue;
			} // if (fMomentsOfSquaresArrf[iSquaref] < fCloseToZeroLimitForMomentsOfSquares)
//normalization
			fMomentsNormalizedOfSquaresArrf[iSquaref] = fMomentsOfSquaresArrf[iSquaref] / fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n Normalization: iSquaref = %d, fMomentsNormalizedOfSquaresArrf[iSquaref] = %E, fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf = %E",
				iSquaref, fMomentsNormalizedOfSquaresArrf[iSquaref], fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			if (fMomentsNormalizedOfSquaresArrf[iSquaref] <= 0.0)
			//if (fMomentsNormalizedOfSquaresArrf[iSquaref] < 0.0)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': fMomentsNormalizedOfSquaresArrf[%d] = %E", iSquaref,fMomentsNormalizedOfSquaresArrf[iSquaref]);
				fprintf(fout_lr, "\n\n An error in 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': fMomentsNormalizedOfSquaresArrf[%d] = %E", iSquaref, fMomentsNormalizedOfSquaresArrf[iSquaref]);

				printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				delete[] nMassesOfSquaresArrf;
				delete[] fMomentsOfSquaresArrf;

				delete[] fMomentsNormalizedOfSquaresArrf;
				return UNSUCCESSFUL_RETURN;
			}//if (fMomentsNormalizedOfSquaresArrf[iSquaref] < 0.0)

			fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf += fMomentsNormalizedOfSquaresArrf[iSquaref]*log (fMomentsNormalizedOfSquaresArrf[iSquaref]);

			fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf += fMomentsNormalizedOfSquaresArrf[iSquaref] * log(fMomentsOfSquaresArrf[iSquaref]);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': iSquaref = %d, fMomentsNormalizedOfSquaresArrf[iSquaref] = %E, fMomentsOfSquaresArrf[iSquaref] = %E", 
				iSquaref,fMomentsNormalizedOfSquaresArrf[iSquaref], fMomentsOfSquaresArrf[iSquaref]);

			fprintf(fout_lr, "\n fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf = %E, fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf = %E",
				fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf, fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);

			//fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		} //if (nMassOfASquaref > 0)

	}//for (iSquaref = 0; iSquaref < nNumOfSquaresInImageTotf; iSquaref++)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n The end of 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf = %E, fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf = %E, fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf = %E",
		fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf, fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf, fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);

	printf( "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d",
		nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

	fprintf(fout_lr, "\n\n The end of 'MomentOf_Q_order_AtFixedLenOfSquareMultiFrac': fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf = %E, fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf = %E, fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf = %E",
		fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf,fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf, fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);

	fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", 
		nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
//the end for spectrum and crowding index
/////////////////////////////////

	delete[] nMassesOfSquaresArrf;
	delete[] fMomentsOfSquaresArrf;

	delete[] fMomentsNormalizedOfSquaresArrf;

	return SUCCESSFUL_RETURN;
} // int MomentOf_Q_order_AtFixedLenOfSquareMultiFrac(...
//////////////////////////////////////////////////////////////////////

int GenerDim_Spectrum_CrowdingIndex_Of_Q_order(

	const int nDim_2pNf,
		const int nScalef,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	const float fQf,
	const int nIntOrFloatf, //1 -- int, 0 -- float
///////////////////////////////////////
	float &fGenerDim_Order_Qf,
	float &fSpectrum_Order_Qf,

	float &fCrowdingIndex_Order_Qf)
{
	int MomentOf_Q_order_AtFixedLenOfSquareMultiFrac(

		const int nDim_2pNf,

		const int nLenOfSquaref,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		const float fQf,
		const int nIntOrFloatf, //1 -- int, 0 -- float
	///////////////////////////////////////
		int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

		float &fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf,
		float &fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf,
		float &fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);

	void SlopeOfAStraightLine(
		const int nDimf,

		const float fX_Arrf[],
		const float fY_Arrf[],

		float &fSlopef);

	int
		nNumOfNonZero_ObjectSquaresTotf,

		nResf,
		iScalef,
		//nScalef = 1,

		iIterf,
		nNumOfLogPointsf,

		nIsfQ_0f = 0, // 1 if it is 
		nIsfQ_1f = 0, // 1 if it is 

		nLenOfSquaref;

	float
		fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf,
			fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf,
			fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf,
		fSumOfMomentsf,

		//fMomentNormalizedf,

		fDiff,

		fNegLogOfLenOfSquare_Arrf[nNumOfIters_ForDim_2powerN_Max],

		fLogPoints_GenerDim_Arrf[nNumOfIters_ForDim_2powerN_Max],
		fLogPoints_Spectrum_Arrf[nNumOfIters_ForDim_2powerN_Max],
		fLogPoints_CrowdingIndex_Arrf[nNumOfIters_ForDim_2powerN_Max],

		fMoments_Arrf[nNumOfIters_ForDim_2powerN_Max],
		fMomentsNormalized_Arrf[nNumOfIters_ForDim_2powerN_Max],

		fLogPoint_GenerDimf = -1.0,
		fLogPoint_Spectrumf = -1.0,
		fLogPoint_CrowdingIndexf = -1.0;

	///////////////////////////////////////
	nNumOfSpectrumFea_Glob += 1;

	for (iIterf = 0; iIterf < nNumOfIters_ForDim_2powerN_Max; iIterf++)
	{
		fNegLogOfLenOfSquare_Arrf[iIterf] = -1.0;
		fLogPoints_GenerDim_Arrf[iIterf] = -1.0;

		fLogPoints_Spectrum_Arrf[iIterf] = -1.0;
		fLogPoints_CrowdingIndex_Arrf[iIterf] = -1.0;

		fMoments_Arrf[iIterf] = 0.0;
		fMomentsNormalized_Arrf[iIterf] = 0.0;

	} //for (iIterf = 0; iIterf < nNumOfIters_ForDim_2powerN_Max; iIterf++)
//////////////////////////////////
	if (fQf < feps && fQf > -feps)
	{
		nIsfQ_0f = 1;
		goto Mark_GenerDimOf_Q_order_1;
	} // if (fQf < feps && fQf > -feps)
///////////////////////
	fDiff = (float)(1.0) - fQf;
	if (fDiff < feps && fDiff > -feps)
	{
		nIsfQ_1f = 1; //information dimension
	} // if (fDiff < feps && fDiff > -feps)

	///////////////////////////////////////////
	Mark_GenerDimOf_Q_order_1:		nNumOfLogPointsf = 0; // nScalef;
		nLenOfSquaref = nDim_2pNf;

		fSumOfMomentsf = 0.0;
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nNumOfSpectrumFea_Glob = %d, iIntensity_Interval_1_Glob = %d, iIntensity_Interval_2_Glob = %d, fQf = %E, nScalef = %d", 
				nNumOfSpectrumFea_Glob,iIntensity_Interval_1_Glob, iIntensity_Interval_2_Glob, fQf, nScalef);

			if (nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea)
			{
				fprintf(fout_lr, "\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea = %d", nNumOfSelected_SpectrumFea);
			}//if (nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS

	for (iScalef = 0; iScalef < nScalef; iScalef++)
	{
		nLenOfSquaref = nLenOfSquaref / 2;

		if (nLenOfSquaref < 2)
		{
			break;
		}//if (nLenOfSquaref < 2)

		nResf = MomentOf_Q_order_AtFixedLenOfSquareMultiFrac(

			nDim_2pNf, //const int nDim_2pNf,

			nLenOfSquaref, //const int nLenOfSquaref,

			sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

			sImageEmbeddedf_BlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

			fQf, //const float fQf,
			nIntOrFloatf, //const int nIntOrFloatf, //1 -- int, 0 -- float
		///////////////////////////////////////
			nNumOfNonZero_ObjectSquaresTotf, //int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

			fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf,	// float &fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf);
				fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf, //float &fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf,
				fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf); // float &fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);

		fMoments_Arrf[iScalef] = fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nNumOfNonZero_ObjectSquaresTotf = %d, fQf = %E, nNumOfLogPointsf = %d", 
			nNumOfNonZero_ObjectSquaresTotf, fQf, nNumOfLogPointsf);

		fprintf(fout_lr, "\n fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf = %E, fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf = %E, fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf = %E",
			fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf, fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf, fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);

		fprintf(fout_lr, "\n  nNumOfSpectrumFea_Glob = %d, iScalef = %d, nLenOfSquaref = %d", nNumOfSpectrumFea_Glob,iScalef, nLenOfSquaref);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			return UNSUCCESSFUL_RETURN;
		}//if (nResf == UNSUCCESSFUL_RETURN)

		if (nResf == -2)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': continue for iScalef = %d, nLenOfSquaref = %d; not enough points",iScalef, nLenOfSquaref);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			continue;
		}//if (nResf == -2)

		fSumOfMomentsf += fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf;

		if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
		{
			if (fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf > 0.0) //> feps?
			{
				fLogPoint_GenerDimf = log(fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf);
			} //
			else
			{
				fLogPoint_GenerDimf = 0.0;
			}//else
/////////////////
			//	fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf
			if (fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf > 0.0) //> feps?
			{
				fLogPoint_Spectrumf = log(fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nIsfQ_0f == 0 && nIsfQ_1f == 0 and fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf > 0.0, iScalef = %d, nLenOfSquaref = %d, fLogPoint_Spectrumf = %E", 
					iScalef, nLenOfSquaref, fLogPoint_Spectrumf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
			} //
			else
			{
				fLogPoint_Spectrumf = 0.0;
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nIsfQ_0f == 0 && nIsfQ_1f == 0 and fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf = %E <= 0.0",
					fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf);

				fprintf(fout_lr, "\n iScalef = %d, nLenOfSquaref = %d, fLogPoint_Spectrumf = %E",iScalef, nLenOfSquaref, fLogPoint_Spectrumf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
			}//else
///////////////////
			if (fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf > 0.0) //> feps?
			{
				fLogPoint_CrowdingIndexf = log(fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);
			} //
			else
			{
				fLogPoint_CrowdingIndexf = 0.0;
			}//else

		} //if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
		else if (nIsfQ_0f == 1)
		{
			if (nNumOfNonZero_ObjectSquaresTotf <= 0)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nNumOfNonZero_ObjectSquaresTotf = %d <= 0", nNumOfNonZero_ObjectSquaresTotf);
				fprintf(fout_lr, "\n\n An error in 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nNumOfNonZero_ObjectSquaresTotf = %d <= 0", nNumOfNonZero_ObjectSquaresTotf);

				printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				return UNSUCCESSFUL_RETURN;
			}// if (nNumOfNonZero_ObjectSquaresTotf <= 0)

			fLogPoint_GenerDimf = log( (float)(nNumOfNonZero_ObjectSquaresTotf) );

			fLogPoint_Spectrumf = fLogPoint_GenerDimf;
			fLogPoint_CrowdingIndexf = fLogPoint_GenerDimf;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nIsfQ_0f == 1, iScalef = %d, nLenOfSquaref = %d, nNumOfNonZero_ObjectSquaresTotf = %d, fLogPoint_Spectrumf = %E",
				iScalef, nLenOfSquaref, nNumOfNonZero_ObjectSquaresTotf,fLogPoint_Spectrumf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		} //else if (nIsfQ_0f == 1)
		else if (nIsfQ_1f == 1)
		{
			if (fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf > 0.0) //> feps?
			{
				fLogPoint_GenerDimf = fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf *log(fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf);
			} //
			else
			{
				fLogPoint_GenerDimf = 0.0;
			}//else
/////////////////
			if (fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf > 0.0) //> feps?
			{
				fLogPoint_Spectrumf = fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf * log(fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf);

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nIsfQ_1f == 1 and fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf > 0.0, iScalef = %d, nLenOfSquaref = %d, fLogPoint_Spectrumf = %E",
					iScalef, nLenOfSquaref, fLogPoint_Spectrumf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
			} //
			else
			{
				fLogPoint_Spectrumf = 0.0;
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nIsfQ_1f == 0 and fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf <= 0.0, iScalef = %d, nLenOfSquaref = %d, fLogPoint_Spectrumf = %E",
					iScalef, nLenOfSquaref, fLogPoint_Spectrumf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			}//else
////////////////////
			if (fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf > 0.0) //> feps?
			{
				fLogPoint_CrowdingIndexf = fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf * log(fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);
			} //
			else
			{
				fLogPoint_CrowdingIndexf = 0.0;
			}//else

		} //else if (nIsfQ_1f == 1)

		fNegLogOfLenOfSquare_Arrf[nNumOfLogPointsf] = -log((float)(nLenOfSquaref));

		fLogPoints_GenerDim_Arrf[nNumOfLogPointsf] = fLogPoint_GenerDimf;

		fLogPoints_Spectrum_Arrf[nNumOfLogPointsf] = fLogPoint_Spectrumf; ////fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf;
			fLogPoints_CrowdingIndex_Arrf[nNumOfLogPointsf] = fLogPoint_CrowdingIndexf; ////fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf;

#ifndef COMMENT_OUT_ALL_PRINTS
		//printf("\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': iScalef = %d, nLenOfSquaref = %d, fLogPoints_GenerDim_Arrf[%d] = %E, fNegLogOfLenOfSquare_Arrf[%d] = %E", 
			//iScalef, nLenOfSquaref, nNumOfLogPointsf, fLogPoints_GenerDim_Arrf[nNumOfLogPointsf], nNumOfLogPointsf,fNegLogOfLenOfSquare_Arrf[nNumOfLogPointsf]);

		fprintf(fout_lr, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': iScalef = %d, nLenOfSquaref = %d, fLogPoints_GenerDim_Arrf[%d] = %E, fNegLogOfLenOfSquare_Arrf[%d] = %E",
			iScalef, nLenOfSquaref, nNumOfLogPointsf, fLogPoints_GenerDim_Arrf[nNumOfLogPointsf], nNumOfLogPointsf, fNegLogOfLenOfSquare_Arrf[nNumOfLogPointsf]);

		fprintf(fout_lr, "\n nNumOfLogPointsf = %d, fLogPoints_Spectrum_Arrf[%d]  = %E", nNumOfLogPointsf, nNumOfLogPointsf,fLogPoints_Spectrum_Arrf[nNumOfLogPointsf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		//
		nNumOfLogPointsf += 1;
	}//for (iScalef = 0; iScalef < nScalef; iScalef++)
/////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
	fprintf(fout_lr, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nNumOfLogPointsf = %d, iIntensity_Interval_1_Glob = %d, iIntensity_Interval_2_Glob = %d, fQf = %E", 
		nNumOfLogPointsf, iIntensity_Interval_1_Glob , iIntensity_Interval_2_Glob, fQf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
////////////////////////////////////////////////////////////////////////////////

	if (nNumOfLogPointsf <= 1)
	{
		fGenerDim_Order_Qf = -1.0;

		fSpectrum_Order_Qf = -1.0;
		fCrowdingIndex_Order_Qf = -1.0;

#ifndef COMMENT_OUT_ALL_PRINTS

		fprintf(fout_lr, "\n\n The end of 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nNumOfLogPointsf = %d, no 'SlopeOfAStraightLine', fGenerDim_Order_Qf = %E, fSpectrum_Order_Qf = %E, fCrowdingIndex_Order_Qf = %E", 
			nNumOfLogPointsf,fGenerDim_Order_Qf, fSpectrum_Order_Qf, fCrowdingIndex_Order_Qf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	}//if (nNumOfLogPointsf <= 1)
	else
	{
#ifndef COMMENT_OUT_ALL_PRINTS

		fprintf(fout_lr, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': 'SlopeOfAStraightLine' for fGenerDim_Order_Qf");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		SlopeOfAStraightLine(
			nNumOfLogPointsf, //const int nDimf,

			fNegLogOfLenOfSquare_Arrf, //const float fX_Arrf[],
			fLogPoints_GenerDim_Arrf, //const float fY_Arrf[],

			fGenerDim_Order_Qf); // float &fSlopef);

		fDiff = (float)(1.0) - fQf; //above

		if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
		{
			fGenerDim_Order_Qf = fGenerDim_Order_Qf / fDiff;
		} // if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
//////////////
#ifndef COMMENT_OUT_ALL_PRINTS

		fprintf(fout_lr, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': 'SlopeOfAStraightLine' for fSpectrum_Order_Qf");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		SlopeOfAStraightLine(
			nNumOfLogPointsf, //const int nDimf,

			fNegLogOfLenOfSquare_Arrf, //const float fX_Arrf[],
			fLogPoints_Spectrum_Arrf, //const float fY_Arrf[],

			fSpectrum_Order_Qf); // float &fSlopef);

///////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS

		fprintf(fout_lr, "\n\n 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': 'SlopeOfAStraightLine' for fCrowdingIndex_Order_Qf");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
	SlopeOfAStraightLine(
		nNumOfLogPointsf, //const int nDimf,

		fNegLogOfLenOfSquare_Arrf, //const float fX_Arrf[],
		fLogPoints_CrowdingIndex_Arrf, //const float fY_Arrf[],

		fCrowdingIndex_Order_Qf); // float &fSlopef);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n The end of 'GenerDim_Spectrum_CrowdingIndex_Of_Q_order': nNumOfLogPointsf = %d, fQf = %E, fDiff = %E", 
			nNumOfLogPointsf, fQf, fDiff);
		
		fprintf(fout_lr, "\n nDim_2pNf = %d, nScalef = %d, fGenerDim_Order_Qf = %E, fSpectrum_Order_Qf = %E, fCrowdingIndex_Order_Qf = %E", 
			nDim_2pNf, nScalef,fGenerDim_Order_Qf, fSpectrum_Order_Qf, fCrowdingIndex_Order_Qf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	} // else

#ifndef COMMENT_OUT_ALL_PRINTS	
	fprintf(fout_lr, "\n nNumOfSpectrumFea_Glob = %d", nNumOfSpectrumFea_Glob);
	if (nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea)
	{
		fprintf(fout_lr, "\n nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea = %d", nNumOfSelected_SpectrumFea);
	}//if (nNumOfSpectrumFea_Glob == nNumOfSelected_SpectrumFea)
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

////////////////////////////////////////////////
	return SUCCESSFUL_RETURN;
}//int GenerDim_Spectrum_CrowdingIndex_Of_Q_order(...
//////////////////////////////////////////////

int GenerDims_Spectrum_CrowdingIndex_Of_All_Q_orders_AtIntensityRange(

	const int nDim_2pNf,
		const int nScalef,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	float fGenerDim_Of_All_Orders_Arrf[], //[nNumOf_Qs_Tot]
	float fSpectrum_Of_All_Orders_Arrf[],
	float fCrowdingIndex_Of_All_Orders_Arrf[])
{
	int GenerDim_Spectrum_CrowdingIndex_Of_Q_order(

		const int nDim_2pNf,
		const int nScalef,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		const float fQf,
		const int nIntOrFloatf, //1 -- int, 0 -- float
	///////////////////////////////////////
		float &fGenerDim_Order_Qf,
		float &fSpectrum_Order_Qf,

		float &fCrowdingIndex_Order_Qf);
	int
		nResf,
		iQf,

		nWidthOfQ_Stepf, // ( (nQs_Max - nQs_Min)/(nNumOf_Qs_Tot - 1) )

		nIntOrFloatf; //1 -- int, 0 -- float

	float
		fQ_Curf,
		fWidthOfQ_Stepf,
		fGenerDim_Order_Qf,
		fSpectrum_Order_Qf,
		fCrowdingIndex_Order_Qf;

	fWidthOfQ_Stepf = (float)(nQs_Max - nQs_Min) / (float)(nNumOf_Qs_Tot - 1);

	nWidthOfQ_Stepf = (int)(fWidthOfQ_Stepf);

	if (fWidthOfQ_Stepf - (float)(nWidthOfQ_Stepf) > feps)
	{
		nIntOrFloatf = 0; //float
	} //if (fWidthOfQ_Stepf - (float)(nWidthOfQ_Stepf) > feps)
	else
	{
		nIntOrFloatf = 1; //int
	}//else

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'GenerDims_Spectrum_CrowdingIndex_Of_All_Q_orders_AtIntensityRange': fWidthOfQ_Stepf = %E, nIntOrFloatf = %d, nNumOf_Qs_Tot = %d, nQs_Min = %d, nQs_Max = %d, nDim_2pNf = %d", 
		fWidthOfQ_Stepf, nIntOrFloatf, nNumOf_Qs_Tot, nQs_Min, nQs_Max, nDim_2pNf);

	fprintf(fout_lr, "\n\n 'GenerDims_Spectrum_CrowdingIndex_Of_All_Q_orders_AtIntensityRange': fWidthOfQ_Stepf = %E, nIntOrFloatf = %d, nNumOf_Qs_Tot = %d, nQs_Min = %d, nQs_Max = %d, nDim_2pNf = %d",
		fWidthOfQ_Stepf, nIntOrFloatf, nNumOf_Qs_Tot, nQs_Min, nQs_Max, nDim_2pNf);
	fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
//////////////////
	for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)
	{
		fGenerDim_Of_All_Orders_Arrf[iQf] = 0.0;

		fQ_Curf = (float)(nQs_Min)+iQf * fWidthOfQ_Stepf;

		nResf = GenerDim_Spectrum_CrowdingIndex_Of_Q_order(

			nDim_2pNf, //const int nDim_2pNf,
				nScalef, //const int nScalef,

			sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

			sImageEmbeddedf_BlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

			fQ_Curf, //const float fQf,
			nIntOrFloatf, //const int nIntOrFloatf, //1 -- int, 0 -- float
		///////////////////////////////////////
			fGenerDim_Order_Qf, // float &fGenerDim_Order_Qf);
			fSpectrum_Order_Qf, //float &fSpectrum_Order_Qf,

			fCrowdingIndex_Order_Qf); // float &fCrowdingIndex_Order_Qf);

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			return UNSUCCESSFUL_RETURN;
		}//if (nResf == UNSUCCESSFUL_RETURN)

		fGenerDim_Of_All_Orders_Arrf[iQf] = fGenerDim_Order_Qf;
		fSpectrum_Of_All_Orders_Arrf[iQf] = fSpectrum_Order_Qf;
		fCrowdingIndex_Of_All_Orders_Arrf[iQf] = fCrowdingIndex_Order_Qf;

#ifndef COMMENT_OUT_ALL_PRINTS
		//printf( "\n\n 'GenerDims_Spectrum_CrowdingIndex_Of_All_Q_orders_AtIntensityRange':  iQf = %d, fQ_Curf = %E", iQf, fQ_Curf);
		fprintf(fout_lr, "\n\n 'GenerDims_Spectrum_CrowdingIndex_Of_All_Q_orders_AtIntensityRange': iQf = %d, nNumOf_Qs_Tot = %d, fQ_Curf = %E", iQf, nNumOf_Qs_Tot,fQ_Curf);
		
		fprintf(fout_lr, "\n fGenerDim_Of_All_Orders_Arrf[%d] = %E, fSpectrum_Of_All_Orders_Arrf[%d] = %E, fCrowdingIndex_Of_All_Orders_Arrf[%d] = %E", 
			iQf, fGenerDim_Of_All_Orders_Arrf[iQf], iQf, fSpectrum_Of_All_Orders_Arrf[iQf], iQf, fCrowdingIndex_Of_All_Orders_Arrf[iQf]);

		fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

		fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
	}//for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)

	return SUCCESSFUL_RETURN;
}// GenerDims_Spectrum_CrowdingIndex_Of_All_Q_orders_AtIntensityRange(...
////////////////////////////////////////////////////////////////////////////

int GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges(

	//const int nDim_2pNf,
	const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

	const COLOR_IMAGE *sColor_ImageInitf,

	float fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[], //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]
	float fSpectrumOf_All_Orders_And_All_IntensityRanges_Arrf[], //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]
	float fCrowdingIndex_Of_All_Orders_And_All_IntensityRanges_Arrf[]) //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]

{
	int Initializing_Embedded_Image(
		const int nDim_2pNf,

		EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef);  //[nDim_2pNf*nDim_2pNf]

	int Embedding_Image_Into_2powerN_ForHausdorff(
		const int nDim_2pNf,

		const int nThresholdForIntensitiesMinf,
		const int nThresholdForIntensitiesMaxf,

		const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

		const COLOR_IMAGE *sColor_ImageInitf,
		EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef); //[nDim_2pNf*nDim_2pNf]

	int GenerDims_Spectrum_CrowdingIndex_Of_All_Q_orders_AtIntensityRange(

		const int nDim_2pNf,
		const int nScalef,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		float fGenerDim_Of_All_Orders_Arrf[], //[nNumOf_Qs_Tot]
		float fSpectrum_Of_All_Orders_Arrf[],
		float fCrowdingIndex_Of_All_Orders_Arrf[]);

	int
		nResf,
		nLenOfIntensityIntervalf = (nIntensityStatMax + 1)/ nNumOfIntensity_IntervalsForMultifractal, // 8
		iIntensity_Interval_1f,
		iIntensity_Interval_2f,
		nNumOfIntensity_IntervalsForMultifractalTotf,

		nScalef,
		nDim_2pNf,

		iIntensity_Intervalf,

		iQf,

		iIndexf,
		nTempf,

		//nProductf = nNumOf_Qs_Tot * nNumOfIntensity_IntervalsForMultifractal,
		nProductf = nNumOf_Qs_Tot * nNumOfIntensity_IntervalsForMultifractalTot,
		nThresholdForIntensitiesMinf,
		nThresholdForIntensitiesMaxf;

	float
		fGenerDim_Of_All_Orders_Arrf[nNumOf_Qs_Tot],
		fSpectrum_Of_All_Orders_Arrf[nNumOf_Qs_Tot],
		fCrowdingIndex_Of_All_Orders_Arrf[nNumOf_Qs_Tot];


/////////////////////////////////////////////////
//initialization
	for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)
	{
		fGenerDim_Of_All_Orders_Arrf[iQf] = 0.0;
	}//for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)

	for (iIntensity_Intervalf = 0; iIntensity_Intervalf < nNumOfIntensity_IntervalsForMultifractalTot; iIntensity_Intervalf++)
	{
		nTempf = iIntensity_Intervalf * nNumOf_Qs_Tot;
		for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)
		{
			iIndexf = iQf + nTempf;

			if (iIndexf >= nProductf)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges': iIndexf = %d >= nProductf = %d", iIndexf, nProductf);
				fprintf(fout_lr, "\n\n An error in 'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges': iIndexf = %d >= nProductf = %d", iIndexf, nProductf);
				printf("\n\n Please press any key:"); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
			//	delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
				return UNSUCCESSFUL_RETURN;
			} //if (iIndexf >= nProductf)

			fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[iIndexf] = 0.0;
			fSpectrumOf_All_Orders_And_All_IntensityRanges_Arrf[iIndexf] = 0.0;
			fCrowdingIndex_Of_All_Orders_And_All_IntensityRanges_Arrf[iIndexf] = 0.0;

		}//for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)

	}//for (iIntensity_Intervalf = 0; iIntensity_Intervalf < nNumOfIntensity_IntervalsForMultifractalTot; iIntensity_Intervalf++)

//////////////

	nResf = Dim_2powerN(
		sColor_ImageInitf->nLength, //const int nLengthf,
		sColor_ImageInitf->nWidth, //const int nWidthf,

		nScalef, //int &nScalef,

		nDim_2pNf); // int &nDim_2pNf);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		//delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges': nScalef = %d, nDim_2pNf = %d, nNumOfIntensity_IntervalsForMultifractalTot = %d", 
		nScalef, nDim_2pNf, nNumOfIntensity_IntervalsForMultifractalTot);
	fprintf(fout_lr, "\n\n 'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges':nScalef = %d, nDim_2pNf = %d, nNumOfIntensity_IntervalsForMultifractalTot = %d",
		nScalef, nDim_2pNf, nNumOfIntensity_IntervalsForMultifractalTot);

	fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

	fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	if (nScalef <= 1)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges': nScalef = %d", nScalef);
		fprintf(fout_lr, "\n\n An error in 'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges': nScalef = %d", nScalef);
		printf("\n\n Please press any key:"); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		//delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
		return UNSUCCESSFUL_RETURN;
	}//if (nScalef <= 1)

	EMBEDDED_IMAGE_BLACK_WHITE sImageEmbeddedf_BlackWhitef; //) //[nDim_2pNf*nDim_2pNf]

	nResf = Initializing_Embedded_Image(
		nDim_2pNf, //const int nDim_2pNf,

		&sImageEmbeddedf_BlackWhitef); // EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef) // //[nDim_2pNf*nDim_2pNf]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		//delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

	///////////////////////////////////////
	nNumOfIntensity_IntervalsForMultifractalTotf = 0;
	for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_1f++)
	{
		nThresholdForIntensitiesMinf = iIntensity_Interval_1f * nLenOfIntensityIntervalf;

		iIntensity_Interval_1_Glob = iIntensity_Interval_1f;
		for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_2f++)
		{
			iIntensity_Interval_2_Glob = iIntensity_Interval_2f;

			nNumOfIntensity_IntervalsForMultifractalTotf += 1;

			if (nNumOfIntensity_IntervalsForMultifractalTotf > nNumOfIntensity_IntervalsForMultifractalTot)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges': nNumOfIntensity_IntervalsForMultifractalTotf = %d > nNumOfIntensity_IntervalsForMultifractalTot = %d", 
					nNumOfIntensity_IntervalsForMultifractalTotf, nNumOfIntensity_IntervalsForMultifractalTot);

				printf( "\n iIntensity_Interval_1f = %d, iIntensity_Interval_2f = %d", iIntensity_Interval_1f, iIntensity_Interval_2f);

				fprintf(fout_lr, "\n\n An error in 'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges': nNumOfIntensity_IntervalsForMultifractalTotf = %d > nNumOfIntensity_IntervalsForMultifractalTot = %d",
					nNumOfIntensity_IntervalsForMultifractalTotf, nNumOfIntensity_IntervalsForMultifractalTot);

				fprintf(fout_lr, "\n iIntensity_Interval_1f = %d, iIntensity_Interval_2f = %d", iIntensity_Interval_1f, iIntensity_Interval_2f);
				printf("\n\n Please press any key:"); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
				return UNSUCCESSFUL_RETURN;
			} //if (nNumOfIntensity_IntervalsForMultifractalTotf > nNumOfIntensity_IntervalsForMultifractalTot)

			nThresholdForIntensitiesMaxf = iIntensity_Interval_2f * nLenOfIntensityIntervalf;

#ifndef COMMENT_OUT_ALL_PRINTS
			//fprintf(fout_lr, "\n\n 1: sWeightsOfColorsf->fWeightOfRed = %E, sWeightsOfColorsf->fWeightOfGreen = %E, sWeightsOfColorsf->fWeightOfBlue = %E",
				//sWeightsOfColorsf->fWeightOfRed, sWeightsOfColorsf->fWeightOfGreen, sWeightsOfColorsf->fWeightOfBlue);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			nResf = Embedding_Image_Into_2powerN_ForHausdorff(
				nDim_2pNf, //const int nDim_2pNf,

				nThresholdForIntensitiesMinf, //const int nThresholdForIntensitiesMinf,
				nThresholdForIntensitiesMaxf, //const int nThresholdForIntensitiesMaxf,

				sWeightsOfColorsf, //const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

				sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,
				&sImageEmbeddedf_BlackWhitef); // EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef) //[nDim_2pNf*nDim_2pNf]

			if (nResf == UNSUCCESSFUL_RETURN)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges': nScalef = %d", nScalef);

				fprintf(fout_lr, "\n\n An error in 'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges': nScalef = %d", nScalef);
				printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
				return UNSUCCESSFUL_RETURN;
			}//if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS	
			fprintf(fout_lr, "\n\n 'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges': nNumOfIntensity_IntervalsForMultifractalTotf = %d > nNumOfIntensity_IntervalsForMultifractalTot = %d",
				nNumOfIntensity_IntervalsForMultifractalTotf, nNumOfIntensity_IntervalsForMultifractalTot);

			fprintf(fout_lr, "\n iIntensity_Interval_1f = %d, iIntensity_Interval_2f = %d", iIntensity_Interval_1f, iIntensity_Interval_2f);
			fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			nResf = GenerDims_Spectrum_CrowdingIndex_Of_All_Q_orders_AtIntensityRange(

				nDim_2pNf, //const int nDim_2pNf,
				nScalef, //const int nScalef,

				sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

				&sImageEmbeddedf_BlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

				fGenerDim_Of_All_Orders_Arrf, // float fGenerDim_Of_All_Orders_Arrf[]); //[nNumOf_Qs_Tot]
				
				fSpectrum_Of_All_Orders_Arrf, //float fSpectrum_Of_All_Orders_Arrf[],
				fCrowdingIndex_Of_All_Orders_Arrf); // float fCrowdingIndex_Of_All_Orders_Arrf[])

			nTempf = (nNumOfIntensity_IntervalsForMultifractalTotf - 1) * nNumOf_Qs_Tot;
			for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)
			{
				iIndexf = iQf + nTempf;

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges': iQf = %d, iIndexf = %d, nTempf = %d", iQf, iIndexf, nTempf);
				fprintf(fout_lr, "\n fGenerDim_Of_All_Orders_Arrf[%d] = %E, fSpectrum_Of_All_Orders_Arrf[%d] = %E, fCrowdingIndex_Of_All_Orders_Arrf[%d] = %E", 
					iQf, fGenerDim_Of_All_Orders_Arrf[iQf], iQf, fSpectrum_Of_All_Orders_Arrf[iQf], iQf, fCrowdingIndex_Of_All_Orders_Arrf[iQf]);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
				if (iIndexf >= nNumOfFeasForMultifractalTot)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n An error in 'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges': iIndexf = %d >= nNumOfFeasForMultifractalTot = %d", iIndexf, nNumOfFeasForMultifractalTot);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
					printf( "\n\n An error in 'GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges': iIndexf = %d >= nNumOfFeasForMultifractalTot = %d", iIndexf, nNumOfFeasForMultifractalTot);
					printf("\n\n Please press any key:"); fflush(fout_lr); getchar();

					delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
					return UNSUCCESSFUL_RETURN;
				}//if (iIndexf >= nNumOfFeasForMultifractalTot)

				fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[iIndexf] = fGenerDim_Of_All_Orders_Arrf[iQf];
				fSpectrumOf_All_Orders_And_All_IntensityRanges_Arrf[iIndexf] = fSpectrum_Of_All_Orders_Arrf[iQf];
				fCrowdingIndex_Of_All_Orders_And_All_IntensityRanges_Arrf[iIndexf] = fCrowdingIndex_Of_All_Orders_Arrf[iQf];

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n iIndexf = %d, fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[%d] = %E", 
					iIndexf, iIndexf,fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[iIndexf]);
				
				fprintf(fout_lr, "\n fSpectrumOf_All_Orders_And_All_IntensityRanges_Arrf[%d] = %E", iIndexf,fSpectrumOf_All_Orders_And_All_IntensityRanges_Arrf[iIndexf]);
				fprintf(fout_lr, "\n fCrowdingIndex_Of_All_Orders_And_All_IntensityRanges_Arrf[%d] = %E", iIndexf,fCrowdingIndex_Of_All_Orders_And_All_IntensityRanges_Arrf[iIndexf]);
				fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
			}//for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)

		} //for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_2f++)

	}//for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_1f++)

	delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
	return SUCCESSFUL_RETURN;
}//int GenerDims_Spectra_CrowdingIndexes_Of_All_Q_orders_At_All_IntensityRanges(...


//////////////////////////////////////////////////////////////
float PowerOfAFloatNumber(
	const int nIntOrFloatf, //1 -- int, 0 -- float
	const int nPowerf,
	const float fPowerf,

	const float fFloatInitf) //fFloatInitf != 0.0
{
	int
		iPowf;

	float
		fPowerCurf = fFloatInitf;
	
	if (nIntOrFloatf == 1)
	{
		if (nPowerf == 0)
			return 1.0;
		else if (nPowerf > 0)
		{
			for (iPowf = 1; iPowf < nPowerf; iPowf++)
			{
				fPowerCurf = fPowerCurf * fFloatInitf;
			}//for (iPowf = 1; iPowf < nPowerf; iPowf++)

		}//else if (nPowerf > 0)
		else if (nPowerf < 0)
		{
			for (iPowf = 1; iPowf < nPowerf; iPowf++)
			{
				fPowerCurf = fPowerCurf * fFloatInitf;
			}//for (iPowf = 1; iPowf < nPowerf; iPowf++)

			fPowerCurf = (float)(1.0) / fPowerCurf;
		} //else if (nPowerf < 0)

	}//if (nIntOrFloatf == 1)
	else if (nIntOrFloatf == 0)
	{
		fPowerCurf = powf(fFloatInitf, fPowerf);

	}//else if (nIntOrFloatf == 0)

	return fPowerCurf;
}//float PowerOfAFloatNumber(
//////////////////////////////////////////////////////////

int OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges(
	const int nNumOfOneFea_Adjustedf,
	const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

	const COLOR_IMAGE *sColor_ImageInitf,

	float &fOneFea_FrGenerDimf)
{

	//float fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[], //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]
	int Initializing_Embedded_Image(
		const int nDim_2pNf,

		EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef);  //[nDim_2pNf*nDim_2pNf]

	int Embedding_Image_Into_2powerN_ForHausdorff(
		const int nDim_2pNf,

		const int nThresholdForIntensitiesMinf,
		const int nThresholdForIntensitiesMaxf,

		const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

		const COLOR_IMAGE *sColor_ImageInitf,
		EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef); //[nDim_2pNf*nDim_2pNf]

	int OneFea_OfGenerDims_Of_One_Q_order_AtIntensityRange(

		const int nQf,
		const int nDim_2pNf,
		const int nScalef,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		float &fOneFea_FrGenerDimf);
		//float fGenerDim_Of_All_Orders_Arrf[], //[nNumOf_Qs_Tot]
		//float fSpectrum_Of_All_Orders_Arrf[],
		//float fCrowdingIndex_Of_All_Orders_Arrf[]);

		int
			nResf,
			nLenOfIntensityIntervalf = (nIntensityStatMax + 1) / nNumOfIntensity_IntervalsForMultifractal, // 8
			iIntensity_Interval_1f,
			iIntensity_Interval_2f,

			nIntensity_Interval_1f,
			nIntensity_Interval_2f,
			nNumOfIntensity_IntervalsForMultifractalTotf,

			nScalef,
			nDim_2pNf,

			nQf,
			//iIndexf,
			nIndexOfIntensitiesCurf = nNumOfOneFea_Adjustedf/ nNumOf_Qs_Tot,

			nIntensitiesFoundf = 0, //not yet

			nProductf = nNumOf_Qs_Tot * nNumOfIntensity_IntervalsForMultifractal,
			nThresholdForIntensitiesMinf,
			nThresholdForIntensitiesMaxf;

//////////////////////////////////////////////////////////////

		if (nNumOfOneFea_Adjustedf >= nNumOfFeasForMultifractalTot)
		{
#ifndef COMMENT_OUT_ALL_PRINTS	
	
			fprintf(fout_lr, "\n\n An error in 'OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges': nNumOfOneFea_Adjustedf = %d >= nNumOfFeasForMultifractalTot = %d",
				nNumOfOneFea_Adjustedf, nNumOfFeasForMultifractalTot);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
			printf("\n\n An error in 'OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges': nNumOfOneFea_Adjustedf = %d >= nNumOfFeasForMultifractalTot = %d",
				nNumOfOneFea_Adjustedf, nNumOfFeasForMultifractalTot);
			printf("\n\n Please press any key:"); fflush(fout_lr); getchar();
			
			//delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
			return UNSUCCESSFUL_RETURN;
		}//if (nNumOfOneFea_Adjustedf >= nNumOfFeasForMultifractalTot)

///////////////////////////////////////////////////////////
//finding 'nIntensity_Interval_1f' and 'nIntensity_Interval_2f'
		nNumOfIntensity_IntervalsForMultifractalTotf = 0;
		for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_1f++)
		{

			//iIntensity_Interval_1_Glob = iIntensity_Interval_1f;
			for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_2f++)
			{
				//iIntensity_Interval_2_Glob = iIntensity_Interval_2f;

				nNumOfIntensity_IntervalsForMultifractalTotf += 1;

				if (nNumOfIntensity_IntervalsForMultifractalTotf - 1 == nIndexOfIntensitiesCurf)
				{
					nIntensitiesFoundf = 1;

					nIntensity_Interval_1f = iIntensity_Interval_1f;
					nIntensity_Interval_2f = iIntensity_Interval_2f;

					nQf = nNumOfOneFea_Adjustedf - (nIndexOfIntensitiesCurf*nNumOf_Qs_Tot);

					if (nQf < 0 || nQf > nNumOf_Qs_Tot - 1)
					{
#ifndef COMMENT_OUT_ALL_PRINTS	
						fprintf(fout_lr, "\n\n An error in 'OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges':  nQf = %d >= nNumOf_Qs_Tot = %d",
							nQf, nNumOf_Qs_Tot);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

						printf("\n\n An error in 'OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges': nQf = %d >= nNumOf_Qs_Tot = %d",
							nQf, nNumOf_Qs_Tot);

						printf("\n\n Please press any key:"); fflush(fout_lr); getchar();
						//delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
						return UNSUCCESSFUL_RETURN;
					}//if (nQf < 0 || nQf > nNumOf_Qs_Tot - 1)

					goto MarkExitOf_2LoopsInOneFea_GenerDim;
				}//if (nNumOfIntensity_IntervalsForMultifractalTotf -1 == nIndexOfIntensitiesCurf)

/*				nTempf = (nNumOfIntensity_IntervalsForMultifractalTotf - 1) * nNumOf_Qs_Tot;
				for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)
				{
					iIndexf = iQf + nTempf;
				} //
*/
			} //for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_2f++)
		} //for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_1f++)

		if (nIntensitiesFoundf == 0)
		{
#ifndef COMMENT_OUT_ALL_PRINTS	
			fprintf(fout_lr, "\n\n An error in 'OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges':  the intensities have not been found");
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
			printf("\n\n An error in 'OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges': the intensities have not been found");

			printf("\n\n Please press any key:"); fflush(fout_lr); getchar();
			//delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
			return UNSUCCESSFUL_RETURN;
		}//if (nIntensitiesFoundf == 0)

	/////////////////////////////////////////////////
	//initialization
		MarkExitOf_2LoopsInOneFea_GenerDim:		nResf = Dim_2powerN(
															sColor_ImageInitf->nLength, //const int nLengthf,
															sColor_ImageInitf->nWidth, //const int nWidthf,

															nScalef, //int &nScalef,

															nDim_2pNf); // int &nDim_2pNf);

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			//delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
			return UNSUCCESSFUL_RETURN;
		}//if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 'OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges': nScalef = %d, nDim_2pNf = %d, nNumOfIntensity_IntervalsForMultifractalTot = %d",
			nScalef, nDim_2pNf, nNumOfIntensity_IntervalsForMultifractalTot);
		fprintf(fout_lr, "\n\n 'OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges':nScalef = %d, nDim_2pNf = %d, nNumOfIntensity_IntervalsForMultifractalTot = %d",
			nScalef, nDim_2pNf, nNumOfIntensity_IntervalsForMultifractalTot);

		fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		if (nScalef <= 1)
		{
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n An error in 'OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges': nScalef = %d", nScalef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			printf("\n\n An error in 'OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges': nScalef = %d", nScalef);
			printf("\n\n Please press any key:"); getchar();
			//delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
			return UNSUCCESSFUL_RETURN;
		}//if (nScalef <= 1)
		//////////////////////////////////////////////
		EMBEDDED_IMAGE_BLACK_WHITE sImageEmbeddedf_BlackWhitef; //) //[nDim_2pNf*nDim_2pNf]

		nResf = Initializing_Embedded_Image(
			nDim_2pNf, //const int nDim_2pNf,

			&sImageEmbeddedf_BlackWhitef); // EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef) // //[nDim_2pNf*nDim_2pNf]

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
			return UNSUCCESSFUL_RETURN;
		}//if (nResf == UNSUCCESSFUL_RETURN)

		///////////////////////////////////////
		nNumOfIntensity_IntervalsForMultifractalTotf = 0;
		//for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_1f++)
		{
			nThresholdForIntensitiesMinf = nIntensity_Interval_1f * nLenOfIntensityIntervalf;

			iIntensity_Interval_1_Glob = nIntensity_Interval_1f;
			//for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_2f++)
			{
				iIntensity_Interval_2_Glob = nIntensity_Interval_2f;

				nNumOfIntensity_IntervalsForMultifractalTotf += 1;

				if (nNumOfIntensity_IntervalsForMultifractalTotf > nNumOfIntensity_IntervalsForMultifractalTot)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
	
					fprintf(fout_lr, "\n\n An error in 'OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges': nNumOfIntensity_IntervalsForMultifractalTotf = %d > nNumOfIntensity_IntervalsForMultifractalTot = %d",
						nNumOfIntensity_IntervalsForMultifractalTotf, nNumOfIntensity_IntervalsForMultifractalTot);

					fprintf(fout_lr, "\n nIntensity_Interval_1f = %d, nIntensity_Interval_2f = %d", nIntensity_Interval_1f, nIntensity_Interval_2f);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
					printf("\n\n An error in 'OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges': nNumOfIntensity_IntervalsForMultifractalTotf = %d > nNumOfIntensity_IntervalsForMultifractalTot = %d",
						nNumOfIntensity_IntervalsForMultifractalTotf, nNumOfIntensity_IntervalsForMultifractalTot);

					printf("\n nIntensity_Interval_1f = %d, nIntensity_Interval_2f = %d", nIntensity_Interval_1f, nIntensity_Interval_2f);
					printf("\n\n Please press any key:"); getchar();

					delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
					return UNSUCCESSFUL_RETURN;
				} //if (nNumOfIntensity_IntervalsForMultifractalTotf > nNumOfIntensity_IntervalsForMultifractalTot)

				nThresholdForIntensitiesMaxf = nIntensity_Interval_2f * nLenOfIntensityIntervalf;

				nResf = Embedding_Image_Into_2powerN_ForHausdorff(
					nDim_2pNf, //const int nDim_2pNf,

					nThresholdForIntensitiesMinf, //const int nThresholdForIntensitiesMinf,
					nThresholdForIntensitiesMaxf, //const int nThresholdForIntensitiesMaxf,

					sWeightsOfColorsf, //const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

					sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,
					&sImageEmbeddedf_BlackWhitef); // EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef) //[nDim_2pNf*nDim_2pNf]

				if (nResf == UNSUCCESSFUL_RETURN)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n An error in 'OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges': nScalef = %d", nScalef);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
					printf("\n\n An error in 'OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges': nScalef = %d", nScalef);
					printf("\n\n Please press any key:"); getchar();

					delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
					return UNSUCCESSFUL_RETURN;
				}//if (nResf == UNSUCCESSFUL_RETURN)

			nResf = OneFea_OfGenerDims_Of_One_Q_order_AtIntensityRange(

					nQf,//const int nQf,
					nDim_2pNf, //const int nDim_2pNf,
					nScalef, //const int nScalef,

					sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

					&sImageEmbeddedf_BlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

					fOneFea_FrGenerDimf); // float &fOneFea_FrGenerDimf);

			} //for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_2f++)

		}//for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_1f++)

		delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
		return SUCCESSFUL_RETURN;
}//int OneFea_GenerDim_Of_All_Orders_And_All_IntensityRanges(
///////////////////////////////////////////////////////////////////////////

int OneFea_Spectrum_Of_All_Orders_And_All_IntensityRanges(
	const int nNumOfOneFea_Adjustedf,
	const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

	const COLOR_IMAGE *sColor_ImageInitf,

	float &fOneFea_FrSpectrumf)
{

	//float fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[], //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]
	int Initializing_Embedded_Image(
		const int nDim_2pNf,

		EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef);  //[nDim_2pNf*nDim_2pNf]

	int Embedding_Image_Into_2powerN_ForHausdorff(
		const int nDim_2pNf,

		const int nThresholdForIntensitiesMinf,
		const int nThresholdForIntensitiesMaxf,

		const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

		const COLOR_IMAGE *sColor_ImageInitf,
		EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef); //[nDim_2pNf*nDim_2pNf]

//	int OneFea_OfGenerDims_Of_One_Q_order_AtIntensityRange(
	int OneFea_OfSpectrum_Of_One_Q_order_AtIntensityRange(

		const int nQf,
		const int nDim_2pNf,
		const int nScalef,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		float &fOneFea_FrSpectrumf);
	//float fGenerDim_Of_All_Orders_Arrf[], //[nNumOf_Qs_Tot]
	//float fSpectrum_Of_All_Orders_Arrf[],
	//float fCrowdingIndex_Of_All_Orders_Arrf[]);

	int
		nResf,
		nLenOfIntensityIntervalf = (nIntensityStatMax + 1) / nNumOfIntensity_IntervalsForMultifractal, // 8
		iIntensity_Interval_1f,
		iIntensity_Interval_2f,

		nIntensity_Interval_1f,
		nIntensity_Interval_2f,
		nNumOfIntensity_IntervalsForMultifractalTotf,

		nScalef,
		nDim_2pNf,

		nQf,
		//iIndexf,
		nIndexOfIntensitiesCurf = nNumOfOneFea_Adjustedf / nNumOf_Qs_Tot,

		nIntensitiesFoundf = 0, //not yet

		nProductf = nNumOf_Qs_Tot * nNumOfIntensity_IntervalsForMultifractal,
		nThresholdForIntensitiesMinf,
		nThresholdForIntensitiesMaxf;

	//EMBEDDED_IMAGE_BLACK_WHITE sImageEmbeddedf_BlackWhitef; //) //[nDim_2pNf*nDim_2pNf]
//////////////////////////////////////////////////////////////
#ifndef COMMENT_OUT_ALL_PRINTS	

	fprintf(fout_lr, "\n\n 'OneFea_Spectrum_Of_All_Orders_And...': nNumOfOneFea_Adjustedf = %d, nNumOfFeasForMultifractalTot = %d, nIndexOfIntensitiesCurf = %d",
		nNumOfOneFea_Adjustedf, nNumOfFeasForMultifractalTot, nIndexOfIntensitiesCurf);
	fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d", nNumOfOneFea_Spectrum_AdjustedGlob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	nNumOfOneFea_Spectrum_AdjustedGlob = nNumOfOneFea_Adjustedf;

	if (nNumOfOneFea_Adjustedf >= nNumOfFeasForMultifractalTot)
	{
#ifndef COMMENT_OUT_ALL_PRINTS	

		fprintf(fout_lr, "\n\n An error in 'OneFea_Spectrum_Of_All_Orders_And...': nNumOfOneFea_Adjustedf = %d >= nNumOfFeasForMultifractalTot = %d",
			nNumOfOneFea_Adjustedf, nNumOfFeasForMultifractalTot);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		printf("\n\n An error in 'OneFea_Spectrum_Of_All_Orders_And...': nNumOfOneFea_Adjustedf = %d >= nNumOfFeasForMultifractalTot = %d",
			nNumOfOneFea_Adjustedf, nNumOfFeasForMultifractalTot);

		printf("\n\n Please press any key:"); fflush(fout_lr); getchar();
		//delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
		return UNSUCCESSFUL_RETURN;
	}//if (nNumOfOneFea_Adjustedf >= nNumOfFeasForMultifractalTot)

///////////////////////////////////////////////////////////
//finding 'nIntensity_Interval_1f' and 'nIntensity_Interval_2f'
	nNumOfIntensity_IntervalsForMultifractalTotf = 0;
	for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_1f++)
	{

		//iIntensity_Interval_1_Glob = iIntensity_Interval_1f;
		for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_2f++)
		{
			//iIntensity_Interval_2_Glob = iIntensity_Interval_2f;

			nNumOfIntensity_IntervalsForMultifractalTotf += 1;

			if (nNumOfIntensity_IntervalsForMultifractalTotf - 1 == nIndexOfIntensitiesCurf)
			{
				nIntensitiesFoundf = 1;

				nIntensity_Interval_1f = iIntensity_Interval_1f;
				nIntensity_Interval_2f = iIntensity_Interval_2f;

				nQf = nNumOfOneFea_Adjustedf - (nIndexOfIntensitiesCurf*nNumOf_Qs_Tot);

#ifndef COMMENT_OUT_ALL_PRINTS	

				fprintf(fout_lr, "\n\n 'OneFea_Spectrum_Of_All_Orders_And...':  nQf = %d, nNumOf_Qs_Tot = %d, nIntensity_Interval_1f = %d, nIntensity_Interval_2f = %d, nNumOfOneFea_Adjustedf = %d",
					nQf, nNumOf_Qs_Tot, nIntensity_Interval_1f, nIntensity_Interval_2f, nNumOfOneFea_Adjustedf);
				fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d", nNumOfOneFea_Spectrum_AdjustedGlob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
				if (nQf < 0 || nQf > nNumOf_Qs_Tot - 1)
				{
#ifndef COMMENT_OUT_ALL_PRINTS	
					printf("\n\n An error in 'OneFea_Spectrum_Of_All_Orders_And...': nQf = %d >= nNumOf_Qs_Tot = %d",
						nQf, nNumOf_Qs_Tot);

					fprintf(fout_lr, "\n\n An error in 'OneFea_Spectrum_Of_All_Orders_And...':  nQf = %d >= nNumOf_Qs_Tot = %d",
						nQf, nNumOf_Qs_Tot);
					printf("\n\n Please press any key:"); fflush(fout_lr); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

					//delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
					return UNSUCCESSFUL_RETURN;
				}//if (nQf < 0 || nQf > nNumOf_Qs_Tot - 1)

				goto MarkExitOf_2LoopsInOneFea_Spectrum;

			}//if (nNumOfIntensity_IntervalsForMultifractalTotf -1 == nIndexOfIntensitiesCurf)

/*				nTempf = (nNumOfIntensity_IntervalsForMultifractalTotf - 1) * nNumOf_Qs_Tot;
				for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)
				{
					iIndexf = iQf + nTempf;
				} //
*/
		} //for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_2f++)
	} //for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_1f++)

	if (nIntensitiesFoundf == 0)
	{
#ifndef COMMENT_OUT_ALL_PRINTS	
		printf("\n\n An error in 'OneFea_Spectrum_Of_All_Orders_And...': the intensities have not been found");

		fprintf(fout_lr, "\n\n An error in 'OneFea_Spectrum_Of_All_Orders_And...':  the intensities have not been found");
		printf("\n\n Please press any key:"); fflush(fout_lr); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	//	delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
		return UNSUCCESSFUL_RETURN;
	}//if (nIntensitiesFoundf == 0)

/////////////////////////////////////////////////
//initialization
MarkExitOf_2LoopsInOneFea_Spectrum:		nResf = Dim_2powerN(
	sColor_ImageInitf->nLength, //const int nLengthf,
	sColor_ImageInitf->nWidth, //const int nWidthf,

	nScalef, //int &nScalef,

	nDim_2pNf); // int &nDim_2pNf);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		//delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'OneFea_Spectrum_Of_All_Orders_And...': nScalef = %d, nDim_2pNf = %d, nNumOfIntensity_IntervalsForMultifractalTot = %d",
		nScalef, nDim_2pNf, nNumOfIntensity_IntervalsForMultifractalTot);
	fprintf(fout_lr, "\n\n 'OneFea_Spectrum_Of_All_Orders_And...': nScalef = %d, nDim_2pNf = %d, nNumOfIntensity_IntervalsForMultifractalTot = %d",
		nScalef, nDim_2pNf, nNumOfIntensity_IntervalsForMultifractalTot);

	fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d", nNumOfOneFea_Spectrum_AdjustedGlob);

	fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	if (nScalef <= 1)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'OneFea_Spectrum_Of_All_Orders_And...': nScalef = %d", nScalef);
		fprintf(fout_lr, "\n\n An error in 'OneFea_Spectrum_Of_All_Orders_And...': nScalef = %d", nScalef);
		printf("\n\n Please press any key:"); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
	///	delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
		return UNSUCCESSFUL_RETURN;
	}//if (nScalef <= 1)
	//////////////////////////////////////////////
	EMBEDDED_IMAGE_BLACK_WHITE sImageEmbeddedf_BlackWhitef; //) //[nDim_2pNf*nDim_2pNf]

	nResf = Initializing_Embedded_Image(
		nDim_2pNf, //const int nDim_2pNf,

		&sImageEmbeddedf_BlackWhitef); // EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef) // //[nDim_2pNf*nDim_2pNf]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		//delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

	///////////////////////////////////////
	nNumOfIntensity_IntervalsForMultifractalTotf = 0;
	//for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_1f++)
	{
		nThresholdForIntensitiesMinf = nIntensity_Interval_1f * nLenOfIntensityIntervalf;

		iIntensity_Interval_1_Glob = nIntensity_Interval_1f;
		//for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_2f++)
		{
			iIntensity_Interval_2_Glob = nIntensity_Interval_2f;

			nNumOfIntensity_IntervalsForMultifractalTotf += 1;

			if (nNumOfIntensity_IntervalsForMultifractalTotf > nNumOfIntensity_IntervalsForMultifractalTot)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'OneFea_Spectrum_Of_All_Orders_And...': nNumOfIntensity_IntervalsForMultifractalTotf = %d > nNumOfIntensity_IntervalsForMultifractalTot = %d",
					nNumOfIntensity_IntervalsForMultifractalTotf, nNumOfIntensity_IntervalsForMultifractalTot);

				printf("\n nIntensity_Interval_1f = %d, nIntensity_Interval_2f = %d", nIntensity_Interval_1f, nIntensity_Interval_2f);

				fprintf(fout_lr, "\n\n An error in 'OneFea_Spectrum_Of_All_Orders_And...': nNumOfIntensity_IntervalsForMultifractalTotf = %d > nNumOfIntensity_IntervalsForMultifractalTot = %d",
					nNumOfIntensity_IntervalsForMultifractalTotf, nNumOfIntensity_IntervalsForMultifractalTot);

				fprintf(fout_lr, "\n nIntensity_Interval_1f = %d, nIntensity_Interval_2f = %d", nIntensity_Interval_1f, nIntensity_Interval_2f);
				printf("\n\n Please press any key:"); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
				return UNSUCCESSFUL_RETURN;
			} //if (nNumOfIntensity_IntervalsForMultifractalTotf > nNumOfIntensity_IntervalsForMultifractalTot)

			nThresholdForIntensitiesMaxf = nIntensity_Interval_2f * nLenOfIntensityIntervalf;

			nResf = Embedding_Image_Into_2powerN_ForHausdorff(
				nDim_2pNf, //const int nDim_2pNf,

				nThresholdForIntensitiesMinf, //const int nThresholdForIntensitiesMinf,
				nThresholdForIntensitiesMaxf, //const int nThresholdForIntensitiesMaxf,

				sWeightsOfColorsf, //const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

				sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,
				&sImageEmbeddedf_BlackWhitef); // EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef) //[nDim_2pNf*nDim_2pNf]

			if (nResf == UNSUCCESSFUL_RETURN)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'OneFea_Spectrum_Of_All_Orders_And...': nScalef = %d", nScalef);

				fprintf(fout_lr, "\n\n An error in 'OneFea_Spectrum_Of_All_Orders_And...': nScalef = %d", nScalef);
				printf("\n\n Please press any key:"); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
				return UNSUCCESSFUL_RETURN;
			}//if (nResf == UNSUCCESSFUL_RETURN)

			nResf = OneFea_OfSpectrum_Of_One_Q_order_AtIntensityRange(

				nQf,//const int nQf,
				nDim_2pNf, //const int nDim_2pNf,
				nScalef, //const int nScalef,

				sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

				&sImageEmbeddedf_BlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

				fOneFea_FrSpectrumf); // float &fOneFea_FrSpectrumf);

		} //for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_2f++)

	}//for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_1f++)

	delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
	return SUCCESSFUL_RETURN;
}//int OneFea_Spectrum_Of_All_Orders_And_All_IntensityRanges(
///////////////////////////////////////////////////////////////////////////

int OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges(
	const int nNumOfOneFea_Adjustedf,
	const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

	const COLOR_IMAGE *sColor_ImageInitf,

	float &fOneFea_FrCrowdingIndexf)
{

	//float fGenerDim_Of_All_Orders_And_All_IntensityRanges_Arrf[], //[nNumOf_Qs_Tot*nNumOfIntensity_IntervalsForMultifractalTot]
	int Initializing_Embedded_Image(
		const int nDim_2pNf,

		EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef);  //[nDim_2pNf*nDim_2pNf]

	int Embedding_Image_Into_2powerN_ForHausdorff(
		const int nDim_2pNf,

		const int nThresholdForIntensitiesMinf,
		const int nThresholdForIntensitiesMaxf,

		const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

		const COLOR_IMAGE *sColor_ImageInitf,
		EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef); //[nDim_2pNf*nDim_2pNf]

//	int OneFea_OfGenerDims_Of_One_Q_order_AtIntensityRange(
//	int OneFea_OfSpectrum_Of_One_Q_order_AtIntensityRange(
		int OneFea_OfCrowdingIndex_Of_One_Q_order_AtIntensityRange(

		const int nQf,
		const int nDim_2pNf,
		const int nScalef,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		float &fOneFea_FrCrowdingIndexf);
	//float fGenerDim_Of_All_Orders_Arrf[], //[nNumOf_Qs_Tot]
	//float fSpectrum_Of_All_Orders_Arrf[],
	//float fCrowdingIndex_Of_All_Orders_Arrf[]);

	int
		nResf,
		nLenOfIntensityIntervalf = (nIntensityStatMax + 1) / nNumOfIntensity_IntervalsForMultifractal, // 8
		iIntensity_Interval_1f,
		iIntensity_Interval_2f,

		nIntensity_Interval_1f,
		nIntensity_Interval_2f,
		nNumOfIntensity_IntervalsForMultifractalTotf,

		nScalef,
		nDim_2pNf,

		nQf,
		//iIndexf,
		nIndexOfIntensitiesCurf = nNumOfOneFea_Adjustedf / nNumOf_Qs_Tot,

		nIntensitiesFoundf = 0, //not yet

		nProductf = nNumOf_Qs_Tot * nNumOfIntensity_IntervalsForMultifractal,
		nThresholdForIntensitiesMinf,
		nThresholdForIntensitiesMaxf;

//////////////////////////////////////////////////////////////

	if (nNumOfOneFea_Adjustedf >= nNumOfFeasForMultifractalTot)
	{
#ifndef COMMENT_OUT_ALL_PRINTS	
		printf("\n\n An error in 'OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges': nNumOfOneFea_Adjustedf = %d >= nNumOfFeasForMultifractalTot = %d",
			nNumOfOneFea_Adjustedf, nNumOfFeasForMultifractalTot);
		fprintf(fout_lr, "\n\n An error in 'OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges': nNumOfOneFea_Adjustedf = %d >= nNumOfFeasForMultifractalTot = %d",
			nNumOfOneFea_Adjustedf, nNumOfFeasForMultifractalTot);
		printf("\n\n Please press any key:"); fflush(fout_lr); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		//delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
		return UNSUCCESSFUL_RETURN;
	}//if (nNumOfOneFea_Adjustedf >= nNumOfFeasForMultifractalTot)

///////////////////////////////////////////////////////////
//finding 'nIntensity_Interval_1f' and 'nIntensity_Interval_2f'
	nNumOfIntensity_IntervalsForMultifractalTotf = 0;
	for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_1f++)
	{

		//iIntensity_Interval_1_Glob = iIntensity_Interval_1f;
		for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_2f++)
		{
			//iIntensity_Interval_2_Glob = iIntensity_Interval_2f;

			nNumOfIntensity_IntervalsForMultifractalTotf += 1;

			if (nNumOfIntensity_IntervalsForMultifractalTotf - 1 == nIndexOfIntensitiesCurf)
			{
				nIntensitiesFoundf = 1;

				nIntensity_Interval_1f = iIntensity_Interval_1f;
				nIntensity_Interval_2f = iIntensity_Interval_2f;

				nQf = nNumOfOneFea_Adjustedf - (nIndexOfIntensitiesCurf*nNumOf_Qs_Tot);

				if (nQf < 0 || nQf > nNumOf_Qs_Tot - 1)
				{
#ifndef COMMENT_OUT_ALL_PRINTS	
					printf("\n\n An error in 'OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges': nQf = %d >= nNumOf_Qs_Tot = %d",
						nQf, nNumOf_Qs_Tot);

					fprintf(fout_lr, "\n\n An error in 'OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges':  nQf = %d >= nNumOf_Qs_Tot = %d",
						nQf, nNumOf_Qs_Tot);
					printf("\n\n Please press any key:"); fflush(fout_lr); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				//	delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
					return UNSUCCESSFUL_RETURN;
				}//if (nQf < 0 || nQf > nNumOf_Qs_Tot - 1)

				goto MarkExitOf_2LoopsInOneFea_CrowdingIndex;
			}//if (nNumOfIntensity_IntervalsForMultifractalTotf -1 == nIndexOfIntensitiesCurf)

		} //for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_2f++)
	} //for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_1f++)

	if (nIntensitiesFoundf == 0)
	{
#ifndef COMMENT_OUT_ALL_PRINTS	
		printf("\n\n An error in 'OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges': the intensities have not been found");

		fprintf(fout_lr, "\n\n An error in 'OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges':  the intensities have not been found");
		printf("\n\n Please press any key:"); fflush(fout_lr); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	//	delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
		return UNSUCCESSFUL_RETURN;
	}//if (nIntensitiesFoundf == 0)

/////////////////////////////////////////////////
//initialization
MarkExitOf_2LoopsInOneFea_CrowdingIndex:		nResf = Dim_2powerN(
													sColor_ImageInitf->nLength, //const int nLengthf,
													sColor_ImageInitf->nWidth, //const int nWidthf,

													nScalef, //int &nScalef,

													nDim_2pNf); // int &nDim_2pNf);

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		//delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges': nScalef = %d, nDim_2pNf = %d, nNumOfIntensity_IntervalsForMultifractalTot = %d",
		nScalef, nDim_2pNf, nNumOfIntensity_IntervalsForMultifractalTot);
	fprintf(fout_lr, "\n\n 'OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges':nScalef = %d, nDim_2pNf = %d, nNumOfIntensity_IntervalsForMultifractalTot = %d",
		nScalef, nDim_2pNf, nNumOfIntensity_IntervalsForMultifractalTot);

	fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

	if (nScalef <= 1)
	{
#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n An error in 'OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges': nScalef = %d", nScalef);
		fprintf(fout_lr, "\n\n An error in 'OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges': nScalef = %d", nScalef);
		printf("\n\n Please press any key:"); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		//delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
		return UNSUCCESSFUL_RETURN;
	}//if (nScalef <= 1)
	//////////////////////////////////////////////
	EMBEDDED_IMAGE_BLACK_WHITE sImageEmbeddedf_BlackWhitef; //) //[nDim_2pNf*nDim_2pNf]

	nResf = Initializing_Embedded_Image(
		nDim_2pNf, //const int nDim_2pNf,

		&sImageEmbeddedf_BlackWhitef); // EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef) // //[nDim_2pNf*nDim_2pNf]

	if (nResf == UNSUCCESSFUL_RETURN)
	{
		delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
		return UNSUCCESSFUL_RETURN;
	}//if (nResf == UNSUCCESSFUL_RETURN)

	///////////////////////////////////////
	nNumOfIntensity_IntervalsForMultifractalTotf = 0;
	//for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_1f++)
	{
		nThresholdForIntensitiesMinf = nIntensity_Interval_1f * nLenOfIntensityIntervalf;

		iIntensity_Interval_1_Glob = nIntensity_Interval_1f;
		//for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_2f++)
		{
			iIntensity_Interval_2_Glob = nIntensity_Interval_2f;

			nNumOfIntensity_IntervalsForMultifractalTotf += 1;

			if (nNumOfIntensity_IntervalsForMultifractalTotf > nNumOfIntensity_IntervalsForMultifractalTot)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges': nNumOfIntensity_IntervalsForMultifractalTotf = %d > nNumOfIntensity_IntervalsForMultifractalTot = %d",
					nNumOfIntensity_IntervalsForMultifractalTotf, nNumOfIntensity_IntervalsForMultifractalTot);

				printf("\n nIntensity_Interval_1f = %d, nIntensity_Interval_2f = %d", nIntensity_Interval_1f, nIntensity_Interval_2f);

				fprintf(fout_lr, "\n\n An error in 'OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges': nNumOfIntensity_IntervalsForMultifractalTotf = %d > nNumOfIntensity_IntervalsForMultifractalTot = %d",
					nNumOfIntensity_IntervalsForMultifractalTotf, nNumOfIntensity_IntervalsForMultifractalTot);

				fprintf(fout_lr, "\n nIntensity_Interval_1f = %d, nIntensity_Interval_2f = %d", nIntensity_Interval_1f, nIntensity_Interval_2f);
				printf("\n\n Please press any key:"); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
				delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
				return UNSUCCESSFUL_RETURN;
			} //if (nNumOfIntensity_IntervalsForMultifractalTotf > nNumOfIntensity_IntervalsForMultifractalTot)

			nThresholdForIntensitiesMaxf = nIntensity_Interval_2f * nLenOfIntensityIntervalf;

			nResf = Embedding_Image_Into_2powerN_ForHausdorff(
				nDim_2pNf, //const int nDim_2pNf,

				nThresholdForIntensitiesMinf, //const int nThresholdForIntensitiesMinf,
				nThresholdForIntensitiesMaxf, //const int nThresholdForIntensitiesMaxf,

				sWeightsOfColorsf, //const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,

				sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,
				&sImageEmbeddedf_BlackWhitef); // EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef) //[nDim_2pNf*nDim_2pNf]

			if (nResf == UNSUCCESSFUL_RETURN)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				printf("\n\n An error in 'OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges': nScalef = %d", nScalef);

				fprintf(fout_lr, "\n\n An error in 'OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges': nScalef = %d", nScalef);
				printf("\n\n Please press any key:"); getchar();

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
				return UNSUCCESSFUL_RETURN;
			}//if (nResf == UNSUCCESSFUL_RETURN)

			nResf = OneFea_OfCrowdingIndex_Of_One_Q_order_AtIntensityRange(

				nQf,//const int nQf,
				nDim_2pNf, //const int nDim_2pNf,
				nScalef, //const int nScalef,

				sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

				&sImageEmbeddedf_BlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

				fOneFea_FrCrowdingIndexf); // float &fOneFea_FrCrowdingIndexf);

		} //for (iIntensity_Interval_2f = iIntensity_Interval_1f + 1; iIntensity_Interval_2f <= nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_2f++)

	}//for (iIntensity_Interval_1f = 0; iIntensity_Interval_1f < nNumOfIntensity_IntervalsForMultifractal; iIntensity_Interval_1f++)

	delete[] sImageEmbeddedf_BlackWhitef.nEmbeddedImage_Arr;
	return SUCCESSFUL_RETURN;
} // OneFea_CrowdingIndex_Of_All_Orders_And_All_IntensityRanges(...
////////////////////////////////////////////////////////////////////

int OneFea_OfGenerDims_Of_One_Q_order_AtIntensityRange(

	const int nQf,
	const int nDim_2pNf,
	const int nScalef,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	float &fOneFea_FrGenerDimf)
{
	int OneFea_GenerDimOf_Q_order(

		const int nDim_2pNf,
		const int nScalef,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		const float fQf,
		const int nIntOrFloatf, //1 -- int, 0 -- float
	///////////////////////////////////////
		float &fGenerDim_Order_Qf);
		//float &fSpectrum_Order_Qf,
		//float &fCrowdingIndex_Order_Qf);
	int
		nResf,

		nWidthOfQ_Stepf, // ( (nQs_Max - nQs_Min)/(nNumOf_Qs_Tot - 1) )

		nIntOrFloatf; //1 -- int, 0 -- float

	float
		fQ_Curf,
		fWidthOfQ_Stepf,
		fGenerDim_Order_Qf;
		//fSpectrum_Order_Qf,
		//fCrowdingIndex_Order_Qf;

	fWidthOfQ_Stepf = (float)(nQs_Max - nQs_Min) / (float)(nNumOf_Qs_Tot - 1);

	nWidthOfQ_Stepf = (int)(fWidthOfQ_Stepf);

	if (fWidthOfQ_Stepf - (float)(nWidthOfQ_Stepf) > feps)
	{
		nIntOrFloatf = 0; //float
	} //if (fWidthOfQ_Stepf - (float)(nWidthOfQ_Stepf) > feps)
	else
	{
		nIntOrFloatf = 1; //int
	}//else

#ifndef COMMENT_OUT_ALL_PRINTS
	printf("\n\n 'OneFea_OfGenerDims_Of_One_Q_order_AtIntensityRange': fWidthOfQ_Stepf = %E, nIntOrFloatf = %d, nNumOf_Qs_Tot = %d, nQs_Min = %d, nQs_Max = %d, nDim_2pNf = %d",
		fWidthOfQ_Stepf, nIntOrFloatf, nNumOf_Qs_Tot, nQs_Min, nQs_Max, nDim_2pNf);
	fprintf(fout_lr, "\n\n 'OneFea_OfGenerDims_Of_One_Q_order_AtIntensityRange': fWidthOfQ_Stepf = %E, nIntOrFloatf = %d, nNumOf_Qs_Tot = %d, nQs_Min = %d, nQs_Max = %d, nDim_2pNf = %d",
		fWidthOfQ_Stepf, nIntOrFloatf, nNumOf_Qs_Tot, nQs_Min, nQs_Max, nDim_2pNf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
	//////////////////
	//for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)
	{
		//fQ_Curf = (float)(nQs_Min)+iQf * fWidthOfQ_Stepf;
		fQ_Curf = (float)(nQs_Min) + (nQf * fWidthOfQ_Stepf);

		nResf = OneFea_GenerDimOf_Q_order(

			nDim_2pNf, //const int nDim_2pNf,
			nScalef, //const int nScalef,

			sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

			sImageEmbeddedf_BlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

			fQ_Curf, //const float fQf,
			nIntOrFloatf, //const int nIntOrFloatf, //1 -- int, 0 -- float
		///////////////////////////////////////
			fGenerDim_Order_Qf); // float &fGenerDim_Order_Qf);
		//	fSpectrum_Order_Qf, //float &fSpectrum_Order_Qf,
		//	fCrowdingIndex_Order_Qf); // float &fCrowdingIndex_Order_Qf);

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			return UNSUCCESSFUL_RETURN;
		}//if (nResf == UNSUCCESSFUL_RETURN)

		//fGenerDim_Of_All_Orders_Arrf[iQf] = fGenerDim_Order_Qf;

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 'OneFea_OfGenerDims_Of_One_Q_order_AtIntensityRange': fGenerDim_Order_Qf = %E, nQf = %d,fQ_Curf = %E", fGenerDim_Order_Qf, nQf, fQ_Curf);
		fprintf(fout_lr, "\n\n 'OneFea_OfGenerDims_Of_One_Q_order_AtIntensityRange': fGenerDim_Order_Qf = %E, nQf = %d,fQ_Curf = %E", fGenerDim_Order_Qf, nQf, fQ_Curf);
		fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
	}//for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)

	fOneFea_FrGenerDimf = fGenerDim_Order_Qf;
	return SUCCESSFUL_RETURN;
} //int OneFea_OfGenerDims_Of_One_Q_order_AtIntensityRange(...
////////////////////////////////////////////////////////////////////

int OneFea_OfSpectrum_Of_One_Q_order_AtIntensityRange(

	const int nQf,
	const int nDim_2pNf,
	const int nScalef,

	const COLOR_IMAGE *sColor_ImageInitf,

	const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

	float &fOneFea_FrSpectrumf)
{
//	int OneFea_GenerDimOf_Q_order(
		int OneFea_SpectrumOf_Q_order(

		const int nDim_2pNf,
		const int nScalef,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		const float fQf,
		const int nIntOrFloatf, //1 -- int, 0 -- float
	///////////////////////////////////////
		float &fSpectrum_Order_Qf);
	//float &fCrowdingIndex_Order_Qf);

	int
		nResf,

		nWidthOfQ_Stepf, // ( (nQs_Max - nQs_Min)/(nNumOf_Qs_Tot - 1) )

		nIntOrFloatf; //1 -- int, 0 -- float

	float
		fQ_Curf,
		fWidthOfQ_Stepf,
		fSpectrum_Order_Qf;

	//fCrowdingIndex_Order_Qf;

	fWidthOfQ_Stepf = (float)(nQs_Max - nQs_Min) / (float)(nNumOf_Qs_Tot - 1);

	nWidthOfQ_Stepf = (int)(fWidthOfQ_Stepf);

	if (fWidthOfQ_Stepf - (float)(nWidthOfQ_Stepf) > feps)
	{
		nIntOrFloatf = 0; //float
	} //if (fWidthOfQ_Stepf - (float)(nWidthOfQ_Stepf) > feps)
	else
	{
		nIntOrFloatf = 1; //int
	}//else

	//////////////////
	//for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)
	{
		//fQ_Curf = (float)(nQs_Min)+iQf * fWidthOfQ_Stepf;
		fQ_Curf = (float)(nQs_Min)+(nQf * fWidthOfQ_Stepf);

#ifndef COMMENT_OUT_ALL_PRINTS

		//nNumOfOneFea_Spectrum_AdjustedGlob
		fprintf(fout_lr, "\n\n 'OneFea_OfSpectrum_Of_One_Q_order_AtIntensityRange': nNumOfOneFea_Spectrum_AdjustedGlob = %d, nQf = %d, nDim_2pNf = %d, nScalef = %d",
			nNumOfOneFea_Spectrum_AdjustedGlob, nQf, nDim_2pNf, nScalef);

		fprintf(fout_lr, "\n fWidthOfQ_Stepf = %E, nIntOrFloatf = %d, nNumOf_Qs_Tot = %d, nQs_Min = %d, nQs_Max = %d, fQ_Curf = %E",
			fWidthOfQ_Stepf, nIntOrFloatf, nNumOf_Qs_Tot, nQs_Min, nQs_Max, fQ_Curf);
		fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d", nNumOfOneFea_Spectrum_AdjustedGlob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		//nResf = OneFea_GenerDimOf_Q_order(
			nResf = OneFea_SpectrumOf_Q_order(

			nDim_2pNf, //const int nDim_2pNf,
			nScalef, //const int nScalef,

			sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

			sImageEmbeddedf_BlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

			fQ_Curf, //const float fQf,
			nIntOrFloatf, //const int nIntOrFloatf, //1 -- int, 0 -- float
		///////////////////////////////////////
				fSpectrum_Order_Qf); // float &fSpectrum_Order_Qf);
		//	fCrowdingIndex_Order_Qf); // float &fCrowdingIndex_Order_Qf);

		if (nResf == UNSUCCESSFUL_RETURN)
		{
			return UNSUCCESSFUL_RETURN;
		}//if (nResf == UNSUCCESSFUL_RETURN)

		//fGenerDim_Of_All_Orders_Arrf[iQf] = fGenerDim_Order_Qf;

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 'OneFea_OfSpectrum_Of_One_Q_order_AtIntensityRange': fSpectrum_Order_Qf = %E, nQf = %d,fQ_Curf = %E", fSpectrum_Order_Qf, nQf, fQ_Curf);
		fprintf(fout_lr, "\n\n 'OneFea_OfSpectrum_Of_One_Q_order_AtIntensityRange':fSpectrum_Order_Qf = %E, nQf = %d,fQ_Curf = %E", fSpectrum_Order_Qf, nQf, fQ_Curf);
		fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d", nNumOfOneFea_Spectrum_AdjustedGlob);

		fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
	}//for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)

	fOneFea_FrSpectrumf = fSpectrum_Order_Qf;
	return SUCCESSFUL_RETURN;
} //int OneFea_OfSpectrum_Of_One_Q_order_AtIntensityRange(...
///////////////////////////////////////////////////////////////

//OneFea_OfCrowdingIndex_Of_One_Q_order_AtIntensityRange
	int OneFea_OfCrowdingIndex_Of_One_Q_order_AtIntensityRange(

		const int nQf,
		const int nDim_2pNf,
		const int nScalef,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		float &fOneFea_FrCrowdingIndexf)
	{
		//	int OneFea_GenerDimOf_Q_order(
		int OneFea_CrowdingIndexOf_Q_order(

			const int nDim_2pNf,
			const int nScalef,

			const COLOR_IMAGE *sColor_ImageInitf,

			const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

			const float fQf,
			const int nIntOrFloatf, //1 -- int, 0 -- float
		///////////////////////////////////////
			float &fCrowdingIndex_Order_Qf);

		int
			nResf,

			nWidthOfQ_Stepf, // ( (nQs_Max - nQs_Min)/(nNumOf_Qs_Tot - 1) )

			nIntOrFloatf; //1 -- int, 0 -- float

		float
			fQ_Curf,
			fWidthOfQ_Stepf,
			fCrowdingIndex_Order_Qf;

		fWidthOfQ_Stepf = (float)(nQs_Max - nQs_Min) / (float)(nNumOf_Qs_Tot - 1);

		nWidthOfQ_Stepf = (int)(fWidthOfQ_Stepf);

		if (fWidthOfQ_Stepf - (float)(nWidthOfQ_Stepf) > feps)
		{
			nIntOrFloatf = 0; //float
		} //if (fWidthOfQ_Stepf - (float)(nWidthOfQ_Stepf) > feps)
		else
		{
			nIntOrFloatf = 1; //int
		}//else

#ifndef COMMENT_OUT_ALL_PRINTS
		printf("\n\n 'OneFea_OfCrowdingIndex_Of_One_Q_order_AtIntensityRange': fWidthOfQ_Stepf = %E, nIntOrFloatf = %d, nNumOf_Qs_Tot = %d, nQs_Min = %d, nQs_Max = %d, nDim_2pNf = %d",
			fWidthOfQ_Stepf, nIntOrFloatf, nNumOf_Qs_Tot, nQs_Min, nQs_Max, nDim_2pNf);
		fprintf(fout_lr, "\n\n 'OneFea_OfCrowdingIndex_Of_One_Q_order_AtIntensityRange': fWidthOfQ_Stepf = %E, nIntOrFloatf = %d, nNumOf_Qs_Tot = %d, nQs_Min = %d, nQs_Max = %d, nDim_2pNf = %d",
			fWidthOfQ_Stepf, nIntOrFloatf, nNumOf_Qs_Tot, nQs_Min, nQs_Max, nDim_2pNf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		//////////////////
		//for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)
		{
			//fQ_Curf = (float)(nQs_Min)+iQf * fWidthOfQ_Stepf;
			fQ_Curf = (float)(nQs_Min)+(nQf * fWidthOfQ_Stepf);

			//nResf = OneFea_GenerDimOf_Q_order(
			nResf = OneFea_CrowdingIndexOf_Q_order(

				nDim_2pNf, //const int nDim_2pNf,
				nScalef, //const int nScalef,

				sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

				sImageEmbeddedf_BlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

				fQ_Curf, //const float fQf,
				nIntOrFloatf, //const int nIntOrFloatf, //1 -- int, 0 -- float
			///////////////////////////////////////
				fCrowdingIndex_Order_Qf); // float &fCrowdingIndex_Order_Qf);

			if (nResf == UNSUCCESSFUL_RETURN)
			{
				return UNSUCCESSFUL_RETURN;
			}//if (nResf == UNSUCCESSFUL_RETURN)

			//fGenerDim_Of_All_Orders_Arrf[iQf] = fGenerDim_Order_Qf;

#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n 'OneFea_OfCrowdingIndex_Of_One_Q_order_AtIntensityRange': fCrowdingIndex_Order_Qf = %E, nQf = %d,fQ_Curf = %E", fCrowdingIndex_Order_Qf, nQf, fQ_Curf);
			fprintf(fout_lr, "\n\n 'OneFea_OfCrowdingIndex_Of_One_Q_order_AtIntensityRange':fCrowdingIndex_Order_Qf = %E, nQf = %d,fQ_Curf = %E", fCrowdingIndex_Order_Qf, nQf, fQ_Curf);
			fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		}//for (iQf = 0; iQf < nNumOf_Qs_Tot; iQf++)

		fOneFea_FrCrowdingIndexf = fCrowdingIndex_Order_Qf;
		return SUCCESSFUL_RETURN;
	} //int OneFea_OfCrowdingIndex_Of_One_Q_order_AtIntensityRange(...
///////////////////////////////////////////////////////

	int OneFea_GenerDimOf_Q_order(

		const int nDim_2pNf,
		const int nScalef,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		const float fQf,
		const int nIntOrFloatf, //1 -- int, 0 -- float
	///////////////////////////////////////
		float &fGenerDim_Order_Qf)
		//float &fSpectrum_Order_Qf,
		//float &fCrowdingIndex_Order_Qf)
	{
		int MomentOf_Q_order_AtFixedLenOfSquareMultiFrac(

			const int nDim_2pNf,

			const int nLenOfSquaref,

			const COLOR_IMAGE *sColor_ImageInitf,

			const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

			const float fQf,
			const int nIntOrFloatf, //1 -- int, 0 -- float
		///////////////////////////////////////
			int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

			float &fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf,
			float &fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf,
			float &fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);
/*
//an option to make it more specific
		int OneFea_GenerDim_MomentOf_Q_order_AtFixedLenOfSquareMultiFrac(

			const int nDim_2pNf,

			const int nLenOfSquaref,

			const COLOR_IMAGE *sColor_ImageInitf,

			const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

			const float fQf,
			const int nIntOrFloatf, //1 -- int, 0 -- float
		///////////////////////////////////////
			int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

			float &fSumOfMomentOf_Q_OrderAtFixedLenf);
*/
		void SlopeOfAStraightLine(
			const int nDimf,

			const float fX_Arrf[],
			const float fY_Arrf[],

			float &fSlopef);

		int
			nNumOfNonZero_ObjectSquaresTotf,

			nResf,
			iScalef,
			//nScalef = 1,

			iIterf,
			nNumOfLogPointsf,

			nIsfQ_0f = 0, // 1 if it is 
			nIsfQ_1f = 0, // 1 if it is 

			nLenOfSquaref;

		float
			fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf,
			fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf,
			fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf,
			fSumOfMomentsf,

			fDiff,

			fNegLogOfLenOfSquare_Arrf[nNumOfIters_ForDim_2powerN_Max],

			fLogPoints_GenerDim_Arrf[nNumOfIters_ForDim_2powerN_Max],

			//fLogPoints_Spectrum_Arrf[nNumOfIters_ForDim_2powerN_Max],
			//fLogPoints_CrowdingIndex_Arrf[nNumOfIters_ForDim_2powerN_Max],

			fMoments_Arrf[nNumOfIters_ForDim_2powerN_Max],
			fMomentsNormalized_Arrf[nNumOfIters_ForDim_2powerN_Max],

			fLogPointf;

		///////////////////////////////////////

		for (iIterf = 0; iIterf < nNumOfIters_ForDim_2powerN_Max; iIterf++)
		{
			fNegLogOfLenOfSquare_Arrf[iIterf] = -1.0;
			fLogPoints_GenerDim_Arrf[iIterf] = -1.0;

			//fLogPoints_Spectrum_Arrf[iIterf] = -1.0;
			//fLogPoints_CrowdingIndex_Arrf[iIterf] = -1.0;

			fMoments_Arrf[iIterf] = 0.0;
			fMomentsNormalized_Arrf[iIterf] = 0.0;

		} //for (iIterf = 0; iIterf < nNumOfIters_ForDim_2powerN_Max; iIterf++)
	//////////////////////////////////
		if (fQf < feps && fQf > -feps)
		{
			nIsfQ_0f = 1;
			goto Mark_OneFea_GenerDimOf_Q_order_1;
		} // if (fQf < feps && fQf > -feps)
	///////////////////////
		fDiff = (float)(1.0) - fQf;
		if (fDiff < feps && fDiff > -feps)
		{
			nIsfQ_1f = 1; //information dimension
		} // if (fDiff < feps && fDiff > -feps)

		///////////////////////////////////////////
	Mark_OneFea_GenerDimOf_Q_order_1:		nNumOfLogPointsf = 0; // nScalef;
		nLenOfSquaref = nDim_2pNf;

		fSumOfMomentsf = 0.0;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'OneFea_GenerDimOf_Q_order': iIntensity_Interval_1_Glob = %d, iIntensity_Interval_2_Glob = %d, fQf = %E, nScalef = %d",
			iIntensity_Interval_1_Glob, iIntensity_Interval_2_Glob, fQf, nScalef);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		for (iScalef = 0; iScalef < nScalef; iScalef++)
		{
			nLenOfSquaref = nLenOfSquaref / 2;

			if (nLenOfSquaref < 2)
			{
				break;
			}//if (nLenOfSquaref < 2)

			nResf = MomentOf_Q_order_AtFixedLenOfSquareMultiFrac(

				nDim_2pNf, //const int nDim_2pNf,

				nLenOfSquaref, //const int nLenOfSquaref,

				sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

				sImageEmbeddedf_BlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

				fQf, //const float fQf,
				nIntOrFloatf, //const int nIntOrFloatf, //1 -- int, 0 -- float
			///////////////////////////////////////
				nNumOfNonZero_ObjectSquaresTotf, //int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

				fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf,	// float &fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf,
				fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf, //float &fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf,
				fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf); // float &fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);

			fMoments_Arrf[iScalef] = fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'OneFea_GenerDimOf_Q_order': nNumOfNonZero_ObjectSquaresTotf = %d, fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf = %E, fQf = %E, nNumOfLogPointsf = %d",
				nNumOfNonZero_ObjectSquaresTotf, fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf, fQf, nNumOfLogPointsf);

			fprintf(fout_lr, "\n iScalef = %d, nLenOfSquaref = %d", iScalef, nLenOfSquaref);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			if (nResf == UNSUCCESSFUL_RETURN)
			{
				return UNSUCCESSFUL_RETURN;
			}//if (nResf == UNSUCCESSFUL_RETURN)

			if (nResf == -2)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'OneFea_GenerDimOf_Q_order': continue for iScalef = %d, nLenOfSquaref = %d; not enough points", iScalef, nLenOfSquaref);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				continue;
			}//if (nResf == -2)

			fSumOfMomentsf += fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf;

			if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
			{
				if (fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf > 0.0) //> feps?
				{
					fLogPointf = log(fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf);
				} //
				else
				{
					fLogPointf = 0.0;
				}//else
			} //if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
			else if (nIsfQ_0f == 1)
			{
				if (nNumOfNonZero_ObjectSquaresTotf <= 0)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					printf("\n\n An error in 'OneFea_GenerDimOf_Q_order': nNumOfNonZero_ObjectSquaresTotf = %d <= 0", nNumOfNonZero_ObjectSquaresTotf);
					fprintf(fout_lr, "\n\n An error in 'OneFea_GenerDimOf_Q_order': nNumOfNonZero_ObjectSquaresTotf = %d <= 0", nNumOfNonZero_ObjectSquaresTotf);

					printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

					return UNSUCCESSFUL_RETURN;
				}// if (nNumOfNonZero_ObjectSquaresTotf <= 0)

				fLogPointf = log((float)(nNumOfNonZero_ObjectSquaresTotf));
			} //else if (nIsfQ_0f == 1)
			else if (nIsfQ_1f == 1)
			{
				if (fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf > 0.0) //> feps?
				{
					fLogPointf = fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf * log(fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf);
				} //
				else
				{
					fLogPointf = 0.0;
				}//else
			} //else if (nIsfQ_1f == 1)

			fNegLogOfLenOfSquare_Arrf[nNumOfLogPointsf] = -log((float)(nLenOfSquaref));

			fLogPoints_GenerDim_Arrf[nNumOfLogPointsf] = fLogPointf;

			//fLogPoints_Spectrum_Arrf[nNumOfLogPointsf] = fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf;
			//fLogPoints_CrowdingIndex_Arrf[nNumOfLogPointsf] = fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf;

#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n 'OneFea_GenerDimOf_Q_order': iScalef = %d, nLenOfSquaref = %d, fLogPoints_GenerDim_Arrf[%d] = %E, fNegLogOfLenOfSquare_Arrf[%d] = %E",
				iScalef, nLenOfSquaref, nNumOfLogPointsf, fLogPoints_GenerDim_Arrf[nNumOfLogPointsf], nNumOfLogPointsf, fNegLogOfLenOfSquare_Arrf[nNumOfLogPointsf]);

			fprintf(fout_lr, "\n\n 'OneFea_GenerDimOf_Q_order': iScalef = %d, nLenOfSquaref = %d, fLogPoints_GenerDim_Arrf[%d] = %E, fNegLogOfLenOfSquare_Arrf[%d] = %E",
				iScalef, nLenOfSquaref, nNumOfLogPointsf, fLogPoints_GenerDim_Arrf[nNumOfLogPointsf], nNumOfLogPointsf, fNegLogOfLenOfSquare_Arrf[nNumOfLogPointsf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			nNumOfLogPointsf += 1;
		}//for (iScalef = 0; iScalef < nScalef; iScalef++)
	/////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'OneFea_GenerDimOf_Q_order': nNumOfLogPointsf = %d, iIntensity_Interval_1_Glob = %d, iIntensity_Interval_2_Glob = %d, fQf = %E",
			nNumOfLogPointsf, iIntensity_Interval_1_Glob, iIntensity_Interval_2_Glob, fQf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		////////////////////////////////////////////////////////////////////////////////
		if (nNumOfLogPointsf <= 1)
		{
			fGenerDim_Order_Qf = -1.0;

			//fSpectrum_Order_Qf = -1.0;
			//fCrowdingIndex_Order_Qf = -1.0;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n The end of 'OneFea_GenerDimOf_Q_order': nNumOfLogPointsf = %d is insufficent, fGenerDim_Order_Qf = %E", nNumOfLogPointsf, fGenerDim_Order_Qf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		}//if (nNumOfLogPointsf <= 1)
		else
		{
			SlopeOfAStraightLine(
				nNumOfLogPointsf, //const int nDimf,

				fNegLogOfLenOfSquare_Arrf, //const float fX_Arrf[],
				fLogPoints_GenerDim_Arrf, //const float fY_Arrf[],

				fGenerDim_Order_Qf); // float &fSlopef);

			fDiff = (float)(1.0) - fQf; //above

			if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
			{
				fGenerDim_Order_Qf = fGenerDim_Order_Qf / fDiff;
			} // if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
	//////////////
/*
			SlopeOfAStraightLine(
				nNumOfLogPointsf, //const int nDimf,

				fNegLogOfLenOfSquare_Arrf, //const float fX_Arrf[],
				fLogPoints_Spectrum_Arrf, //const float fY_Arrf[],

				fSpectrum_Order_Qf); // float &fSlopef);
	///////////////////////////
			SlopeOfAStraightLine(
				nNumOfLogPointsf, //const int nDimf,

				fNegLogOfLenOfSquare_Arrf, //const float fX_Arrf[],
				fLogPoints_CrowdingIndex_Arrf, //const float fY_Arrf[],

				fCrowdingIndex_Order_Qf); // float &fSlopef);
*/
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n The end of 'OneFea_GenerDimOf_Q_order': nNumOfLogPointsf = %d, fGenerDim_Order_Qf = %E, fQf = %E, fDiff = %E",
				nNumOfLogPointsf, fGenerDim_Order_Qf, fQf, fDiff);

		//	fprintf(fout_lr, "\n fGenerDim_Order_Qf = %E, fSpectrum_Order_Qf = %E, fCrowdingIndex_Order_Qf = %E", fGenerDim_Order_Qf, fSpectrum_Order_Qf, fCrowdingIndex_Order_Qf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		} // else
	////////////////////////////////////////////////
		return SUCCESSFUL_RETURN;
	}//int OneFea_GenerDimOf_Q_order(...
//////////////////////////////////////////////////////////////////////////

	int OneFea_SpectrumOf_Q_order(

		const int nDim_2pNf,
		const int nScalef,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		const float fQf,
		const int nIntOrFloatf, //1 -- int, 0 -- float
	///////////////////////////////////////
		float &fSpectrum_Order_Qf)
		
		//float &fCrowdingIndex_Order_Qf)
	{

		int MomentOf_Q_order_AtFixedLenOfSquareMultiFrac(

			const int nDim_2pNf,

			const int nLenOfSquaref,

			const COLOR_IMAGE *sColor_ImageInitf,

			const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

			const float fQf,
			const int nIntOrFloatf, //1 -- int, 0 -- float
		///////////////////////////////////////
			int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

			float &fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf,
			float &fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf,
			float &fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);
		/*
		//an option to make it more specific
				int OneFea_GenerDim_MomentOf_Q_order_AtFixedLenOfSquareMultiFrac(

					const int nDim_2pNf,

					const int nLenOfSquaref,

					const COLOR_IMAGE *sColor_ImageInitf,

					const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

					const float fQf,
					const int nIntOrFloatf, //1 -- int, 0 -- float
				///////////////////////////////////////
					int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

					float &fSumOfMomentOf_Q_OrderAtFixedLenf);
		*/
		void SlopeOfAStraightLine(
			const int nDimf,

			const float fX_Arrf[],
			const float fY_Arrf[],

			float &fSlopef);

		int
			nNumOfNonZero_ObjectSquaresTotf,

			nResf,
			iScalef,
			//nScalef = 1,

			iIterf,
			nNumOfLogPointsf,

			nIsfQ_0f = 0, // 1 if it is 
			nIsfQ_1f = 0, // 1 if it is 

			nLenOfSquaref;

		float
			//fSumOfMomentOf_Q_OrderAtFixedLenf,
			fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf,
			fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf,
			fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf,
			fSumOfMomentsf,

			fDiff,

			fNegLogOfLenOfSquare_Arrf[nNumOfIters_ForDim_2powerN_Max],

			//fLogPoints_GenerDim_Arrf[nNumOfIters_ForDim_2powerN_Max],

			fLogPoints_Spectrum_Arrf[nNumOfIters_ForDim_2powerN_Max],
			//fLogPoints_CrowdingIndex_Arrf[nNumOfIters_ForDim_2powerN_Max],

			fMoments_Arrf[nNumOfIters_ForDim_2powerN_Max],
			fMomentsNormalized_Arrf[nNumOfIters_ForDim_2powerN_Max],

			fLogPointf;

		///////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'OneFea_SpectrumOf_Q_order': nNumOfOneFea_Spectrum_AdjustedGlob = %d,  nDim_2pNf = %d, nScalef = %d, fQf = %E, nIntOrFloatf = %d",
			nNumOfOneFea_Spectrum_AdjustedGlob, nDim_2pNf, nScalef, fQf, nIntOrFloatf);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		for (iIterf = 0; iIterf < nNumOfIters_ForDim_2powerN_Max; iIterf++)
		{
			fNegLogOfLenOfSquare_Arrf[iIterf] = -1.0;
			//fLogPoints_GenerDim_Arrf[iIterf] = -1.0;

			fLogPoints_Spectrum_Arrf[iIterf] = -1.0;
			//fLogPoints_CrowdingIndex_Arrf[iIterf] = -1.0;

			fMoments_Arrf[iIterf] = 0.0;
			fMomentsNormalized_Arrf[iIterf] = 0.0;

		} //for (iIterf = 0; iIterf < nNumOfIters_ForDim_2powerN_Max; iIterf++)
	//////////////////////////////////
		if (fQf < feps && fQf > -feps)
		{
			nIsfQ_0f = 1;
			goto Mark_SpectrumDimOf_Q_order_1;
		} // if (fQf < feps && fQf > -feps)
	///////////////////////
		fDiff = (float)(1.0) - fQf;
		if (fDiff < feps && fDiff > -feps)
		{
			nIsfQ_1f = 1; //information dimension
		} // if (fDiff < feps && fDiff > -feps)

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'OneFea_SpectrumOf_Q_order': fDiff = %E, nIsfQ_0f = %d, nIsfQ_1f = %d",fDiff, nIsfQ_0f, nIsfQ_1f);
		fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		///////////////////////////////////////////
	Mark_SpectrumDimOf_Q_order_1:		nNumOfLogPointsf = 0; // nScalef;
		nLenOfSquaref = nDim_2pNf;

		fSumOfMomentsf = 0.0;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'OneFea_SpectrumOf_Q_order': nNumOfOneFea_Spectrum_AdjustedGlob = %d, iIntensity_Interval_1_Glob = %d, iIntensity_Interval_2_Glob = %d, nIsfQ_0f = %d, nIsfQ_1f = %d",
			nNumOfOneFea_Spectrum_AdjustedGlob,iIntensity_Interval_1_Glob, iIntensity_Interval_2_Glob, nIsfQ_0f, nIsfQ_1f);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		for (iScalef = 0; iScalef < nScalef; iScalef++)
		{
			nLenOfSquaref = nLenOfSquaref / 2;

			if (nLenOfSquaref < 2)
			{
				break;
			}//if (nLenOfSquaref < 2)

			nResf = MomentOf_Q_order_AtFixedLenOfSquareMultiFrac(

				nDim_2pNf, //const int nDim_2pNf,

				nLenOfSquaref, //const int nLenOfSquaref,

				sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

				sImageEmbeddedf_BlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

				fQf, //const float fQf,
				nIntOrFloatf, //const int nIntOrFloatf, //1 -- int, 0 -- float
			///////////////////////////////////////
				nNumOfNonZero_ObjectSquaresTotf, //int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

				fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf,	// float &fSumOfMomentOf_Q_OrderAtFixedLenf);
				fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf, //float &fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf,
				fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf); // float &fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);

			fMoments_Arrf[iScalef] = fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf;
			fSumOfMomentsf += fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'OneFea_SpectrumOf_Q_order': nNumOfNonZero_ObjectSquaresTotf = %d, fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf = %E, fQf = %E, nNumOfLogPointsf = %d",
				nNumOfNonZero_ObjectSquaresTotf, fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf, fQf, nNumOfLogPointsf);
			
			fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

			fprintf(fout_lr, "\n iScalef = %d, nScalef = %d, nLenOfSquaref = %d, fMoments_Arrf[%d] = %E", iScalef, nScalef,nLenOfSquaref, iScalef,fMoments_Arrf[iScalef]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			if (nResf == UNSUCCESSFUL_RETURN)
			{
				return UNSUCCESSFUL_RETURN;
			}//if (nResf == UNSUCCESSFUL_RETURN)

			if (nResf == -2)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'OneFea_SpectrumOf_Q_order': continue for iScalef = %d, nLenOfSquaref = %d; not enough points", iScalef, nLenOfSquaref);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				continue;
			}//if (nResf == -2)

			if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
			{
				if (fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf > 0.0) //> feps?
				{
					fLogPointf = log(fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n nIsfQ_0f == 0 && nIsfQ_1f == 0, iScalef = %d, nLenOfSquaref = %d, fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf = %E > 0.0, fLogPointf = %E", 
						iScalef, nLenOfSquaref, fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf,fLogPointf);
					fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
				} //
				else
				{
					fLogPointf = 0.0;
#ifndef COMMENT_OUT_ALL_PRINTS
						fprintf(fout_lr, "\n\n nIsfQ_0f == 0 && nIsfQ_1f == 0, fLogPointf = 0.0, iScalef = %d, nLenOfSquaref = %d, fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf = %E <= 0.0",
							iScalef, nLenOfSquaref, fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf);
						fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
				}//else
			} //if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
			else if (nIsfQ_0f == 1)
			{
				if (nNumOfNonZero_ObjectSquaresTotf <= 0)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					printf("\n\n An error in 'OneFea_SpectrumOf_Q_order': nNumOfNonZero_ObjectSquaresTotf = %d <= 0", nNumOfNonZero_ObjectSquaresTotf);
					fprintf(fout_lr, "\n\n An error in 'OneFea_SpectrumOf_Q_order': nNumOfNonZero_ObjectSquaresTotf = %d <= 0", nNumOfNonZero_ObjectSquaresTotf);
					fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

					printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

					return UNSUCCESSFUL_RETURN;
				}// if (nNumOfNonZero_ObjectSquaresTotf <= 0)

				fLogPointf = log((float)(nNumOfNonZero_ObjectSquaresTotf));

#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n nIsfQ_0f == 1, iScalef = %d, nLenOfSquaref = %d, fLogPointf = %E, nNumOfNonZero_ObjectSquaresTotf = %d",
					iScalef, nLenOfSquaref, fLogPointf, nNumOfNonZero_ObjectSquaresTotf);
				fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			} //else if (nIsfQ_0f == 1)
			else if (nIsfQ_1f == 1)
			{
				if (fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf > 0.0) //> feps?
				{
					fLogPointf = fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf * log(fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf);

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n nIsfQ_1f == 1, iScalef = %d, nLenOfSquaref = %d, fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf = %E > 0.0, fLogPointf = %E",
						iScalef, nLenOfSquaref, fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf, fLogPointf);
					fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
				} //
				else
				{
					fLogPointf = 0.0;

#ifndef COMMENT_OUT_ALL_PRINTS
					fprintf(fout_lr, "\n\n nIsfQ_1f == 1, fLogPointf = 0.0, iScalef = %d, nLenOfSquaref = %d, fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf = %E <= 0.0",
						iScalef, nLenOfSquaref, fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf);
					fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
				}//else
			} //else if (nIsfQ_1f == 1)

			fNegLogOfLenOfSquare_Arrf[nNumOfLogPointsf] = -log((float)(nLenOfSquaref));

			//fLogPoints_GenerDim_Arrf[nNumOfLogPointsf] = fLogPointf;

			fLogPoints_Spectrum_Arrf[nNumOfLogPointsf] = fLogPointf; // fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf;
			//fLogPoints_CrowdingIndex_Arrf[nNumOfLogPointsf] = fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf;

#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n 'OneFea_SpectrumOf_Q_order': iScalef = %d, nLenOfSquaref = %d, fLogPoints_Spectrum_Arrf[%d] = %E, fNegLogOfLenOfSquare_Arrf[%d] = %E",
				iScalef, nLenOfSquaref, nNumOfLogPointsf, fLogPoints_Spectrum_Arrf[nNumOfLogPointsf], nNumOfLogPointsf, fNegLogOfLenOfSquare_Arrf[nNumOfLogPointsf]);

			fprintf(fout_lr, "\n\n 'OneFea_SpectrumOf_Q_order': iScalef = %d, nLenOfSquaref = %d, fLogPoints_Spectrum_Arrf[%d] = %E, fNegLogOfLenOfSquare_Arrf[%d] = %E, nNumOfLogPointsf = %d",
				iScalef, nLenOfSquaref, nNumOfLogPointsf, fLogPoints_Spectrum_Arrf[nNumOfLogPointsf], nNumOfLogPointsf, fNegLogOfLenOfSquare_Arrf[nNumOfLogPointsf], nNumOfLogPointsf);
			fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			nNumOfLogPointsf += 1;
		}//for (iScalef = 0; iScalef < nScalef; iScalef++)
	/////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'OneFea_SpectrumOf_Q_order': nNumOfLogPointsf = %d, iIntensity_Interval_1_Glob = %d, iIntensity_Interval_2_Glob = %d, fQf = %E",
			nNumOfLogPointsf, iIntensity_Interval_1_Glob, iIntensity_Interval_2_Glob, fQf);
		fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		////////////////////////////////////////////////////////////////////////////////
		if (nNumOfLogPointsf <= 1)
		{
			//fGenerDim_Order_Qf = -1.0;

			fSpectrum_Order_Qf = -1.0;
			//fCrowdingIndex_Order_Qf = -1.0;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n The end of 'OneFea_SpectrumOf_Q_order': nNumOfLogPointsf = %d is insufficient, fSpectrum_Order_Qf = %E", nNumOfLogPointsf, fSpectrum_Order_Qf);
			fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		}//if (nNumOfLogPointsf <= 1)
		else
		{
/*
			SlopeOfAStraightLine(
				nNumOfLogPointsf, //const int nDimf,

				fNegLogOfLenOfSquare_Arrf, //const float fX_Arrf[],
				fLogPoints_GenerDim_Arrf, //const float fY_Arrf[],

				fGenerDim_Order_Qf); // float &fSlopef);

			fDiff = 1.0 - fQf; //above

			if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
			{
				fGenerDim_Order_Qf = fGenerDim_Order_Qf / fDiff;
			} // if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
*/
	//////////////

			SlopeOfAStraightLine(
				nNumOfLogPointsf, //const int nDimf,

				fNegLogOfLenOfSquare_Arrf, //const float fX_Arrf[],
				fLogPoints_Spectrum_Arrf, //const float fY_Arrf[],

				fSpectrum_Order_Qf); // float &fSlopef);
/*
	///////////////////////////
			SlopeOfAStraightLine(
				nNumOfLogPointsf, //const int nDimf,

				fNegLogOfLenOfSquare_Arrf, //const float fX_Arrf[],
				fLogPoints_CrowdingIndex_Arrf, //const float fY_Arrf[],

				fCrowdingIndex_Order_Qf); // float &fSlopef);
*/
#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n The end of 'OneFea_SpectrumOf_Q_order': nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfLogPointsf = %d, fSpectrum_Order_Qf = %E, fQf = %E",
				nNumOfOneFea_Spectrum_AdjustedGlob,nNumOfLogPointsf, fSpectrum_Order_Qf, fQf);
			fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		} // else
	////////////////////////////////////////////////
		return SUCCESSFUL_RETURN;
	}//int OneFea_SpectrumOf_Q_order(...
//////////////////////////////////////////////////////////////////////

	int OneFea_CrowdingIndexOf_Q_order(

		const int nDim_2pNf,
		const int nScalef,

		const COLOR_IMAGE *sColor_ImageInitf,

		const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

		const float fQf,
		const int nIntOrFloatf, //1 -- int, 0 -- float
	///////////////////////////////////////
		float &fCrowdingIndex_Order_Qf)
	{

		int MomentOf_Q_order_AtFixedLenOfSquareMultiFrac(

			const int nDim_2pNf,

			const int nLenOfSquaref,

			const COLOR_IMAGE *sColor_ImageInitf,

			const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

			const float fQf,
			const int nIntOrFloatf, //1 -- int, 0 -- float
		///////////////////////////////////////
			int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

			float &fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf,
			float &fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf,
			float &fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);
		/*
		//an option to make it more specific
				int OneFea_CrowdingIndex_MomentOf_Q_order_AtFixedLenOfSquareMultiFrac(

					const int nDim_2pNf,

					const int nLenOfSquaref,

					const COLOR_IMAGE *sColor_ImageInitf,

					const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

					const float fQf,
					const int nIntOrFloatf, //1 -- int, 0 -- float
				///////////////////////////////////////
					int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

					float &fSumOfMomentOf_Q_OrderAtFixedLenf);
		*/
		void SlopeOfAStraightLine(
			const int nDimf,

			const float fX_Arrf[],
			const float fY_Arrf[],

			float &fSlopef);

		int
			nNumOfNonZero_ObjectSquaresTotf,

			nResf,
			iScalef,
			//nScalef = 1,

			iIterf,
			nNumOfLogPointsf,

			nIsfQ_0f = 0, // 1 if it is 
			nIsfQ_1f = 0, // 1 if it is 

			nLenOfSquaref;

		float
			fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf,
			fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf,
			fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf,
			fSumOfMomentsf,

			fDiff,

			fNegLogOfLenOfSquare_Arrf[nNumOfIters_ForDim_2powerN_Max],

			//fLogPoints_GenerDim_Arrf[nNumOfIters_ForDim_2powerN_Max],
			//fLogPoints_Spectrum_Arrf[nNumOfIters_ForDim_2powerN_Max],
			fLogPoints_CrowdingIndex_Arrf[nNumOfIters_ForDim_2powerN_Max],

			fMoments_Arrf[nNumOfIters_ForDim_2powerN_Max],
			fMomentsNormalized_Arrf[nNumOfIters_ForDim_2powerN_Max],

			fLogPointf;

		///////////////////////////////////////

		for (iIterf = 0; iIterf < nNumOfIters_ForDim_2powerN_Max; iIterf++)
		{
			fNegLogOfLenOfSquare_Arrf[iIterf] = -1.0;
			//fLogPoints_GenerDim_Arrf[iIterf] = -1.0;
			//fLogPoints_Spectrum_Arrf[iIterf] = -1.0;
			fLogPoints_CrowdingIndex_Arrf[iIterf] = -1.0;

			fMoments_Arrf[iIterf] = 0.0;
			fMomentsNormalized_Arrf[iIterf] = 0.0;

		} //for (iIterf = 0; iIterf < nNumOfIters_ForDim_2powerN_Max; iIterf++)
	//////////////////////////////////
		if (fQf < feps && fQf > -feps)
		{
			nIsfQ_0f = 1;
			goto Mark_CrowdingIndexOf_Q_order_1;
		} // if (fQf < feps && fQf > -feps)
	///////////////////////
		fDiff = (float)(1.0) - fQf;
		if (fDiff < feps && fDiff > -feps)
		{
			nIsfQ_1f = 1; //information dimension

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'OneFea_CrowdingIndexOf_Q_order': fDiff = %E, nIsfQ_0f = %d, nIsfQ_1f = %d", fDiff, nIsfQ_0f, nIsfQ_1f);
			fprintf(fout_lr, "\n nNumOfOneFea_Spectrum_AdjustedGlob = %d, nNumOfSpectrumFea_Glob = %d", nNumOfOneFea_Spectrum_AdjustedGlob, nNumOfSpectrumFea_Glob);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS
		} // if (fDiff < feps && fDiff > -feps)

		///////////////////////////////////////////
	Mark_CrowdingIndexOf_Q_order_1:		nNumOfLogPointsf = 0; // nScalef;
		nLenOfSquaref = nDim_2pNf;

		fSumOfMomentsf = 0.0;

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'OneFea_CrowdingIndexOf_Q_order': iIntensity_Interval_1_Glob = %d, iIntensity_Interval_2_Glob = %d, fQf = %E, nScalef = %d",
			iIntensity_Interval_1_Glob, iIntensity_Interval_2_Glob, fQf, nScalef);

#endif //#ifndef COMMENT_OUT_ALL_PRINTS

		for (iScalef = 0; iScalef < nScalef; iScalef++)
		{
			nLenOfSquaref = nLenOfSquaref / 2;

			if (nLenOfSquaref < 2)
			{
				break;
			}//if (nLenOfSquaref < 2)

			nResf = MomentOf_Q_order_AtFixedLenOfSquareMultiFrac(

				nDim_2pNf, //const int nDim_2pNf,

				nLenOfSquaref, //const int nLenOfSquaref,

				sColor_ImageInitf, //const COLOR_IMAGE *sColor_ImageInitf,

				sImageEmbeddedf_BlackWhitef, //const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_BlackWhitef, //) //[nDim_2pNf*nDim_2pNf]

				fQf, //const float fQf,
				nIntOrFloatf, //const int nIntOrFloatf, //1 -- int, 0 -- float
			///////////////////////////////////////
				nNumOfNonZero_ObjectSquaresTotf, //int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf

				fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf,	// float &fSumOfMomentsForGenerDim_Q_OrderAtFixedLenf);
				fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf, //float &fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf,
				fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf); // float &fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);

			fMoments_Arrf[iScalef] = fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf;
			fSumOfMomentsf += fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n 'OneFea_CrowdingIndexOf_Q_order': nNumOfNonZero_ObjectSquaresTotf = %d, fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf = %E, fQf = %E, nNumOfLogPointsf = %d, fSumOfMomentsf = %E",
				nNumOfNonZero_ObjectSquaresTotf, fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf, fQf, nNumOfLogPointsf, fSumOfMomentsf);

			fprintf(fout_lr, "\n iScalef = %d, nLenOfSquaref = %d", iScalef, nLenOfSquaref);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			if (nResf == UNSUCCESSFUL_RETURN)
			{
				return UNSUCCESSFUL_RETURN;
			}//if (nResf == UNSUCCESSFUL_RETURN)

			if (nResf == -2)
			{
#ifndef COMMENT_OUT_ALL_PRINTS
				fprintf(fout_lr, "\n\n 'OneFea_CrowdingIndxOf_Q_order': continue for iScalef = %d, nLenOfSquaref = %d; not enough points", iScalef, nLenOfSquaref);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

				continue;
			}//if (nResf == -2)

			if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
			{
				if (fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf > 0.0) //> feps?
				{
					fLogPointf = log(fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);
				} //
				else
				{
					fLogPointf = 0.0;
				}//else
			} //if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
			else if (nIsfQ_0f == 1)
			{
				if (nNumOfNonZero_ObjectSquaresTotf <= 0)
				{
#ifndef COMMENT_OUT_ALL_PRINTS
					printf("\n\n An error in 'OneFea_CrowdingIndxOf_Q_order': nNumOfNonZero_ObjectSquaresTotf = %d <= 0", nNumOfNonZero_ObjectSquaresTotf);
					fprintf(fout_lr, "\n\n An error in 'OneFea_CrowdingIndxOf_Q_order': nNumOfNonZero_ObjectSquaresTotf = %d <= 0", nNumOfNonZero_ObjectSquaresTotf);

					printf("\n\n Please press any key:"); getchar();
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

					return UNSUCCESSFUL_RETURN;
				}// if (nNumOfNonZero_ObjectSquaresTotf <= 0)

				fLogPointf = log((float)(nNumOfNonZero_ObjectSquaresTotf));

			} //else if (nIsfQ_0f == 1)
			else if (nIsfQ_1f == 1)
			{
				if (fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf > 0.0) //> feps?
				{
					fLogPointf = fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf * log(fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf);
				} //
				else
				{
					fLogPointf = 0.0;
				}//else
			} //else if (nIsfQ_1f == 1)

			fNegLogOfLenOfSquare_Arrf[nNumOfLogPointsf] = -log((float)(nLenOfSquaref));

			//fLogPoints_GenerDim_Arrf[nNumOfLogPointsf] = fLogPointf;

			//fLogPoints_Spectrum_Arrf[nNumOfLogPointsf] = fSumOfMomentsForSpectrum_Q_OrderAtFixedLenf;
			//fLogPoints_CrowdingIndex_Arrf[nNumOfLogPointsf] = fSumOfMomentsForCrowdingIndex_Q_OrderAtFixedLenf; //?
			fLogPoints_CrowdingIndex_Arrf[nNumOfLogPointsf] = fLogPointf;

#ifndef COMMENT_OUT_ALL_PRINTS
			printf("\n\n 'OneFea_CrowdingIndxOf_Q_order': iScalef = %d, nLenOfSquaref = %d, fLogPoints_CrowdingIndex_Arrf[%d] = %E, fNegLogOfLenOfSquare_Arrf[%d] = %E",
				iScalef, nLenOfSquaref, nNumOfLogPointsf, fLogPoints_CrowdingIndex_Arrf[nNumOfLogPointsf], nNumOfLogPointsf, fNegLogOfLenOfSquare_Arrf[nNumOfLogPointsf]);

			fprintf(fout_lr, "\n\n 'OneFea_CrowdingIndxOf_Q_order': iScalef = %d, nLenOfSquaref = %d, fLogPoints_CrowdingIndex_Arrf[%d] = %E, fNegLogOfLenOfSquare_Arrf[%d] = %E",
				iScalef, nLenOfSquaref, nNumOfLogPointsf, fLogPoints_CrowdingIndex_Arrf[nNumOfLogPointsf], nNumOfLogPointsf, fNegLogOfLenOfSquare_Arrf[nNumOfLogPointsf]);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

			nNumOfLogPointsf += 1;
		}//for (iScalef = 0; iScalef < nScalef; iScalef++)
	/////////////////////////////////////////

#ifndef COMMENT_OUT_ALL_PRINTS
		fprintf(fout_lr, "\n\n 'OneFea_CrowdingIndxOf_Q_order': nNumOfLogPointsf = %d, iIntensity_Interval_1_Glob = %d, iIntensity_Interval_2_Glob = %d, fQf = %E",
			nNumOfLogPointsf, iIntensity_Interval_1_Glob, iIntensity_Interval_2_Glob, fQf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	
		////////////////////////////////////////////////////////////////////////////////
		if (nNumOfLogPointsf <= 1)
		{
			//fGenerDim_Order_Qf = -1.0;

			//fSpectrum_Order_Qf = -1.0;
			fCrowdingIndex_Order_Qf = -1.0;

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n The end of 'OneFea_CrowdingIndxOf_Q_order': nNumOfLogPointsf = %d is insufficient, fCrowdingIndex_Order_Qf = %E", nNumOfLogPointsf, fCrowdingIndex_Order_Qf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		}//if (nNumOfLogPointsf <= 1)
		else
		{
			/*
			SlopeOfAStraightLine(
				nNumOfLogPointsf, //const int nDimf,

				fNegLogOfLenOfSquare_Arrf, //const float fX_Arrf[],
				fLogPoints_GenerDim_Arrf, //const float fY_Arrf[],

				fGenerDim_Order_Qf); // float &fSlopef);

			fDiff = 1.0 - fQf; //above

			if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
			{
				fGenerDim_Order_Qf = fGenerDim_Order_Qf / fDiff;
			} // if (nIsfQ_0f == 0 && nIsfQ_1f == 0)
			*/
			//////////////
/*
			SlopeOfAStraightLine(
				nNumOfLogPointsf, //const int nDimf,

				fNegLogOfLenOfSquare_Arrf, //const float fX_Arrf[],
				fLogPoints_Spectrum_Arrf, //const float fY_Arrf[],

				fSpectrum_Order_Qf); // float &fSlopef);
*/

	///////////////////////////
			SlopeOfAStraightLine(
				nNumOfLogPointsf, //const int nDimf,

				fNegLogOfLenOfSquare_Arrf, //const float fX_Arrf[],
				fLogPoints_CrowdingIndex_Arrf, //const float fY_Arrf[],

				fCrowdingIndex_Order_Qf); // float &fSlopef);

#ifndef COMMENT_OUT_ALL_PRINTS
			fprintf(fout_lr, "\n\n The end of 'OneFea_CrowdingIndxOf_Q_order': nNumOfLogPointsf = %d, fCrowdingIndex_Order_Qf = %E, fQf = %E",
				nNumOfLogPointsf, fCrowdingIndex_Order_Qf, fQf);

			//	fprintf(fout_lr, "\n fGenerDim_Order_Qf = %E, fSpectrum_Order_Qf = %E, fCrowdingIndex_Order_Qf = %E", fGenerDim_Order_Qf, fSpectrum_Order_Qf, fCrowdingIndex_Order_Qf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS	

		} // else
	////////////////////////////////////////////////
		return SUCCESSFUL_RETURN;
	}//int  int OneFea_CrowdingIndxOf_Q_order((...

  //printf("\n\n Please press any key:"); getchar();



