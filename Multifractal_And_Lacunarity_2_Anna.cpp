//
//  Multifractal_SelectedFeas_2.cpp
//  FeatureLibrary
//
//  Created by Anna Yang on 1/2/20.
//  Copyright © 2020 imagosystems. All rights reserved.
//

#include <stdio.h>
#include "Multifractal_And_Lacunarity_2.h"

//Tug-of-war lacunarityóA novel approach for estimating lacunarity --https://aip.scitation.org/doi/full/10.1063/1.4966539
int MassesOfAll_ObjectSquares_AtFixedLenOfSquare(
                                                 const int nDim_2pNf,
                                                 
                                                 const int nLenOfSquaref,
                                                 
                                                 const COLOR_IMAGE *sColor_ImageInitf,
                                                 
                                                 const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]
                                                 
                                                 int &nNumOfSquaresInImageTotf,
                                                 
                                                 int &nNumOfNonZero_ObjectSquaresTotf, //<= nNumOfSquaresTotf
                                                 int &nMassOfImageTotf, //<=
                                                 
                                                 int nMassesOfSquaresArrf[]) //[nNumOfSquaresTotf]


{
    int
    nIndexOfSquareCurf,
    
    nIndexOfPixelInEmbeddedImagef,
    
    nIndexOfPixelMaxf = nDim_2pNf * nDim_2pNf,
    
    iWidf,
    iLenf,
    
    nNumOfSquaresInImageSidef = nDim_2pNf / nLenOfSquaref,
    
    nMassOfASquaref,
    nMassOfASquareMaxf = nLenOfSquaref * nLenOfSquaref,
    
    nWidOfSquareMinf,
    nWidOfSquareMaxf,
    
    nLenOfSquareMin_Laf,
    nLenOfSquareMaxf,
    
    iWidSquaresf,
    iLenSquaresf;
    
    //float        fRatioOfSquaresf;
    ////////////////////////////////////////////////
    nNumOfSquaresInImageTotf = nNumOfSquaresInImageSidef * nNumOfSquaresInImageSidef;
    
    nNumOfNonZero_ObjectSquaresTotf = 0;
    nMassOfImageTotf = 0;
    
#ifndef COMMENT_OUT_ALL_PRINTS
    nMassOfASquareMaxf = nLenOfSquaref * nLenOfSquaref;
    
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
    
    for (iWidSquaresf = 0; iWidSquaresf < nNumOfSquaresInImageSidef; iWidSquaresf++)
    {
        nWidOfSquareMinf = iWidSquaresf * nLenOfSquaref;
        nWidOfSquareMaxf = (iWidSquaresf + 1)* nLenOfSquaref;
        
        for (iLenSquaresf = 0; iLenSquaresf < nNumOfSquaresInImageSidef; iLenSquaresf++)
        {
            nIndexOfSquareCurf = iLenSquaresf + iWidSquaresf*nNumOfSquaresInImageSidef;
            if (nIndexOfSquareCurf >= nNumOfSquaresInImageTotf)
            {
#ifndef COMMENT_OUT_ALL_PRINTS
                printf("\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nIndexOfSquareCurf = %d >= nNumOfSquaresInImageTotf = %d", nIndexOfSquareCurf, nNumOfSquaresInImageTotf);
                
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
                
                return UNSUCCESSFUL_RETURN;
            }//if (nIndexOfSquareCurf >= nNumOfSquaresInImageTotf)
            
            nLenOfSquareMin_Laf = iLenSquaresf * nLenOfSquaref;
            nLenOfSquareMaxf = (iLenSquaresf + 1)* nLenOfSquaref;
            
            if (nLenOfSquareMin_Laf > sColor_ImageInitf->nLength || nWidOfSquareMinf > sColor_ImageInitf->nWidth)
            {
                nMassOfASquaref = 0;
                goto MarkContinueForiLenSquares;
            }//if (nLenOfSquareMin_Laf > sColor_ImageInitf->nLength || nWidOfSquareMinf > sColor_ImageInitf->nWidth)
            
            nMassOfASquaref = 0;
            for (iWidf = nWidOfSquareMinf; iWidf < nWidOfSquareMaxf; iWidf++)
            {
                for (iLenf = nLenOfSquareMin_Laf; iLenf < nLenOfSquareMaxf; iLenf++)
                {
                    nIndexOfPixelInEmbeddedImagef = iLenf + (iWidf*nDim_2pNf);
                    
                    if (nIndexOfPixelInEmbeddedImagef >= nIndexOfPixelMaxf)
                    {
#ifndef COMMENT_OUT_ALL_PRINTS
                        printf("\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nIndexOfPixelInEmbeddedImagef = %d >= nIndexOfPixelMaxf = %d", nIndexOfPixelInEmbeddedImagef, nIndexOfPixelMaxf);
                        
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
                        
                        return UNSUCCESSFUL_RETURN;
                    }//if (nIndexOfPixelInEmbeddedImagef >= nIndexOfPixelMaxf)
                    
                    if (sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] == 1)
                    {
                        nMassOfASquaref += 1;
                    } //if (sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] == 1)
                    
                } //for (iLenf = nLenOfSquareMin_Laf
                
            } // for (iWidf = nWidOfSquareMinf; iWidf < nWidOfSquareMaxf; iWidf++)
            
            nMassOfImageTotf += nMassOfASquaref;
            
        MarkContinueForiLenSquares: nMassesOfSquaresArrf[nIndexOfSquareCurf] = nMassOfASquaref;
            if (nMassOfASquaref > 0)
            {
                nNumOfNonZero_ObjectSquaresTotf += 1;
            }//if (nMassOfASquaref > 0)
            
            if (nMassOfASquaref > nMassOfASquareMaxf)
            {
#ifndef COMMENT_OUT_ALL_PRINTS
                printf("\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nMassOfASquaref = %d > nMassOfASquareMaxf = %d", nMassOfASquaref, nMassOfASquareMaxf);
                printf( "\n iLenSquaresf = %d, iWidSquaresf = %d, nNumOfSquaresInImageSidef = %d", iLenSquaresf, iWidSquaresf, nNumOfSquaresInImageSidef);
                
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
                
                return UNSUCCESSFUL_RETURN;
            }//if (nMassOfASquaref > nMassOfASquareMaxf)
        } //for (iLenSquaresf = 0; iLenSquaresf < nNumOfSquaresInImageSidef; iLenSquaresf++)
        
    } // for (iWidSquaresf = 0; iWidSquaresf < nNumOfSquaresInImageSidef; iWidSquaresf++)
    
    if (nMassOfImageTotf < 0)
    {
#ifndef COMMENT_OUT_ALL_PRINTS
        printf("\n\n An error in 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nMassOfImageTotf = %d < 0", nMassOfImageTotf);
        
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
        
        return UNSUCCESSFUL_RETURN;
    } // if (nMassOfImageTotf < 0)
    
#ifndef COMMENT_OUT_ALL_PRINTS
    printf("\n\n The end of 'MassesOfAll_ObjectSquares_AtFixedLenOfSquare': nNumOfNonZero_ObjectSquaresTotf = %d, nNumOfSquaresInImageTotf = %d",
           nNumOfNonZero_ObjectSquaresTotf, nNumOfSquaresInImageTotf);
    
    printf("\n nLenOfSquaref = %d, nNumOfSquaresInImageSidef = %d, nMassOfImageTotf = %d", nLenOfSquaref, nNumOfSquaresInImageSidef, nMassOfImageTotf);
    
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
    
    return SUCCESSFUL_RETURN;
} //int MassesOfAll_ObjectSquares_AtFixedLenOfSquare(...

float PowerOfAFloatNumber(
                          const int nIntOrFloatf, //1 -- int, 0 -- float
                          const int nPowerf,
                          const float fPowerf,
                          
                          const float fFloatInitf) //fFloatInitf != 0.0
{
    int
    iPowf;
    
    float
    fPowerCurf = fFloatInitf;
    
    if (nIntOrFloatf == 1)
    {
        if (nPowerf == 0)
            return 1.0;
        else if (nPowerf > 0)
        {
            for (iPowf = 1; iPowf < nPowerf; iPowf++)
            {
                fPowerCurf = fPowerCurf * fFloatInitf;
            }//for (iPowf = 1; iPowf < nPowerf; iPowf++)
            
        }//else if (nPowerf > 0)
        else if (nPowerf < 0)
        {
            for (iPowf = 1; iPowf < nPowerf; iPowf++)
            {
                fPowerCurf = fPowerCurf * fFloatInitf;
            }//for (iPowf = 1; iPowf < nPowerf; iPowf++)
            
            fPowerCurf = (float)(1.0) / fPowerCurf;
        } //else if (nPowerf < 0)
        
    }//if (nIntOrFloatf == 1)
    else if (nIntOrFloatf == 0)
    {
        fPowerCurf = powf(fFloatInitf, fPowerf);
        
    }//else if (nIntOrFloatf == 0)
    
    return fPowerCurf;
}//float PowerOfAFloatNumber(

int NumOfObjectSquaresAndLogPoint_WithResolution(
                                                 const int nDim_2pNf,
                                                 
                                                 const int nLenOfSquaref,
                                                 
                                                 const COLOR_IMAGE *sColor_ImageInitf,
                                                 
                                                 const EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef, //) //[nDim_2pNf*nDim_2pNf]
                                                 
                                                 int &nNumOfObjectSquaresTotf,
                                                 float &fLogPointf)

{
    int
    nIndexOfPixelInEmbeddedImagef,
    
    nIndexOfPixelMaxf = nDim_2pNf* nDim_2pNf,
    
    iWidf,
    iLenf,
    
    nNumOfSquaresInImageSidef = nDim_2pNf / nLenOfSquaref,
    
    nNumOfSquaresInImageTotf, // = nNumOfSquaresInImageSidef* nNumOfSquaresInImageSidef,
    
    nWidOfSquareMinf,
    nWidOfSquareMaxf,
    
    nLenOfSquareMin_Laf,
    nLenOfSquareMaxf,
    
    iWidSquaresf,
    iLenSquaresf;
    
    float
    fRatioOfSquaresf;
    ////////////////////////////////////////////////
    nNumOfSquaresInImageTotf = nNumOfSquaresInImageSidef * nNumOfSquaresInImageSidef;
    
    nNumOfObjectSquaresTotf = 0;
    
#ifndef COMMENT_OUT_ALL_PRINTS
    
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
    
    for (iWidSquaresf = 0; iWidSquaresf < nNumOfSquaresInImageSidef; iWidSquaresf++)
    {
        nWidOfSquareMinf = iWidSquaresf * nLenOfSquaref;
        
        if (nWidOfSquareMinf > sColor_ImageInitf->nWidth)
        {
            continue;
        }//if (nWidOfSquareMinf > sColor_ImageInitf->nWidth)
        
        nWidOfSquareMaxf = (iWidSquaresf + 1)* nLenOfSquaref;
        
        for (iLenSquaresf = 0; iLenSquaresf < nNumOfSquaresInImageSidef; iLenSquaresf++)
        {
            nLenOfSquareMin_Laf = iLenSquaresf * nLenOfSquaref;
            nLenOfSquareMaxf = (iLenSquaresf + 1)* nLenOfSquaref;
            
            if (nLenOfSquareMin_Laf > sColor_ImageInitf->nLength)
            {
                continue;
            }//if (nLenOfSquareMin_Laf > sColor_ImageInitf->nLength)
            
            for (iWidf = nWidOfSquareMinf; iWidf < nWidOfSquareMaxf; iWidf++)
            {
                for (iLenf = nWidOfSquareMinf; iLenf < nLenOfSquareMaxf; iLenf++)
                {
                    nIndexOfPixelInEmbeddedImagef = iLenf + (iWidf*nDim_2pNf);
                    
                    if (nIndexOfPixelInEmbeddedImagef >= nIndexOfPixelMaxf)
                    {
#ifndef COMMENT_OUT_ALL_PRINTS
                        printf("\n\n An error in 'NumOfObjectSquaresAndLogPoint_WithResolution': nIndexOfPixelInEmbeddedImagef = %d >= nIndexOfPixelMaxf = %d", nIndexOfPixelInEmbeddedImagef,nIndexOfPixelMaxf);
                        
                        
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
                        
                        return UNSUCCESSFUL_RETURN;
                    }//if (nIndexOfPixelInEmbeddedImagef >= nIndexOfPixelMaxf)
                    
                    if (sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] == 1)
                    {
                        nNumOfObjectSquaresTotf += 1;
                        
                        goto MarkContinueForiLenSquares;
                    } //if (sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] == 1)
                    
                } //for (iLenf = nWidOfSquareMinf; iLenf < nLenOfSquareMaxf; iLenf++)
            } // for (iWidf = nWidOfSquareMinf; iWidf < nWidOfSquareMaxf; iWidf++)
            
        MarkContinueForiLenSquares: continue;
        } //for (iLenSquaresf = 0; iLenSquaresf < nNumOfSquaresInImageSidef; iLenSquaresf++)
        
    } // for (iWidSquaresf = 0; iWidSquaresf < nNumOfSquaresInImageSidef; iWidSquaresf++)
    
    fRatioOfSquaresf = (float)(nNumOfObjectSquaresTotf) / (float)(nNumOfSquaresInImageTotf);
    
    if (nNumOfObjectSquaresTotf > 1)
    {
        //fLogPointf = log(nNumOfObjectSquaresTotf) / (-log(nLenOfSquaref));
        fLogPointf = (float)(log(nNumOfObjectSquaresTotf) );
    }
    else
        fLogPointf = 0.0;
    
#ifndef COMMENT_OUT_ALL_PRINTS
    printf( "\n\n The end of 'NumOfObjectSquaresAndLogPoint_WithResolution': nNumOfObjectSquaresTotf = %d, nNumOfSquaresInImageTotf = %d, fLogPointf = %E, fRatioOfSquaresf = %E",
           nNumOfObjectSquaresTotf, nNumOfSquaresInImageTotf, fLogPointf, fRatioOfSquaresf);
    printf("\n nLenOfSquaref = %d, nNumOfSquaresInImageSidef = %d", nLenOfSquaref, nNumOfSquaresInImageSidef);
    
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
    
    return SUCCESSFUL_RETURN;
} //int NumOfObjectSquaresAndLogPoint_WithResolution(...

//https://www.varsitytutors.com/hotmath/hotmath_help/topics/line-of-best-fit
void SlopeOfAStraightLine(
                          const int nDimf,
                          
                          const float fX_Arrf[],
                          const float fY_Arrf[],
                          
                          float &fSlopef)
{
    int
    i;
    float
    fX_Diff,
    fX_Averf = 0.0,
    fY_Averf = 0.0,
    
    fSumForNumeratorf = 0.0,
    fSumForDenominatorf = 0.0;
    
    for (i = 0; i < nDimf; i++)
    {
        fX_Averf += fX_Arrf[i];
        fY_Averf += fY_Arrf[i];
        
#ifndef COMMENT_OUT_ALL_PRINTS
        printf("\n\n 'SlopeOfAStraightLine' 1: i = %d, fX_Averf = %E, fY_Averf = %E, fY_Arrf[i] = %E", i, fX_Averf, fY_Averf, fY_Arrf[i]);
        
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
        
    }//for (i = 0; i < nDimf; i++)
    
    fX_Averf = fX_Averf / nDimf;
    fY_Averf = fY_Averf / nDimf;
    
#ifndef COMMENT_OUT_ALL_PRINTS
    printf("\n\n 'SlopeOfAStraightLine': nDimf = %d, fX_Averf = %E, fY_Averf = %E", nDimf, fX_Averf, fY_Averf);
    
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
    for (i = 0; i < nDimf; i++)
    {
        fX_Diff = fX_Arrf[i] - fX_Averf;
        
        fSumForNumeratorf += fX_Diff * (fY_Arrf[i] - fY_Averf);
        
        fSumForDenominatorf += fX_Diff * fX_Diff;
#ifndef COMMENT_OUT_ALL_PRINTS
        printf("\n\n 'SlopeOfAStraightLine': i = %d, fSumForNumeratorf = %E, fSumForDenominatorf = %E", i, fSumForNumeratorf, fSumForDenominatorf);
        
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
    }//for (i = 0; i < nDimf; i++)
    
    if (fSumForDenominatorf > 0.0)
    {
        fSlopef = fSumForNumeratorf / fSumForDenominatorf;
    }//if (fSumForDenominatorf > 0.0)
    else
        fSlopef = fLarge;
    
}//void SlopeOfAStraightLine(...

int Dim_2powerN(
                const int nLengthf,
                const int nWidthf,
                
                int &nScalef,
                int &nDim_2pNf)
{
    int
    i,
    nTempf,
    nLargerDimInitf;
    
    if (nLengthf >= nWidthf)
        nLargerDimInitf = nLengthf;
    else
        nLargerDimInitf = nWidthf;
    
    nTempf = 2;
    for (i = 2; i < nNumOfIters_ForDim_2powerN_Max_La; i++)
    {
        nTempf = nTempf * 2;
        if (nTempf >= nLargerDimInitf)
        {
            nDim_2pNf = nTempf;
            break;
        }//if (nTempf >= nLargerDimInitf)
    }//for (i = 2; i < nNumOfIters_ForDim_2powerN_Max_La; i++)
    /////////////////////////////////
    
    if (i == 3 || i >= nNumOfIters_ForDim_2powerN_Max_La - 1)
    {
#ifndef COMMENT_OUT_ALL_PRINTS
        printf( "\n\n An error in 'Dim_2powerN': i = %d, nNumOfIters_ForDim_2powerN_Max_La - 1 = %dE", i, nNumOfIters_ForDim_2powerN_Max_La - 1);
        
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
        return UNSUCCESSFUL_RETURN;
    }
    else
    {
        nScalef = i;
        return SUCCESSFUL_RETURN;
    } //
}//int Dim_2powerN(...

int Initializing_Embedded_Image(
                                const int nDim_2pNf,
                                
                                EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef) // //[nDim_2pNf*nDim_2pNf]
{
    int
    nIndexOfPixelInEmbeddedImagef,
    nImageSizeCurf = nDim_2pNf * nDim_2pNf,
    
    iWidf,
    iLenf;
    
    sImageEmbeddedf_ForLacunarityBlackWhitef->nWidth = nDim_2pNf;
    sImageEmbeddedf_ForLacunarityBlackWhitef->nLength = nDim_2pNf;
    
    sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr = new int[nImageSizeCurf];
    
    if (sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr == nullptr)
    {
#ifndef COMMENT_OUT_ALL_PRINTS
        printf("\n\n An error in 'Initializing_Embedded_Image': sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr == nullptr");
        
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
        
        return UNSUCCESSFUL_RETURN;
    } //if (sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr == nullptr)
    
    for (iWidf = 0; iWidf < nDim_2pNf; iWidf++)
    {
        
        for (iLenf = 0; iLenf < nDim_2pNf; iLenf++)
        {
            nIndexOfPixelInEmbeddedImagef = iLenf + (iWidf*nDim_2pNf);
            
            sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] = -1; //invalid
            
        } //for (iLenf = 0; iLenf < nDim_2pNf; iLenf++)
    } // for (iWidf = 0; iWidf < nDim_2pNf; iWidf++)
    
    return SUCCESSFUL_RETURN;
} //int Initializing_Embedded_Image(...

int Embedding_Image_Into_2powerN_ForHausdorff(
                                              const int nDim_2pNf,
                                              
                                              const int nThresholdForIntensitiesMinf,
                                              const int nThresholdForIntensitiesMaxf,
                                              
                                              const WEIGHTES_OF_RGB_COLORS *sWeightsOfColorsf,
                                              
                                              const COLOR_IMAGE *sColor_ImageInitf,
                                              EMBEDDED_IMAGE_BLACK_WHITE *sImageEmbeddedf_ForLacunarityBlackWhitef) //[nDim_2pNf*nDim_2pNf]

{
    int
    nIndexOfPixelCurf,
    nIndexOfPixelInEmbeddedImagef,
    
    iWidf,
    iLenf,
    
    nNumOfWhitePixelsTotf = 0,
    nNumOfBlackPixelsTotf = 0,
    
    nRedInitf,
    nGreenInitf,
    nBlueInitf,
    
    nDivisorf = 3,
    
    nRedCurf,
    nGreenCurf,
    nBlueCurf,
    nIntensityAverf;
    
    float
    fRatiof;
    
#ifndef COMMENT_OUT_ALL_PRINTS
    
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
    
    if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
    {
        nDivisorf = 3;
    } // if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
    else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
    {
        nDivisorf = 2;
    } //else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
    
    else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
    {
        nDivisorf = 2;
    } //else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
    else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
    {
        nDivisorf = 2;
    } //else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
    
    else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
    {
        nDivisorf = 1;
    } //else if (sWeightsOfColorsf->fWeightOfRed > 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
    else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
    {
        nDivisorf = 1;
    } //else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen > 0.0 && sWeightsOfColorsf->fWeightOfBlue == 0.0)
    else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
    {
        nDivisorf = 1;
    } //else if (sWeightsOfColorsf->fWeightOfRed == 0.0 && sWeightsOfColorsf->fWeightOfGreen == 0.0 && sWeightsOfColorsf->fWeightOfBlue > 0.0)
    else
    {
#ifndef COMMENT_OUT_ALL_PRINTS
        printf("\n\n An error in 'Embedding_Image_Into_2powerN_ForHausdorff': the color weights are wrong");
        printf( "\n\n sWeightsOfColorsf->fWeightOfRed = %E, sWeightsOfColorsf->fWeightOfGreen = %E, sWeightsOfColorsf->fWeightOfBlue = %E",
               sWeightsOfColorsf->fWeightOfRed, sWeightsOfColorsf->fWeightOfGreen, sWeightsOfColorsf->fWeightOfBlue);
        
        printf( "\n nDim_2pNf = %d, nThresholdForIntensitiesMinf = %d, nThresholdForIntensitiesMaxf = %d", nDim_2pNf, nThresholdForIntensitiesMinf, nThresholdForIntensitiesMaxf);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
        return UNSUCCESSFUL_RETURN;
    }//else
    
    for (iWidf = 0; iWidf < nDim_2pNf; iWidf++)
    {
        for (iLenf = 0; iLenf < nDim_2pNf; iLenf++)
        {
            nIndexOfPixelInEmbeddedImagef = iLenf + (iWidf*nDim_2pNf);
            
            if (iWidf < sColor_ImageInitf->nWidth && iLenf < sColor_ImageInitf->nLength)
            {
                nIndexOfPixelCurf = iLenf + iWidf * sColor_ImageInitf->nLength;
                
                nRedInitf = sColor_ImageInitf->nRed_Arr[nIndexOfPixelCurf];
                nGreenInitf = sColor_ImageInitf->nGreen_Arr[nIndexOfPixelCurf];
                nBlueInitf = sColor_ImageInitf->nBlue_Arr[nIndexOfPixelCurf];
                
                nRedCurf = (int)((float)(nRedInitf)* sWeightsOfColorsf->fWeightOfRed);
                nBlueCurf = (int)((float)(nGreenInitf)* sWeightsOfColorsf->fWeightOfGreen);
                nGreenCurf = (int)((float)(nBlueInitf)* sWeightsOfColorsf->fWeightOfBlue);
                
                nIntensityAverf = (nRedCurf + nBlueCurf + nGreenCurf) / nDivisorf;
                
                if (nIntensityAverf > nIntensityStatMax_La)
                    nIntensityAverf = nIntensityStatMax_La;
                
                if (nIntensityAverf >= nThresholdForIntensitiesMinf && nIntensityAverf <= nThresholdForIntensitiesMaxf)
                {
                    sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] = 1; //
                    nNumOfWhitePixelsTotf += 1;
                } //if (nIntensityAverf >= nThresholdForIntensitiesMinf && nIntensityAverf <= nThresholdForIntensitiesMaxf)
                else
                {
                    sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] = 0;
                    nNumOfBlackPixelsTotf += 1;
                }//else if (nIntensityAverf <= nThresholdForIntensitiesf)
                
            } // if (iWidf < sColor_ImageInitf->nWidth && iLenf < sColor_ImageInitf->nLength)
            else
            {
                sImageEmbeddedf_ForLacunarityBlackWhitef->nEmbeddedImage_Arr[nIndexOfPixelInEmbeddedImagef] = 0; // no nonzero pixels
                nNumOfBlackPixelsTotf += 1;
            }//else
            
        } //for (iLenf = 0; iLenf < nDim_2pNf; iLenf++)
    } // for (iWidf = 0; iWidf < nDim_2pNf; iWidf++)
    
    fRatiof = (float)(nNumOfWhitePixelsTotf) / (float)(nDim_2pNf*nDim_2pNf);
    
#ifndef COMMENT_OUT_ALL_PRINTS
    
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
    
    return SUCCESSFUL_RETURN;
} //int Embedding_Image_Into_2powerN_ForHausdorff(...
