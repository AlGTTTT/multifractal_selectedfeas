//
//  sample.cpp
//  FeatureLibrary
//
//  Created by Anna Yang on 1/2/20.
//  Copyright © 2020 imagosystems. All rights reserved.
//

#include <stdio.h>

int FeatureStats::doGeneratingFeaturesInOneRow(
                                               const Image& image_in,
                                               FEATURES_ALL_OF_COOC *sFeasAllOfCoocf)
//    Image& image_out)
{
    
    int
    nRes;
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    int
    i,
    j,
    
    nSizeOfImage, // = nImageWidth*nImageHeight,
    
    nIndexOfPixelCur,
    
    iLen,
    iWid,
    
    nRed,
    nGreen,
    nBlue,
    
    nIntensityOfMaskImage,
    nIntensity_Read_Test_ImageMax = -nLarge,
    
    nImageWidth,
    nImageHeight;
    ////////////////////////////////////////////////////////////////////////
    fout_lr = fopen("wMain_Image_Features.txt", "w");
    
    if (fout_lr == NULL)
    {
#ifndef COMMENT_OUT_ALL_PRINTS
        printf("\n\n fout_lr == NULL");
        getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
        
        return - 1;
    } //if (fout_lr == NULL)
    
    fout_PrintFeatures = fopen("wVectorOfFeatures.txt", "w");
    
    if (fout_PrintFeatures == NULL)
    {
#ifndef COMMENT_OUT_ALL_PRINTS
        printf("\n\n fout_PrintFeatures == NULL");
        getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
        
        return -1;
    } //if (fout_PrintFeatures == NULL)
    
    // size of image
    nImageWidth = image_in.width();
    
    nImageWidth = image_in.width();
    nImageHeight = image_in.height();
    
#ifndef COMMENT_OUT_ALL_PRINTS
    printf("\n\n nImageWidth = %d, nImageHeight = %d", nImageWidth, nImageHeight);
    fprintf(fout_lr, "\n\n nImageWidth = %d, nImageHeight = %d", nImageWidth, nImageHeight);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
    
    //#define nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot (nNumOfHistogramIntervals + nNumOfHistogramIntervals - 2 + nAllScales_andCoocSizeMax + (nNumOfDirecAndDistAndScaleTot * 11) + (nNumOfScalesAndDistTot*11) )
    
#ifndef COMMENT_OUT_ALL_PRINTS
    printf("\n\n The total number of features: nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot = %d", nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot);
    
    printf( "\n\n nCoocSizeMax = %d, nNumOfHistogramIntervals = %d, nNumOfDirecAndDistAndScaleTot = %d, nNumOfScalesAndDistTot = %d",
           nCoocSizeMax, nNumOfHistogramIntervals, nNumOfDirecAndDistAndScaleTot, nNumOfScalesAndDistTot);
    
    fprintf(fout_lr,"\n\n The total number of features: nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot = %d", nNumOfAllFeasIn_FEATURES_ALL_OF_COOC_Tot);
    
    fprintf(fout_lr, "\n\n nCoocSizeMax = %d, nNumOfHistogramIntervals = %d, nNumOfDirecAndDistAndScaleTot = %d, nNumOfScalesAndDistTot = %d",
            nCoocSizeMax,nNumOfHistogramIntervals, nNumOfDirecAndDistAndScaleTot, nNumOfScalesAndDistTot);
    
    fflush(fout_lr);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
    
    int bytesOfWidth = image_in.pitchInBytes();
    
    Image imageToSave(nImageWidth, nImageHeight, bytesOfWidth);
    
    ///////////////////////////////////////////////////////
    COLOR_IMAGE sColor_Image; //
    
    nRes = Initializing_Color_To_CurSize_A(
                                           nImageHeight, //const int nImageWidthf,
                                           nImageWidth, //const int nImageLengthf,
                                           
                                           &sColor_Image); // COLOR_IMAGE *sColor_Imagef);
    
    if (nRes == -1)
    {
        return -1;
    }// if (nRes == -1)
    
    nSizeOfImage = nImageWidth*nImageHeight;
    
    //finding 'nIntensity_Read_Test_ImageMax' to decide if the intensity rescaling to (0,255) is needed
    for (j = 0; j < nImageHeight; j++)
    {
        for (i = 0; i < nImageWidth; i++)
        {
            nIndexOfPixelCur = i + j*nImageWidth;
            
            nRed = image_in(j, i, R);
            nGreen = image_in(j, i, G);
            nBlue = image_in(j, i, B);
            
            if (nRed < 0 || nRed > nIntensityStatForReadMax || nGreen < 0 || nGreen > nIntensityStatForReadMax || nBlue < 0 || nBlue > nIntensityStatForReadMax)
            {
#ifndef COMMENT_OUT_ALL_PRINTS
                
                fprintf(fout_lr, "\n\n An error in reading the image: nRed = %d, nGreen = %d, nBlue = %d, i = %d, j = %d", nRed, nGreen, nBlue, i, j);
                printf("\n\n An error in reading the image: nRed = %d, nGreen = %d, nBlue = %d, i = %d, j = %d", nRed, nGreen, nBlue, i, j);
                printf("\n\n Please press any key to exit");
                getchar(); exit(1);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
                
                return (-1);
            } // if (nRed < 0 || nRed > nIntensityStatForReadMax || nGreen < 0 || nGreen > nIntensityStatForReadMax || nBlue < 0 || nBlue > nIntensityStatForReadMax)
            
            if (nRed > nIntensity_Read_Test_ImageMax)
                nIntensity_Read_Test_ImageMax = nRed;
            
            if (nGreen > nIntensity_Read_Test_ImageMax)
                nIntensity_Read_Test_ImageMax = nGreen;
            
            if (nBlue > nIntensity_Read_Test_ImageMax)
                nIntensity_Read_Test_ImageMax = nBlue;
            
        } // for (int i = 0; i < nImageWidth; i++)
        
    }//for (int j = 0; j < nImageHeight; j++)
    
#ifndef COMMENT_OUT_ALL_PRINTS
    printf("\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);
    fprintf(fout_lr, "\n\n nIntensity_Read_Test_ImageMax = %d", nIntensity_Read_Test_ImageMax);
#endif //#ifndef COMMENT_OUT_ALL_PRINTS
    ////////////////////////////////////////////////////////////////////////////////////////////
    
    for (j = 0; j < nImageHeight; j++)
    {
        for (i = 0; i < nImageWidth; i++)
        {
            nIndexOfPixelCur = i + j*nImageWidth;
            
            nRed = image_in(j, i, R);
            nGreen = image_in(j, i, G);
            nBlue = image_in(j, i, B);
            
            iLen = i;
            iWid = j;
            
            if (nIntensity_Read_Test_ImageMax > nGrayLevelIntensityMax)
            {
                //rescaling
                
                sColor_Image.nRed_Arr[nIndexOfPixelCur] = nRed / 256;
                
                sColor_Image.nGreen_Arr[nIndexOfPixelCur] = nGreen / 256;
                sColor_Image.nBlue_Arr[nIndexOfPixelCur] = nBlue / 256;
                
                //////////////////////////////////////////////////////////////////////////////////////////////////////
                sColor_Image.nRed_Arr[nIndexOfPixelCur] = nRed;
                sColor_Image.nGreen_Arr[nIndexOfPixelCur] = nGreen;
                sColor_Image.nBlue_Arr[nIndexOfPixelCur] = nBlue;
                
            } //if (nIntensity_Read_Test_ImageMax > nGrayLevelIntensityMax)
            else
            {
                // no rescaling
                
                sColor_Image.nRed_Arr[nIndexOfPixelCur] = nRed;
                sColor_Image.nGreen_Arr[nIndexOfPixelCur] = nGreen;
                sColor_Image.nBlue_Arr[nIndexOfPixelCur] = nBlue;
            } //else
            
        } // for (int i = 0; i < nImageWidth; i++)
    }//for (int j = 0; j < nImageHeight; j++)
    
    /////////////////////////////////////////////////////////////////////////////////////////////
    GRAYSCALE_IMAGE sGRAYSCALE_IMAGE;
    
    nRes = Initializing_Grayscale_Image_A(
                                          //nImageHeight, //const int nImageWidthf,
                                          //nImageWidth, //const int nImageLengthf,
                                          
                                          &sColor_Image, // COLOR_IMAGE *sColor_Imagef);
                                          &sGRAYSCALE_IMAGE); // GRAYSCALE_IMAGE *sGrayscale_Imagef);
    
    if (nRes == -1)
    {
        delete[] sColor_Image.nRed_Arr;
        delete[] sColor_Image.nGreen_Arr;
        delete[] sColor_Image.nBlue_Arr;
        
        delete[] sColor_Image.nLenObjectBoundary_Arr;
        delete[] sColor_Image.nIsAPixelBackground_Arr;
        
        /////////////////////////////////////////////////
        delete[] sGRAYSCALE_IMAGE.nPixelArr;
        return (-1);
    }// if (nRes == -1)
    
    // Caculate Multifractal features
    float fOneSelectedFeaf;
    float fOneDim_Multifractal_Arr[nNumOfMultifractalFeasFor_OneDimTot];
    
    nRes = doMultifractalSpectrumOf_ColorImage(image_in, &sColor_Image, fOneDim_Multifractal_Arr);
    nRes = doOneFea_OfMultifractal(nNumOfSelectedFea, &sColor_Image, fOneSelectedFeaf);
    
    // Caculate Lacunarity features
    float fOne_SelectedLacunar_Fea;
    float fOneDim_Lacunar_Arr[nNumOfLacunarFeasFor_OneDimTot];
    float fFractal_Dimension_Out;
    
    nRes = doLacunarityOf_ColorImage(image_in, fOneDim_Lacunar_Arr, fFractal_Dimension_Out);
    nRes = doOne_Fea_OfLacunarityOf_ColorImage(
                                               nNumOfSelectedLacunarityFea,
                                               image_in,
                                               &sColor_Image,
                                               fOne_SelectedLacunar_Fea);
    
}
